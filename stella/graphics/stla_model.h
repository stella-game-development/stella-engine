#pragma once

#include "stla_gfx.h"
#include "stla_mesh.h"
#include "stla_geometry.h"
#include "stla_utils.h"

#include <filesystem>

namespace stla
{
namespace gfx
{

class model
{
public:

    enum class texture_type
    {
        undefined = 0,
        diffuse
    };

    struct texture_info
    {
        texture_type type{ texture_type::undefined };
        std::filesystem::path file_path;
    };

    model() = default;
    void load(const utils::assimp::scene_loader &loader);
    void clear();
    glm::mat4 get_normalization_transform(float target_min=-1.0f, float target_max=1.0f) const;
    const std::vector<mesh> &get_meshes() const { return m_meshes; }
    const std::vector<texture_info> &get_diffuse_maps() const { return m_diffuse_maps; }
protected:
    virtual void process_mesh(const aiMesh *p_mesh, const glm::mat4 &node_transform, const aiScene *p_scene);
    // Parallel arrays
    std::vector<mesh> m_meshes;
    std::vector<texture_info> m_diffuse_maps;
private:
    std::filesystem::path m_file_path;
};

}
}

