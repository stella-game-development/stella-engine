#include "stla_skeletal_animation.h"

namespace stla
{
namespace gfx
{

int32_t animation::channel::get_position_index(float animation_time) const
{
    for (int index = 0; index < positions.size() - 1; ++index) {
        if (animation_time < positions[index + 1].time_stamp) {
            return index;
        }
    }
    STLA_ASSERT(0);
    return -1;
}

int32_t animation::channel::get_rotation_index(float animation_time) const
{
    for (int index = 0; index < rotations.size() - 1; ++index) {
        if (animation_time < rotations[index + 1].time_stamp) {
            return index;
        }
    }
    STLA_ASSERT(0);
    return -1;
}

int32_t animation::channel::get_scale_index(float animation_time) const
{
    for (int index = 0; index < scales.size() - 1; ++index) {
        if (animation_time < scales[index + 1].time_stamp) {
            return index;
        }
    }
    STLA_ASSERT(0);
    return -1;
}

glm::mat4 animation::channel::get_local_transform(float animation_time) const 
{
    glm::mat4 translation = interpolate_position(animation_time);
    glm::mat4 rotation = interpolate_rotation(animation_time);
    glm::mat4 scale = interpolate_scaling(animation_time);
    return translation * rotation * scale;
}

float animation::channel::get_scale_factor(float last_time_stamp, float next_time_stamp, float animation_time) const
{
    float scale_factor = 0.0f;
    float mid_way_length = animation_time - last_time_stamp;
    float frames_diff = next_time_stamp - last_time_stamp;
    scale_factor = mid_way_length / frames_diff;
    return scale_factor;
}

glm::mat4 animation::channel::interpolate_position(float animation_time) const
{
    if (positions.size() == 0) {
        return glm::mat4(1.0f);
    }

    if (positions.size() == 1) {
        return glm::translate(glm::mat4(1.0f), positions[0].position);
    }

    const int p0_index = get_position_index(animation_time);
    const int p1_index = p0_index + 1;

    const float scaleFactor = get_scale_factor(
        positions[p0_index].time_stamp,
        positions[p1_index].time_stamp, 
        animation_time
    );
    
    const glm::vec3 final_position = glm::mix(
        positions[p0_index].position,
        positions[p1_index].position, 
        scaleFactor
    );

    return glm::translate(glm::mat4(1.0f), final_position);
}

glm::mat4 animation::channel::interpolate_rotation(float animation_time) const
{
    if (rotations.size() == 0) {
        return glm::mat4(1.0f);
    }

    if (rotations.size() == 1) {
        return glm::toMat4(glm::normalize(rotations[0].orientation));
    }

    const int p0_index = get_rotation_index(animation_time);
    const int p1_index = p0_index + 1;

    const float scale_factor = get_scale_factor(
        rotations[p0_index].time_stamp,
        rotations[p1_index].time_stamp, 
        animation_time
    );

    const glm::quat final_rotation = glm::normalize(
        glm::slerp(
            rotations[p0_index].orientation,
            rotations[p1_index].orientation, 
            scale_factor
        )
    );

    return glm::toMat4(final_rotation);
}

glm::mat4 animation::channel::interpolate_scaling(float animation_time) const
{
    if (scales.size() == 0) {
        return glm::mat4(1.0f);
    }

    if (scales.size() == 1) {
        return glm::scale(glm::mat4(1.0f), scales[0].scale);
    }

    const int p0_index = get_scale_index(animation_time);
    const int p1_index = p0_index + 1;

    const float scale_factor = get_scale_factor(
        scales[p0_index].time_stamp,
        scales[p1_index].time_stamp, 
        animation_time
    );
    
    const glm::vec3 final_scale = glm::mix(
        scales[p0_index].scale, 
        scales[p1_index].scale, 
        scale_factor
    );
    
    return glm::scale(glm::mat4(1.0f), final_scale);
}

animation::animation(const aiAnimation &animation) :
    m_name(animation.mName.C_Str()), 
    m_duration(static_cast<float>(animation.mDuration)),
    m_ticks_per_second(static_cast<float>(animation.mTicksPerSecond))
{
    for (unsigned int i = 0; i < animation.mNumChannels; i++) {
        const aiNodeAnim *p_channel = animation.mChannels[i];
        
        channel new_channel;
        new_channel.name = p_channel->mNodeName.C_Str();

        for (unsigned int j = 0; j < p_channel->mNumPositionKeys; j++) {
            new_channel.positions.push_back(key_position(
                static_cast<float>(p_channel->mPositionKeys[j].mTime), utils::assimp::glm_helpers::convert(p_channel->mPositionKeys[j].mValue)));
        }
        
        for (unsigned int j = 0; j < p_channel->mNumScalingKeys; j++) {
            new_channel.scales.push_back(key_scale(
                static_cast<float>(p_channel->mScalingKeys[j].mTime), utils::assimp::glm_helpers::convert(p_channel->mScalingKeys[j].mValue)));
        }

        for (unsigned int j = 0; j < p_channel->mNumRotationKeys; j++) {
            new_channel.rotations.push_back(key_rotation(
                static_cast<float>(p_channel->mRotationKeys[j].mTime), utils::assimp::glm_helpers::convert(p_channel->mRotationKeys[j].mValue)));
        }

        m_channels.push_back(new_channel);
    }
}

const animation::channel *animation::find_channel(const std::string &name) const
{
    for (const auto &channel : m_channels) {
        if (name == channel.name) {
            return &channel;
        }
    }
    return nullptr;
}

void animator::set_animation(const animation &anim, const skeleton &skel, uint32_t bone_count, const glm::mat4 &global_inverse_transform)
{
    mp_animation = &anim;
    mp_skeleton = &skel;
    m_final_bone_matrices.clear();
    m_final_bone_matrices.resize(bone_count);
    m_global_inverse_transform = global_inverse_transform;
}

void animator::update(float dt)
{
    m_delta_time = dt;

    if (mp_animation == nullptr || mp_skeleton == nullptr) {
        return;
    }

    m_current_time += mp_animation->get_ticks_per_second() * (dt / 1000.0f);
    m_current_time = m_current_time > mp_animation->get_duration() ? m_current_time = 0.0f : m_current_time;
    calculate_bone_transforms();
}

void animator::calculate_bone_transforms()
{
    std::stack<glm::mat4> transform_stack;
    mp_skeleton->preorder([&](const bone &bone, uint32_t depth) {
        while (transform_stack.size() != depth) { transform_stack.pop();  }
        glm::mat4 node_transform = bone.local_transformation;

        const animation::channel *p_channel = mp_animation->find_channel(bone.name);

        if (p_channel) {
            node_transform = p_channel->get_local_transform(m_current_time);
        }

        glm::mat4 parent_transform = glm::mat4(1.0f);
        if (depth != 0) {
            STLA_ASSERT_RELEASE(transform_stack.size() != 0);
            parent_transform = transform_stack.top();
        }

        glm::mat4 transform = parent_transform * node_transform;

        if (bone.id >= 0) {
            STLA_ASSERT_RELEASE(bone.id < m_final_bone_matrices.size());
            const int index = bone.id;
            m_final_bone_matrices[index] = transform * bone.offset;
        }

        transform_stack.push(transform);
    });
}

} // namespace gfx
} // namespace stla

