#pragma once

#include "stla_utils.h"
#include "stla_model.h"
#include "stla_animation_data.h"

namespace stla
{
namespace gfx
{
    
class animated_model : private model
{
public:
    static inline const uint32_t MAX_BONE_INFLUENCE = 4;

    STLA_PACK(
    struct bone_vertex_data
    {
        int bone_ids[MAX_BONE_INFLUENCE];
        float bone_weights[MAX_BONE_INFLUENCE];
        bone_vertex_data();
        bool set_data(int bone_id, float weight);
    });

    using model::get_meshes;
    using model::get_normalization_transform;

    animated_model() = default;   
    animated_model(const utils::assimp::scene_loader &loader, const skeleton &skeleton);
    ~animated_model();
    
    void clear();

protected:

    virtual void process_mesh(const aiMesh *p_mesh, const glm::mat4 &node_transform, const aiScene *p_scene) override;

private:

    const skeleton *mp_skeleton{ nullptr };
};

} // namespace gfx
} // namespace stla
