#pragma once

#include "stla_gfx.h"
#include "stla_log.h"
#include "stla_geometry.h"

#include <assimp/scene.h>
#include <glm/glm.hpp>

#include <stdint.h>

#include <vector>
#include <filesystem>
#include <concepts>
#include <iostream>
#include <string>
#include <functional>

namespace stla
{
namespace gfx
{

class mesh
{
public: 

    STLA_PACK(struct vertex_p3
    {
        float position[3];
    });

    STLA_PACK(struct vertex_p3n3
    {
        float position[3];
        float normal[3];
    });

    STLA_PACK(struct vertex_p3n3uv2
    {
        float position[3];
        float normal[3];
        float uv[2];
    });

    using vertex_buffer_layout = std::vector<gfx::vertex_attribute_descriptor>;

    struct vertex_buffer
    {
        vertex_buffer_layout layout;
        std::vector<uint8_t> data;
        
        uint32_t get_vertex_element_count() const
        {
            uint32_t count = 0;
            for (const auto &attrib : layout) {
                count += attrib.count;
            }
            return count;
        }

        uint32_t get_vertex_stride() const
        {
            uint32_t vertex_stride = 0;
            for (const auto &attrib : layout) {
                vertex_stride += attrib.get_stride();
            }
            return vertex_stride;
        }
    };

    mesh() = default;
    mesh(const mesh &) = default;
    mesh(mesh &&) = default;
    bool load(const std::filesystem::path &file_path);
    bool load(const aiMesh *p_mesh, const glm::mat4 &transform=glm::mat4(1.0f));
    void push_vertex_buffer(const vertex_buffer_layout &layout, const std::vector<uint8_t> &data);
    const std::vector<vertex_buffer> &get_vertex_buffers() const { return m_vertex_buffers; }
    const std::vector<uint32_t> &get_indices() const { return m_indices; }
    vertex_buffer &create_vertex_buffer();
    void clear();
    uint32_t get_vertex_count() const;
    glm::mat4 get_transform() const { return m_transform; }
    math::aabb3d get_global_aabb() const { return m_global_aabb; }
    mesh &operator = (const mesh &) = default;

    static mesh cube();

    template<typename Func>
    requires std::invocable<Func, const vertex_p3 &, const vertex_p3 &, const vertex_p3 &>
    void for_each(Func visitor) const
    {
        if (m_vertex_buffers.size() == 0) {
            return;
        }

        const auto &vertex_buffer = m_vertex_buffers.front();
        const uint32_t vertex_element_count = vertex_buffer.get_vertex_element_count();

        if ((vertex_buffer.data.size() == 0 && m_indices.size() == 0) || 
            (vertex_buffer.data.size() != 0 && m_indices.size() == 0 && (vertex_buffer.layout.size() % 3 != 0)) ||
            (m_indices.size() != 0 && vertex_buffer.data.size() == 0 && (m_indices.size() % 3 != 0))) {
            STLA_LOG_ERROR("Unable to 'for_each' an invalid triangle mesh (vertices=%d) (indices=%d)", 
                static_cast<int>(vertex_buffer.data.size()), static_cast<int>(m_indices.size()));
            STLA_ASSERT(0);
            return;
        } 

        if (m_indices.size() != 0) {
            for (int i = 0; i < m_indices.size() - 2; i += 3) {
                const uint32_t v0_idx = m_indices[i + 0];
                const uint32_t v1_idx = m_indices[i + 1];
                const uint32_t v2_idx = m_indices[i + 2];

                const float *p_v0 = reinterpret_cast<const float *>(&vertex_buffer.data[v0_idx * vertex_element_count * sizeof(float)]);
                const float *p_v1 = reinterpret_cast<const float *>(&vertex_buffer.data[v1_idx * vertex_element_count * sizeof(float)]);
                const float *p_v2 = reinterpret_cast<const float *>(&vertex_buffer.data[v2_idx * vertex_element_count * sizeof(float)]);
            
                visitor(
                    *reinterpret_cast<const vertex_p3 *>(p_v0),
                    *reinterpret_cast<const vertex_p3 *>(p_v1),
                    *reinterpret_cast<const vertex_p3 *>(p_v2)
                );

                // if constexpr (std::invocable<Func>) {
                //     visitor();
                // }
                // else if constexpr (std::invocable<Func, const float *, const float *, const float *>) {
                //     visitor(p_v0, p_v1, p_v2);
                // }
                // else if constexpr (std::invocable<Func, const float *, const float *, const float *, const std::vector<gfx::vertex_attribute_descriptor> &>) {
                //     visitor(p_v0, p_v1, p_v2, m_vertex_layout);
                // }
            }
        }
        else {
            STLA_LOG_ERROR("'for_each' not implemented for triangle mesh with no indices");
            STLA_ASSERT_RELEASE(0);
        }
    }

    friend std::ostream &operator << (std::ostream &os, const mesh &mesh);

private:

    glm::mat4 m_transform = glm::mat4(1.0f);
    math::aabb3d m_global_aabb;
    std::filesystem::path m_file_path;
    std::string m_name;
    std::vector<vertex_buffer> m_vertex_buffers;
    std::vector<uint32_t> m_indices;
};

} // namespace gfx
} // namespace stla
