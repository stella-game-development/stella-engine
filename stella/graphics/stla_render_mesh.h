#pragma once

#include "stla_gfx.h"

#include <stdint.h>

#include <vector>

namespace stla
{
namespace gfx
{

class mesh;
class command_buffer;

class render_mesh
{
public:
    render_mesh() = default;
    render_mesh(const mesh &mesh);
    bool create(const mesh &mesh);
    void destroy();
    void draw(command_buffer& command_buffer) const;
private:

    struct render_buffer
    {
        buffer buffer;
        std::vector<vertex_attribute_descriptor> layout;
    };

    uint32_t m_index_count{ 0 };
    uint32_t m_vertex_count{ 0 };
    buffer m_index_buffer;
    std::vector<render_buffer> m_render_buffers;
};

} // namespace gfx
} // namespace stla
