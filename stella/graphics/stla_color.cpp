#pragma once

#include "stla_color.h"

namespace stla
{
namespace color
{

template<> 
glm::u8vec4 red<glm::u8vec4>()
{
    return glm::u8vec4(255, 0, 0, 255);
}

template<>
glm::u8vec4 green<glm::u8vec4>()
{
    return glm::u8vec4(0, 255, 0, 255);
}

template<>
glm::u8vec4 blue<glm::u8vec4>()
{
    return glm::u8vec4(0, 0, 255, 255);
}

template<> 
glm::vec4 red<glm::vec4>()
{
    return glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
}

template<>
glm::vec4 green<glm::vec4>()
{
    return glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
}

template<>
glm::vec4 blue<glm::vec4>()
{
    return glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
}

glm::vec3 hex_to_rgb(uint32_t hex) { 
    return glm::vec3(
        ((hex & 0xFF0000) >> 16) / 256.0f,
        ((hex & 0x00FF00) >> 8) / 256.0f,
        ((hex & 0x0000FF) >> 0) / 256.0f
    ); 
}   
} // namespace color
} // namespace stla
