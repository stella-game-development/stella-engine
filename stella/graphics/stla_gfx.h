#pragma once

#include "stla_utils.h"

#include <glad/glad.h>

#include <array>
#include <vector>
#include <filesystem>
#include <variant>
#include <unordered_map>
#include <queue>
#include <cstdlib>
#include <filesystem>

namespace stla
{
namespace gfx
{

class command_buffer;
class shader;

enum class pixel_format
{
    undefined,
    r8g8b8a8,
    r8g8b8,
    r8g8b8a8_float,
    depth24_stencil8
};

enum class primitive_type
{
    undefined, 
    line,
    triangle
};

enum class buffer_type 
{
    undefined,
    index,
    vertex,
    uniform,
    shader_storage
};

enum class image_type 
{
    undefined,
    texture,
    color_attachment,
    depth_attachment
};

enum class copy_type 
{
    undefined,
    host_to_buffer,
    host_to_image,
    buffer_to_host,
    buffer_to_image,
    buffer_to_buffer,
    image_to_host,
    image_to_image,
    image_to_buffer
};

enum class shader_type 
{
    undefined,
    vertex,
    fragment
};

enum class vertex_attribute_type 
{
    undefined,
    f,
    i,
    mat4
};

enum sampler_wrap_type 
{
    undefined,
    repeat,
    clamp_to_edge
};

enum class sampler_filter_type 
{
    undefined,
    nearest,
    linear
};

union color 
{
    float raw[4];
    struct 
    {
        float r;
        float g;
        float b;
        float a;
    };

    color() : r(0.0f), g(0.0f), b(0.0f), a(0.0f) { }
    color(float v) : r(v), g(v), b(v), a(v) { }
    color(float r, float g, float b, float a) :
        r(r), g(g), b(b), a(a) { }
};

union ivec2 
{
    uint32_t raw[2];
    struct 
    {
        uint32_t x;
        uint32_t y;
    };
    ivec2() { std::memset(this, 0, sizeof(ivec2)); }
    ivec2(uint32_t x, uint32_t y) : x(x), y(y) { }
};

struct region_2d 
{
    ivec2 offset;
    ivec2 extent;

    region_2d(); 
    region_2d(uint32_t offset_x, uint32_t offset_y, uint32_t extent_x, uint32_t extent_y);
};

struct draw_state 
{
    bool enable_depth_test{ true };
    bool enable_cull_face{ true };
    bool enable_blend{ true };

    draw_state() = default;
    draw_state(bool depth, bool cull, bool blend) :
        enable_depth_test(depth), enable_cull_face(cull), enable_blend(blend) { }
};

struct vertex_attribute_descriptor 
{
    uint32_t loc{ 0 };
    uint32_t count{ 0 };
    vertex_attribute_type type{ vertex_attribute_type::undefined };
    bool instanced{ false };
    vertex_attribute_descriptor() = default;
    vertex_attribute_descriptor(uint32_t loc, uint32_t count, vertex_attribute_type type, bool instanced) : type(type), count(count), loc(loc), instanced(instanced) { }
    size_t get_stride() const;
};

struct image_info
{
    image_type type{ image_type::undefined };
    pixel_format format{ pixel_format::undefined };
    sampler_wrap_type wrap{ sampler_wrap_type::undefined };
    sampler_filter_type filter{ sampler_filter_type::undefined };
    ivec2 size;
    image_info() = default;
    image_info(image_type type, pixel_format format, sampler_wrap_type wrap, sampler_filter_type filter, const ivec2 &size) :
        type(type), format(format), wrap(wrap), filter(filter), size(size) { }
};

class api
{
public:

    api();
    ~api();

    static void startup();
    static void shutdown();
    static bool submit(command_buffer &cmd_buffer);

private:

    bool execute_commands(command_buffer &cmd_buffer);
    uint32_t get_program(uint32_t vertex_shader, uint32_t fragment_shader);

    static inline api *sp_instance{ nullptr };
    static inline const std::vector<unsigned int> s_color_attachments = { 
        GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3,
        GL_COLOR_ATTACHMENT4, GL_COLOR_ATTACHMENT5, GL_COLOR_ATTACHMENT6, GL_COLOR_ATTACHMENT7,
        GL_COLOR_ATTACHMENT8, GL_COLOR_ATTACHMENT9, GL_COLOR_ATTACHMENT10, GL_COLOR_ATTACHMENT11,
        GL_COLOR_ATTACHMENT12, GL_COLOR_ATTACHMENT13, GL_COLOR_ATTACHMENT14, GL_COLOR_ATTACHMENT15,
    };

    bool m_renderpass_started{ false };
    uint32_t m_framebuffer{ 0 };
    uint32_t m_vao{ 0 };
    uint32_t m_active_shader{ 0 };
    std::unordered_map<uint32_t, std::unordered_map<uint32_t, uint32_t>> m_shader_programs; // vertex -> fragment -> linked program
};

class shader
{
public:
    bool create(shader_type type, const void *p_code);
    bool create(shader_type type, const std::filesystem::path &file_path);
    void destroy();
    bool is_valid() const;
    uint32_t get() const { return m_shader; }
    shader_type get_type() const { return m_type; }
private:
    shader_type m_type{ shader_type::undefined };
    uint32_t m_shader{ 0 };
};

class buffer
{
public:
    bool create(buffer_type type, size_t size, const void *p_data=nullptr);
    template<typename T>
    bool create(buffer_type type, const std::vector<T> &data);
    template<typename T>
    bool create(buffer_type type, const T &data);
    void destroy();
    bool is_valid() const;
    buffer_type get_type() const { return m_type; }
    size_t get_size() const { return m_size; }
    uint32_t get() const { return m_buffer; }
    bool copy(const void *src, size_t size, size_t offset=0);
    template<typename T>
    bool copy(const T &data);
    template<typename T>
    bool copy(const std::vector<T> &data);
    void *map();
    void unmap();
private:
    buffer_type m_type{ buffer_type::undefined };
    size_t m_size{ 0 };
    uint32_t m_buffer{ 0 };
    bool m_mapped{ false };
};

template<typename T>
bool buffer::create(buffer_type type, const std::vector<T> &data)
{
    return create(type, data.size() * sizeof(T), data.data());
}

template<typename T>
bool buffer::create(buffer_type type, const T &data)
{
    return create(type, sizeof(T), &data);
}

template<typename T>
bool buffer::copy(const T &data)
{
    return copy(&data, sizeof(T), 0);
}

template<typename T>
bool buffer::copy(const std::vector<T> &data)
{
    return copy(data.data(), sizeof(T) * data.size(), 0);
}
    
class image
{
public:
    bool create(const image_info &info);
    bool create(const image_info &info, const void *p_data, size_t size);
    template<typename T>
    bool create(const image_info &info, const T &data);
    bool create(const std::filesystem::path &file_path, sampler_wrap_type wrap, sampler_filter_type filter);
    void destory();
    bool is_valid() const;
    image_type get_type() const { return m_info.type; }
    pixel_format get_pixel_format() const { return m_info.format; }
    ivec2 get_size() const { return m_info.size; }
    uint32_t get() const { return m_image; }
    uint64_t get_handle() const { return m_handle; }
    bool copy(const void *src, size_t size);
private:
    image_info m_info;
    uint32_t m_image{ 0 };
    uint64_t m_handle{ 0 };
};

template<typename T>
bool image::create(const image_info &info, const T &data)
{
    return create(info, &data, sizeof(T));
}

class command_buffer
{
public:
    command_buffer() = default;
    bool execute();
    void set_state(const draw_state &state);
    void begin_rendering(const region_2d &region);
    void begin_rendering(const image &color_attachment, const image &depth_attachment, const region_2d &region);
    void end_rendering();
    void bind(const shader &vertex_shader, const shader &fragment_shader);
    void bind(const buffer &buffer);
    void bind(const vertex_attribute_descriptor &desc, const buffer &buffer);
    void bind(const std::vector<vertex_attribute_descriptor> &layout, const buffer &buffer);
    void bind_uniform(uint32_t loc, const buffer &buffer);
    void clear(const color &color, const region_2d &region);
    void copy(buffer &dst, const void *src, size_t size, size_t offset=0); // host to buffer
    void copy(void *dst, const buffer &src, size_t size, size_t offset=0); // buffer to host
    void draw(uint32_t vertex_count, uint32_t instances=1);
    void draw_indexed(uint32_t vertex_count, uint32_t instances=1);

    enum class command_type
    {
        begin_rendering,
        end_rendering,
        set_draw_state,
        bind_program,
        bind_buffer,
        bind_uniform,
        clear,
        copy,
        draw,
        draw_indexed
    };

    class command_data
    {
    public:

        struct begin_rendering
        {
            std::vector<const image *> color_attachments;
            const image *p_depth_attachment{ nullptr };
            region_2d region;
        };

        struct end_rendering { };

        struct set_draw_state
        {
            draw_state state;
        };

        struct bind_program
        {
            const shader *p_vertex{ nullptr };
            const shader *p_fragment{ nullptr };
        };

        struct bind_buffer
        {
            std::vector<vertex_attribute_descriptor> layout;
            const buffer *p_buffer{ nullptr };
        };

        struct bind_uniform
        {
            uint32_t location{ 0 };
            const buffer *p_buffer{ nullptr };
        };

        struct clear
        {
            color color;
            region_2d region;
        };

        struct copy
        {
            copy_type type{ copy_type::undefined };
            void *p_dst{ nullptr };
            const void *p_src{ nullptr };
            size_t size{ 0 };
            size_t offset{ 0 };
        };

        struct draw
        {
            uint32_t instances{ 1 };
            uint32_t vertex_count{ 0 };
        };

        struct draw_indexed
        {
            uint32_t instances{ 1 };
            uint32_t index_count{ 0 }; 
        };

        command_data(const begin_rendering &data) : m_command_data(data) { }
        command_data(const end_rendering &data) : m_command_data(data) { }
        command_data(const set_draw_state &data) : m_command_data(data) { }
        command_data(const bind_program &data) : m_command_data(data) { }
        command_data(const bind_buffer &data) : m_command_data(data) { }
        command_data(const bind_uniform &data) : m_command_data(data) { }
        command_data(const clear &data) : m_command_data(data) { }
        command_data(const copy &data) : m_command_data(data) { }
        command_data(const draw &data) : m_command_data(data) { }
        command_data(const draw_indexed &data) : m_command_data(data) { }

        template<typename T>
        const T &get() const
        {
            const T *p_data = std::get_if<T>(&m_command_data);
            STLA_ASSERT_RELEASE(p_data != nullptr);
            return *p_data;
        }

        command_type get_type() const
        {
            return static_cast<command_type>(m_command_data.index());
        }

    private:

        std::variant<
            begin_rendering, 
            end_rendering, 
            set_draw_state, 
            bind_program, 
            bind_buffer,
            bind_uniform,
            clear,
            copy,
            draw,
            draw_indexed
        > m_command_data;
    };

    std::queue<command_data> &get_commands() { return m_command_queue; }

private:
    std::queue<command_data> m_command_queue;
    bool m_renderpass_started{ false };
};

} // namespace gfx
} // namespace stla
