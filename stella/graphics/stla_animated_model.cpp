#include "stla_animated_model.h"

namespace stla
{
namespace gfx
{

animated_model::animated_model(const utils::assimp::scene_loader &loader, const skeleton &skeleton)
{
    mp_skeleton = &skeleton;
    model::load(loader);
}

animated_model::~animated_model() 
{
    clear();
} 

void animated_model::clear()
{
    model::clear();
    mp_skeleton = nullptr; 
}

void animated_model::process_mesh(const aiMesh *p_mesh, const glm::mat4 &node_transform, const aiScene *p_scene)
{
    STLA_ASSERT_RELEASE(mp_skeleton != nullptr);

    model::process_mesh(p_mesh, node_transform, p_scene);
    
    auto &tri_mesh = m_meshes.back();
    STLA_ASSERT(tri_mesh.get_vertex_buffers().size() == 1);

    auto &bone_vertex_buffer = tri_mesh.create_vertex_buffer();
    const auto &base_final_vertex_attrib = tri_mesh.get_vertex_buffers().front().layout.back();
    
    // Buffer layout
    const uint32_t loc = (base_final_vertex_attrib.type == vertex_attribute_type::mat4 ?
        base_final_vertex_attrib.loc + 4 : base_final_vertex_attrib.loc + 1);

    bone_vertex_buffer.layout.push_back(
        stla::gfx::vertex_attribute_descriptor(
            loc + 0, MAX_BONE_INFLUENCE, stla::gfx::vertex_attribute_type::i, false));
    
    bone_vertex_buffer.layout.push_back(
        stla::gfx::vertex_attribute_descriptor(
            loc + 1, MAX_BONE_INFLUENCE, stla::gfx::vertex_attribute_type::f, false));

    // Initialize all bone data
    std::vector<bone_vertex_data> bone_data;
    bone_data.resize(p_mesh->mNumVertices);

    for (int i = 0; i < p_mesh->mNumBones; i++) {
        const aiBone *p_aibone = p_mesh->mBones[i];
        const bone *p_bone = mp_skeleton->get_bone(p_mesh->mBones[i]->mName.C_Str());
        STLA_ASSERT_RELEASE(p_bone != nullptr);
        STLA_ASSERT_RELEASE(p_bone->id != -1);

        if (p_bone->mesh_vertex_weights.find(p_mesh->mName.C_Str()) == p_bone->mesh_vertex_weights.end()) {
            STLA_LOG_WARN("Bone has no weights for mesh (%s), expected %d", p_mesh->mName.C_Str(), p_aibone->mNumWeights);
            continue;
        }

        const auto &weights = p_bone->mesh_vertex_weights.at(p_mesh->mName.C_Str());
        for (const auto &weight : weights) {
            STLA_ASSERT_RELEASE(weight.vertex_id <= bone_data.size());
            if (!bone_data[weight.vertex_id].set_data(p_bone->id, weight.value)) {
                STLA_LOG_ERROR("Vertex %d bone data full", weight.vertex_id);
            }
        }
    }

    bone_vertex_buffer.data.resize(bone_data.size() * sizeof(bone_data[0]));
    std::memcpy(bone_vertex_buffer.data.data(), bone_data.data(), bone_vertex_buffer.data.size());
}

animated_model::bone_vertex_data::bone_vertex_data()
{
    for (int i = 0; i < MAX_BONE_INFLUENCE; i++) {
        bone_ids[i] = -1;
        bone_weights[i] = 0.0f;
    }
}

bool animated_model::bone_vertex_data::set_data(int bone_id, float weight)
{
    for (int i = 0; i < MAX_BONE_INFLUENCE; i++) {
        if (bone_ids[i] < 0) {
            bone_weights[i] = weight;
            bone_ids[i] = bone_id;
            return true;
        }
    }
    return false;
}
} // namespace gfx
} // namespace stla
