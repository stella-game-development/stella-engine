#include "stla_animation_data.h"

namespace stla
{
namespace gfx
{

skeleton::skeleton(const utils::assimp::scene_loader &loader)
{
    load(loader);
}

void skeleton::load(const utils::assimp::scene_loader &loader)
{
    clear();
    loader.for_each([&](const aiNode *p_node, const glm::mat4 &parent_transform, const aiScene *p_scene) { process_node(p_node, parent_transform, p_scene); });
    loader.for_each([&](const aiMesh *p_mesh, const aiScene *p_scene) { process_mesh(p_mesh, p_scene); });
}

void skeleton::clear()
{
    m_root = bone();
}

const bone *skeleton::get_bone(const std::string &name) const
{
    if (m_name_to_bone_map.find(name) == m_name_to_bone_map.end()) {
        return nullptr;
    }
    return m_name_to_bone_map.at(name);
}


void skeleton::process_node(const aiNode *p_node, const glm::mat4 &parent_transform, const aiScene *p_scene)
{ 
    if (p_node->mParent == nullptr) {
        m_root.name = p_node->mName.C_Str();
        m_root.p_parent = nullptr;
        // NOTE: this is important to ensure realloc doesn't happen since pointers to children are stored
        m_root.children.reserve(p_node->mNumChildren); 
        m_root.local_transformation = utils::assimp::glm_helpers::convert(p_node->mTransformation);

        m_name_to_bone_map[m_root.name] = &m_root;
        return;
    }

    STLA_ASSERT_RELEASE(p_node->mParent);
    STLA_ASSERT_RELEASE(m_name_to_bone_map.find(p_node->mParent->mName.C_Str()) != m_name_to_bone_map.end());
    
    bone *p_parent = m_name_to_bone_map[p_node->mParent->mName.C_Str()];

    p_parent->children.push_back(bone());

    bone &new_bone = p_parent->children.back();
    new_bone.name = p_node->mName.C_Str();
    new_bone.p_parent = p_parent;
    new_bone.local_transformation = utils::assimp::glm_helpers::convert(p_node->mTransformation);
    
    new_bone.children.reserve(p_node->mNumChildren);

    m_name_to_bone_map[new_bone.name] = &new_bone;
}

void skeleton::process_mesh(const aiMesh *p_mesh, const aiScene *p_scene) 
{
    for (int bone_idx = 0; bone_idx < p_mesh->mNumBones; bone_idx++) {
        // int bone_id = -1;
        const aiBone *p_aibone = p_mesh->mBones[bone_idx];
        const std::string bone_name = p_aibone->mName.C_Str();
        
        STLA_ASSERT_RELEASE(m_name_to_bone_map.find(bone_name) != m_name_to_bone_map.end());

        bone *p_bone = m_name_to_bone_map[bone_name];
        if (p_bone->mesh_vertex_weights.find(p_mesh->mName.C_Str()) != p_bone->mesh_vertex_weights.end()) {
            STLA_LOG_ERROR("All meshes must have a unique name");
            STLA_ASSERT_RELEASE(0);
        }

        if (m_name_to_bone_id_map.find(bone_name) == m_name_to_bone_id_map.end()) {
            p_bone->id = m_name_to_bone_id_map.size();
            p_bone->offset = utils::assimp::glm_helpers::convert(p_aibone->mOffsetMatrix);
            m_name_to_bone_id_map[bone_name] = p_bone->id;
        }
        
        STLA_ASSERT_RELEASE(m_name_to_bone_id_map.find(bone_name) != m_name_to_bone_id_map.end());
        int32_t bone_id = m_name_to_bone_id_map[bone_name];

        auto &weights = p_bone->mesh_vertex_weights[p_mesh->mName.C_Str()];
        for (int i = 0; i < p_aibone->mNumWeights; i++) {
            const bone::weight new_weight = { p_aibone->mWeights[i].mVertexId, p_aibone->mWeights[i].mWeight };
            weights.push_back(new_weight);
        }
    }
}
    
} // namespace gfx
} // namespace stla
