#include "stla_gfx.h"
#include "stla_log.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include <glad/glad.h>

#include <iostream>
#include <fstream>
#include <sstream>

#ifdef NDEBUG
    #define GL_CHECK(glFunc) (glFunc)
#else
    #define GL_CHECK(glFunc)                                               \
        do {                                                               \
            glFunc;                                                        \
            GLenum err = glGetError();                                     \
            if (err != GL_NO_ERROR) {                                      \
                STLA_LOG_ERROR("OpenGL Error (%d) after calling %s in %s at line %d", \
                              err, #glFunc, __FILE__, __LINE__);           \
                STLA_ASSERT(0);                                            \
            }                                                              \
        } while(0)
#endif

void APIENTRY gl_debug_callback(GLenum source, GLenum type, GLuint id, GLenum severity,
    GLsizei length, const GLchar * message, const void* userParam)
{
    //std::cerr << "OpenGL Debug Message: " << message << std::endl;
}

namespace stla
{
namespace gfx
{

static uint32_t to_gl_format(pixel_format format)
{
    switch (format)
    {
        case pixel_format::r8g8b8a8:  return GL_RGBA;
        case pixel_format::r8g8b8:    return GL_RGB;
        case pixel_format::r8g8b8a8_float: return GL_RGBA;
        case pixel_format::depth24_stencil8: return GL_RGBA;
        default: STLA_ASSERT(0); return GL_NONE;
    }
}

static uint32_t to_gl_internal_format(pixel_format format)
{
    switch (format)
    {
        case pixel_format::r8g8b8a8:  return GL_RGBA8;
        case pixel_format::r8g8b8:    return GL_RGB8;
        case pixel_format::r8g8b8a8_float: return GL_RGBA32F;
        case pixel_format::depth24_stencil8: return GL_DEPTH24_STENCIL8;
        default: STLA_ASSERT(0); return GL_NONE;
    }
}

static uint32_t to_gl_data_type(pixel_format format)
{
    switch (format)
    {
        case pixel_format::r8g8b8a8:  return GL_UNSIGNED_BYTE;
        case pixel_format::r8g8b8:    return GL_UNSIGNED_BYTE;
        case pixel_format::r8g8b8a8_float: return GL_FLOAT;
        default: STLA_ASSERT(0); return GL_NONE;
    }
}

static uint32_t to_gl_buffer_type(buffer_type type)
{
    switch (type)
    {
        case buffer_type::vertex:         return GL_ARRAY_BUFFER;
        case buffer_type::index:          return GL_ELEMENT_ARRAY_BUFFER;
        case buffer_type::uniform:        return GL_UNIFORM_BUFFER;
        case buffer_type::shader_storage: return GL_SHADER_STORAGE_BUFFER;
        default: return GL_NONE;
    }
}

static uint32_t to_gl_type(shader_type type)
{
    switch (type)
    {
        case shader_type::vertex:   return GL_VERTEX_SHADER;
        case shader_type::fragment: return GL_FRAGMENT_SHADER;
        default:                    return GL_NONE;
    }
}

static uint32_t to_gl_type(vertex_attribute_type type)
{
    switch (type)
    {
        case vertex_attribute_type::f: return GL_FLOAT;
        case vertex_attribute_type::i: return GL_INT;
        case vertex_attribute_type::mat4: return GL_FLOAT_MAT4;
        default: return GL_NONE;
    }
}

static uint32_t to_gl_type(sampler_wrap_type type)
{
    switch (type)
    {
        case sampler_wrap_type::repeat: return GL_REPEAT;
        case sampler_wrap_type::clamp_to_edge: return GL_CLAMP_TO_EDGE;
        default: return GL_NONE;
    }
}

static uint32_t to_gl_type(sampler_filter_type type)
{
    switch (type)
    {
        case sampler_filter_type::linear: return GL_LINEAR;
        case sampler_filter_type::nearest: return GL_NEAREST;
        default: return GL_NONE;
    }
}

static size_t get_size(vertex_attribute_type type)
{
    switch (type)
    {
        case vertex_attribute_type::f: return sizeof(float);
        case vertex_attribute_type::i: return sizeof(int);
        case vertex_attribute_type::mat4: return sizeof(float) * 4 * 4;
        default:
        {
            STLA_LOG_ERROR("Unhandled vertex attrib type (type=%d)", static_cast<int>(type));
            STLA_ASSERT_RELEASE(0);
            return 0;
        }
    }
}

static uint32_t get_pixel_size(pixel_format format)
{
    switch (format)
    {
        case pixel_format::r8g8b8a8:  return sizeof(uint8_t) * 4;
        case pixel_format::r8g8b8:    return sizeof(uint8_t) * 3;
        case pixel_format::r8g8b8a8_float: return sizeof(float) * 4;
        default: STLA_ASSERT(0); return 0;
    }
}

static uint64_t set_vertex_attribute(const vertex_attribute_descriptor &attrib, size_t vertex_stride, uint64_t offset)
{
    switch (attrib.type)
    {
        case vertex_attribute_type::mat4:
        {
            STLA_ASSERT_RELEASE(attrib.count == 1);
            glEnableVertexAttribArray(attrib.loc + 0);
            glVertexAttribPointer(attrib.loc + 0, 4, GL_FLOAT, GL_FALSE, static_cast<GLsizei>(vertex_stride), reinterpret_cast<void *>(offset));
            offset += get_size(vertex_attribute_type::f) * 4;

            glEnableVertexAttribArray(attrib.loc + 1);
            glVertexAttribPointer(attrib.loc + 1, 4, GL_FLOAT, GL_FALSE, static_cast<GLsizei>(vertex_stride), reinterpret_cast<void *>(offset));
            offset += get_size(vertex_attribute_type::f) * 4;

            glEnableVertexAttribArray(attrib.loc + 2);
            glVertexAttribPointer(attrib.loc + 2, 4, GL_FLOAT, GL_FALSE, static_cast<GLsizei>(vertex_stride), reinterpret_cast<void *>(offset));
            offset += get_size(vertex_attribute_type::f) * 4;

            glEnableVertexAttribArray(attrib.loc + 3);
            glVertexAttribPointer(attrib.loc + 3, 4, GL_FLOAT, GL_FALSE, static_cast<GLsizei>(vertex_stride), reinterpret_cast<void *>(offset));
            offset += get_size(vertex_attribute_type::f) * 4;

            GL_CHECK(glVertexAttribDivisor(attrib.loc + 0, attrib.instanced ? 1 : 0));
            GL_CHECK(glVertexAttribDivisor(attrib.loc + 1, attrib.instanced ? 1 : 0));
            GL_CHECK(glVertexAttribDivisor(attrib.loc + 2, attrib.instanced ? 1 : 0));
            GL_CHECK(glVertexAttribDivisor(attrib.loc + 3, attrib.instanced ? 1 : 0));

            break;
        }   
        default:
        {
            GL_CHECK(glEnableVertexAttribArray(attrib.loc));
            if (attrib.type == vertex_attribute_type::i) {
                GL_CHECK(glVertexAttribIPointer(
                    attrib.loc, 
                    attrib.count, 
                    to_gl_type(attrib.type), 
                    static_cast<GLsizei>(vertex_stride),
                    reinterpret_cast<void *>(offset)));
            }
            else {
                GL_CHECK(glVertexAttribPointer(
                    attrib.loc, 
                    attrib.count, 
                    to_gl_type(attrib.type), 
                    GL_FALSE, 
                    static_cast<GLsizei>(vertex_stride),
                    reinterpret_cast<void *>(offset)));
            }

            offset += get_size(attrib.type) * attrib.count; 
            
            GL_CHECK(glVertexAttribDivisor(attrib.loc, attrib.instanced ? 1 : 0));
            
            break;
        }
    }

    return offset;
}

region_2d::region_2d()
{
    offset.x = 0;
    offset.y = 0;
    extent.x = 0;
    extent.y = 0;
}

region_2d::region_2d(uint32_t offset_x, uint32_t offset_y, uint32_t extent_x, uint32_t extent_y)
{
    offset.x = offset_x;
    offset.y = offset_y;
    extent.x = extent_x;
    extent.y = extent_y;
}

size_t vertex_attribute_descriptor::get_stride() const
{
    return get_size(type) * count;
}

api::api()
{
    STLA_ASSERT_RELEASE(gladLoadGLLoader((GLADloadproc)SDL_GL_GetProcAddress));
    
    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(gl_debug_callback, nullptr);

    GL_CHECK(glGenVertexArrays(1, &m_vao));
    GL_CHECK(glBindVertexArray(m_vao)); 
    
    GL_CHECK(glGenFramebuffers(1, &m_framebuffer));
}

api::~api()
{
    glFinish();
    glDeleteVertexArrays(1, &m_vao);
    glDeleteFramebuffers(1, &m_framebuffer);
    m_vao = 0;
    m_framebuffer = 0;
}

void api::startup()
{
    if (sp_instance != nullptr) {
        return;
    }

    sp_instance = new api();
}

void api::shutdown()
{
    if (sp_instance == nullptr) {
        return;
    }

    delete sp_instance;
}

bool api::submit(command_buffer &cmd_buffer)
{
    if (sp_instance == nullptr) {
        return false;
    }

    return sp_instance->execute_commands(cmd_buffer);
}

bool api::execute_commands(command_buffer &cmd_buffer)
{
    auto &commands = cmd_buffer.get_commands();

    while (commands.size() != 0) {
        auto data = commands.front();
        commands.pop();

        switch (data.get_type())
        {
            case command_buffer::command_type::begin_rendering:
            {
                const auto &begin = data.get<command_buffer::command_data::begin_rendering>();
                STLA_ASSERT(m_renderpass_started == false);

                if (begin.color_attachments.size() == 0) {
                    GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, 0));
                }
                else {
                    glBindFramebuffer(GL_FRAMEBUFFER, m_framebuffer);

                    uint32_t i = 0;
                    for (const auto &color_attach : begin.color_attachments) {
                        if (color_attach == nullptr || !color_attach->is_valid()) {
                            STLA_LOG_ERROR("Invalid color attachment (attachment=%d)", i);
                            continue;
                        }

                        glFramebufferTexture2D(
                            GL_FRAMEBUFFER, 
                            GL_COLOR_ATTACHMENT0 + i, 
                            GL_TEXTURE_2D, 
                            // color_attach.get()->texture,
                            color_attach->get(),
                            0
                        );
                    
                        i++;
                    }

                    glDrawBuffers(begin.color_attachments.size(), s_color_attachments.data());

                    if (begin.p_depth_attachment == nullptr || !begin.p_depth_attachment->is_valid()) {
                        STLA_LOG_ERROR("Invalid depth attachment");
                    }
                    else {
                        glFramebufferTexture2D(
                            GL_FRAMEBUFFER, 
                            GL_DEPTH_STENCIL_ATTACHMENT, 
                            GL_TEXTURE_2D, 
                            // begin.depth_attachment.get()->texture,
                            begin.p_depth_attachment->get(),
                            0
                        );
                    }

                    uint32_t framebuffer_complete = glCheckFramebufferStatus(GL_FRAMEBUFFER);
                    STLA_ASSERT(framebuffer_complete == GL_FRAMEBUFFER_COMPLETE);
                }

                m_renderpass_started = true;
                GL_CHECK(glViewport(begin.region.offset.x, begin.region.offset.y, begin.region.extent.x, begin.region.extent.y));

                m_active_shader = 0;

                break;
            }
            case command_buffer::command_type::end_rendering:
            {
                STLA_ASSERT(m_renderpass_started);

                m_renderpass_started = false;
                m_active_shader = 0;

                //for (int i = 0; i < 1; i++) {
                //   GL_CHECK(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, 0, 0));
                //}

                //GL_CHECK(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, 0, 0));

                for (int i = 0; i < 16; i++) {
                    GL_CHECK(glDisableVertexAttribArray(i));
                }

                GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, 0));

                break;
            }
            case command_buffer::command_type::set_draw_state:
            {
                const auto &draw_state = data.get<command_buffer::command_data::set_draw_state>();

                if (draw_state.state.enable_depth_test) {
                    GL_CHECK(glEnable(GL_DEPTH_TEST));
                    GL_CHECK(glDepthFunc(GL_LESS));
                }
                else {
                    GL_CHECK(glDisable(GL_DEPTH_TEST));
                }

                if (draw_state.state.enable_cull_face) {
                    GL_CHECK(glEnable(GL_CULL_FACE));
                    GL_CHECK(glCullFace(GL_BACK));
                    GL_CHECK(glFrontFace(GL_CCW));
                }
                else {
                    GL_CHECK(glDisable(GL_CULL_FACE));
                }

                if (draw_state.state.enable_blend) {
                    GL_CHECK(glEnable(GL_BLEND));
                    GL_CHECK(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));
                }
                else {
                    GL_CHECK(glDisable(GL_BLEND));
                }

                break;
            }
            case command_buffer::command_type::bind_program:
            {
                const auto &bind = data.get<command_buffer::command_data::bind_program>();
                if (bind.p_vertex == nullptr || bind.p_fragment == nullptr) {
                    STLA_LOG_ERROR("Shader is null (vertex=%p) (fragment=%d)", bind.p_vertex, bind.p_fragment);
                    break;
                }

                uint32_t program = get_program(bind.p_vertex->get(), bind.p_fragment->get());
                if (program == 0) {
                    STLA_LOG_ERROR("Failed to link shaders");
                    break;
                }

                GL_CHECK(glUseProgram(program));

                m_active_shader = program;

                break;
            }
            case command_buffer::command_type::bind_buffer:
            {
                const auto &bind = data.get<command_buffer::command_data::bind_buffer>();
                if (bind.p_buffer == nullptr) {
                    STLA_LOG_ERROR("Buffer is null");
                    break;
                }

                if (!bind.p_buffer->is_valid()) {
                    STLA_LOG_ERROR("Buffer is invalid");
                    break;
                }
                
                GL_CHECK(glBindBuffer(to_gl_buffer_type(bind.p_buffer->get_type()), bind.p_buffer->get()));

                size_t vertex_stride = 0;
                for (const auto &attrib : bind.layout) {
                    vertex_stride += attrib.get_stride();
                }

                uint64_t offset = 0;
                for (uint32_t i = 0; i < bind.layout.size(); i++) {
                    const auto &attrib = bind.layout[i];     
                    offset = set_vertex_attribute(attrib, vertex_stride, offset);
                    
                }

                break;
            }
            case command_buffer::command_type::bind_uniform:
            {
                const auto &bind = data.get<command_buffer::command_data::bind_uniform>();

                if (m_active_shader == 0) {
                    STLA_LOG_ERROR("Failed to bind uniform, no active shader");
                    break;
                }

                if (bind.p_buffer == nullptr) {
                    STLA_LOG_ERROR("Uniform buffer is null");
                    break;
                }

                if (!bind.p_buffer->is_valid()) {
                    STLA_LOG_ERROR("Uniform buffer is invalid");
                    break;
                } 

                GL_CHECK(glUniformBlockBinding(m_active_shader, bind.location, 0));
                GL_CHECK(glBindBufferRange(
                    GL_UNIFORM_BUFFER, 
                    bind.location, 
                    bind.p_buffer->get(), 
                    0, 
                    bind.p_buffer->get_size()));
        
                break;
            }
            case command_buffer::command_type::clear:
            {
                const auto &clear = data.get<command_buffer::command_data::clear>();
                glEnable(GL_SCISSOR_TEST);
                GL_CHECK(glScissor(clear.region.offset.x, clear.region.offset.y, clear.region.extent.x, clear.region.extent.y));
                GL_CHECK(glClearColor(clear.color.r, clear.color.g, clear.color.b, clear.color.a));
                GL_CHECK(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
                GL_CHECK(glDisable(GL_SCISSOR_TEST));
                break;
            }
            case command_buffer::command_type::copy:
            {
                const auto &copy = data.get<command_buffer::command_data::copy>();
                if (copy.type == copy_type::undefined) {
                    STLA_LOG_ERROR("Invalid copy type (type=%d)", static_cast<int>(copy.type));
                    break;
                }

                if (copy.p_dst == nullptr) {
                    STLA_LOG_ERROR("Copy destination is null");
                    break;
                }
                
                if (copy.p_src == nullptr) {
                    STLA_LOG_ERROR("Copy source is null");
                    break;
                } 

                switch (copy.type)
                {
                    case copy_type::host_to_buffer:
                    {
                        buffer *p_buffer = reinterpret_cast<buffer *>(copy.p_dst);

                        if (!p_buffer->copy(copy.p_src, copy.size, copy.offset)) {
                            STLA_LOG_ERROR("Failed to copy %d bytes at offset %d from host to buffer",
                                copy.size, copy.offset);
                        }
                
                        break;
                    }
                    case copy_type::host_to_image:
                    {
                        image *p_image = reinterpret_cast<image *>(copy.p_dst);
    
                        if (copy.offset != 0) {
                            STLA_LOG_ERROR("Host to image copy with non-zero offset unsupported!");
                            return false;
                        }
    
                        if (!p_image->copy(copy.p_src, copy.size)) {
                            STLA_LOG_ERROR("Failed to copy %d bytes from host to image", copy.size);
                        }
                        
                        break;
                    }
                    default:
                    {
                        STLA_LOG_ERROR("Unhandled copy type (type=%d)", static_cast<int>(copy.type));
                        STLA_ASSERT_RELEASE(0);
                        break;
                    }
                }

                break;
            }
            case command_buffer::command_type::draw:
            {
                const auto &draw = data.get<command_buffer::command_data::draw>();
                if (draw.vertex_count == 0) {
                    STLA_LOG_ERROR("Invalid draw argument(s) (vertex_count=%d)", draw.vertex_count);
                    break;
                }

                STLA_ASSERT_RELEASE(draw.instances == 1);
                GL_CHECK(glDrawArrays(GL_TRIANGLES, 0, draw.vertex_count));

                break;
            }
            case command_buffer::command_type::draw_indexed:
            {
                const auto &draw = data.get<command_buffer::command_data::draw_indexed>();
                if (draw.index_count == 0) {
                    STLA_LOG_ERROR("Invalid draw argument(s) (index_count=%d)", draw.index_count);
                    break;
                }

                if (draw.instances == 1) {
                    GL_CHECK(glDrawElements(GL_TRIANGLES, draw.index_count, GL_UNSIGNED_INT, 0));
                }
                else {
                    GL_CHECK(glDrawElementsInstanced(
                        GL_TRIANGLES, 
                        draw.index_count, 
                        GL_UNSIGNED_INT, 
                        0, 
                        draw.instances));
                }

                break;
            }
            default: STLA_LOG_ERROR("Unknown command: %d\n", static_cast<int>(data.get_type()));
        }
    }

    return true;
}

uint32_t api::get_program(uint32_t vertex_shader, uint32_t fragment_shader)
{
    uint32_t program = 0;

    if (m_shader_programs.find(vertex_shader) != m_shader_programs.end() && 
        m_shader_programs[vertex_shader].find(fragment_shader) != m_shader_programs[vertex_shader].end()) {
        program = m_shader_programs[vertex_shader][fragment_shader];
    }

    if (program == 0) {
        program = glCreateProgram();
        GL_CHECK(glAttachShader(program, vertex_shader));
        GL_CHECK(glAttachShader(program, fragment_shader));
        GL_CHECK(glLinkProgram(program));
        
        int success;
        char info_log[512];
        
        GL_CHECK(glGetProgramiv(program, GL_LINK_STATUS, &success));
        if (!success) {
            GL_CHECK(glGetProgramInfoLog(program, 512, NULL, info_log));
            STLA_LOG_ERROR("ERROR::SHADER::PROGRAM::LINKING_FAILED : %s", info_log);
            STLA_ASSERT(0);
        }

        m_shader_programs[vertex_shader][fragment_shader] = program;
    }

    return program;
}

bool shader::create(shader_type type, const void *p_code)
{
    if (p_code == nullptr) {
        return false;
    }

    if (m_shader != 0) {
        destroy();
    }

    m_type   = type;
    m_shader = glCreateShader(to_gl_type(type));

    GL_CHECK(glShaderSource(m_shader, 1, (const GLchar *const *)&p_code, NULL));
    GL_CHECK(glCompileShader(m_shader));

    int success;
    char info_log[512];
    GL_CHECK(glGetShaderiv(m_shader, GL_COMPILE_STATUS, &success));
    if (!success) {
        GL_CHECK(glGetShaderInfoLog(m_shader, 512, NULL, info_log));
        STLA_LOG_ERROR("ERROR::SHADER::COMPILATION_FAILED : %s", info_log);
        m_shader = 0;
        m_type = shader_type::undefined;
        return false;
    }

    return true;
}

bool shader::create(shader_type type, const std::filesystem::path &file_path)
{
    if (!std::filesystem::exists(file_path)) {
        return false;
    }

    std::ifstream file(file_path.string()); 
    if (!file.is_open()) {
        return false;
    }

    std::stringstream buffer;
    buffer << file.rdbuf(); 

    std::string contents = buffer.str(); 

    return create(type, contents.c_str());
}

void shader::destroy()
{
    if (m_shader == 0) {
        return;
    }
    GL_CHECK(glDeleteShader(m_shader));
    m_shader = 0;
    m_type = shader_type::undefined;
}

bool shader::is_valid() const { return m_shader != 0; }

bool buffer::create(buffer_type type, size_t size, const void *p_data)
{
    if (type == buffer_type::undefined || size == 0) {
        return false;
    }

    if (m_buffer != 0) {
        destroy();
    }

    m_type = type;
    m_size = size;
    GL_CHECK(glGenBuffers(1, &m_buffer));
    GL_CHECK(glBindBuffer(to_gl_buffer_type(m_type), m_buffer));
    GL_CHECK(glBufferData(to_gl_buffer_type(m_type), size, p_data, GL_DYNAMIC_DRAW));
    GL_CHECK(glBindBuffer(to_gl_buffer_type(m_type), 0));
    return true;
}

void buffer::destroy()
{
    if (m_buffer == 0) {
        return;
    }

    GL_CHECK(glDeleteBuffers(1, &m_buffer));
    m_buffer = 0;
    m_type = buffer_type::undefined;
    m_size = 0;
    m_mapped = false;
}

bool buffer::is_valid() const
{
    return m_buffer != 0 && m_size != 0 && m_type != buffer_type::undefined;
}

bool buffer::copy(const void *src, size_t size, size_t offset) 
{
    if (!is_valid()) {
        return false;
    }

    if ((size + offset) > m_size) {
        STLA_LOG_ERROR("Copy overflow (size=%d) (offset=%d) (buffer_size=%d)",
            size, offset, m_size);
        return false;
    }

    uint8_t *p_dest = reinterpret_cast<uint8_t *>(map());

    p_dest = p_dest + offset;

    memcpy(p_dest, src, size);

    unmap();

    return true;
}

void *buffer::map()
{
    if (!is_valid()) {
        return nullptr;
    }
    
    if (m_mapped) {
        return nullptr;
    }

    GL_CHECK(glBindBuffer(to_gl_buffer_type(m_type), m_buffer));

    m_mapped = true;

    return glMapBuffer(to_gl_buffer_type(m_type), GL_READ_WRITE);
}

void buffer::unmap()
{
    if (!is_valid()) {
        return;
    }

    if (!m_mapped) {
        return;
    }

    GL_CHECK(glUnmapBuffer(to_gl_buffer_type(m_type)));

    GL_CHECK(glBindBuffer(to_gl_buffer_type(m_type), 0));

    m_mapped = false;
}

bool image::create(const image_info &info)
{
    if (info.type == image_type::undefined) {
        return false;
    }

    if (info.format == pixel_format::undefined) {
        return false;
    }

    if (info.size.x == 0 || info.size.y == 0) {
        return false;
    }

    if (m_image != 0) {
        destory();
    }

    m_info = info;
    GL_CHECK(glCreateTextures(GL_TEXTURE_2D, 1, &m_image));
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_image));

    switch (m_info.type)
    {
        case image_type::color_attachment:
        case image_type::texture:
        {
            // glTextureStorage2D(m_image, 1, GL_RGBA8, m_info.size.x, m_info.size.y);
            GL_CHECK(glTextureStorage2D(m_image, 1, to_gl_internal_format(m_info.format), m_info.size.x, m_info.size.y));
            break;
        }
        case image_type::depth_attachment: 
        {
            GL_CHECK(glTextureStorage2D(m_image, 1, to_gl_internal_format(m_info.format), m_info.size.x, m_info.size.y));
            break;
        }
        default:
        {
            STLA_LOG_ERROR("Unhandled image type (type=%d)", static_cast<int>(m_info.type));
            destory();
            STLA_ASSERT(0);
            return false;
        }
    }

    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, to_gl_type(info.filter)));
    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, to_gl_type(info.filter)));
    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, to_gl_type(info.wrap)));
    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, to_gl_type(info.wrap)));

    m_handle = glGetTextureHandleARB(m_image);
    STLA_ASSERT_RELEASE(m_handle != 0);
    GL_CHECK(glMakeTextureHandleResidentARB(m_handle));

    return true;
}

bool image::create(const image_info &info, const void *p_data, size_t size)
{
    if (p_data == nullptr || size == 0) {
        return false;
    }

    if (!create(info)) {
        return false;
    }

    if (!copy(p_data, size)) {
        destory();
        return false;
    }

    return true;
}

bool image::create(const std::filesystem::path &file_path, sampler_wrap_type wrap, sampler_filter_type filter)
{
    if (!std::filesystem::exists(file_path)) {
        return false;
    }

    const int channels = 4;
    int w = 0;
    int h = 0;
    int c = 0;

    unsigned char *p_data = nullptr; 
    p_data = stbi_load(file_path.string().c_str(), &w, &h, &c, STBI_rgb_alpha);

    if (p_data == nullptr) {
        return false;
    }

    image_info info = image_info(image_type::texture, pixel_format::r8g8b8a8, wrap, filter, ivec2(w, h));

    bool success = create(info);

    if (!success) {
        destory();
    }
    else {
        success = copy(p_data, w * h * c);
    }

    stbi_image_free(p_data);

    return success;
}

void image::destory()
{
    if (!is_valid()) {
        return;
    }

    GL_CHECK(glMakeTextureHandleNonResidentARB(m_handle));
    GL_CHECK(glDeleteTextures(1, &m_image));

    m_image = 0;
    m_handle = 0;
    m_info = image_info();
}

bool image::is_valid() const
{
    return (
        m_image != 0 &&
        m_handle != 0 &&
        m_info.type != image_type::undefined &&
        m_info.format != pixel_format::undefined &&
        m_info.size.x != 0 &&
        m_info.size.y != 0
    );
}

bool image::copy(const void *src, size_t size)
{
    if (!is_valid()) {
        return false;
    } 

    uint32_t image_size_bytes = m_info.size.x * m_info.size.y * get_pixel_size(m_info.format);
    
    if (size > image_size_bytes) {
        STLA_LOG_ERROR("Image copy overflow (copy_size=%d) (image_size=%d)",
            size, image_size_bytes);
        return false;
    }

    GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_image));
    GL_CHECK(glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_info.size.x, m_info.size.y, to_gl_format(m_info.format), to_gl_data_type(m_info.format), src));
    GL_CHECK(glGenerateMipmap(GL_TEXTURE_2D));

    return true;
}

bool command_buffer::execute()
{
    return api::submit(*this);
}

void command_buffer::set_state(const draw_state &state)
{
    command_data::set_draw_state data;
    data.state = state;
    m_command_queue.push(data);
}

void command_buffer::begin_rendering(const region_2d &region)
{
    command_data::begin_rendering data;
    data.region = region;
    m_command_queue.push(data);
}

void command_buffer::begin_rendering(const image &color_attachment, const image &depth_attachment, const region_2d &region)
{
    command_data::begin_rendering data;
    data.color_attachments.push_back(&color_attachment);
    data.p_depth_attachment = &depth_attachment;
    data.region = region;
    m_command_queue.push(data);
}

void command_buffer::end_rendering()
{
    command_data::end_rendering data;
    m_command_queue.push(data);
}

void command_buffer::bind(const shader &vertex_shader, const shader &fragment_shader)
{
    command_data::bind_program data;
    data.p_vertex = &vertex_shader;
    data.p_fragment = &fragment_shader;
    m_command_queue.push(data);
}

void command_buffer::bind(const buffer &buffer)
{
    command_data::bind_buffer data;
    data.p_buffer = &buffer;
    m_command_queue.push(data);
}

void command_buffer::bind(const vertex_attribute_descriptor &desc, const buffer &buffer)
{
    command_data::bind_buffer data;
    data.layout.push_back(desc);
    data.p_buffer = &buffer;
    m_command_queue.push(data);
}

void command_buffer::bind(const std::vector<vertex_attribute_descriptor> &layout, const buffer &buffer)
{
    command_data::bind_buffer data;
    data.layout = layout;
    data.p_buffer = &buffer;
    m_command_queue.push(data);
}
    
void command_buffer::bind_uniform(uint32_t loc, const buffer &buffer)
{
    command_data::bind_uniform data;
    data.location = loc;
    data.p_buffer = &buffer;
    m_command_queue.push(data);
}

void command_buffer::clear(const color &color, const region_2d &region)
{
    command_data::clear data;
    data.color = color;
    data.region = region;
    m_command_queue.push(data);
}

void command_buffer::copy(buffer &dst, const void *src, size_t size, size_t offset)
{
    command_data::copy data;
    data.p_dst = reinterpret_cast<void *>(&dst);
    data.p_src = src;
    data.size = size;
    data.offset = offset;
    data.type = copy_type::host_to_buffer;
    m_command_queue.push(data);
}

void command_buffer::copy(void *dst, const buffer &src, size_t size, size_t offset)
{
    command_data::copy data;
    data.p_dst = dst;
    data.p_src = reinterpret_cast<const void *>(&src);
    data.size = size;
    data.offset = offset;
    data.type = copy_type::buffer_to_host;
    m_command_queue.push(data);
}

void command_buffer::draw(uint32_t vertex_count, uint32_t instances)
{
    command_data::draw data;
    data.vertex_count = vertex_count;
    data.instances = instances;
    m_command_queue.push(data);
}

void command_buffer::draw_indexed(uint32_t index_count, uint32_t instances)
{
    command_data::draw_indexed data;
    data.index_count = index_count;
    data.instances = instances;
    m_command_queue.push(data);
}

} // namespace gfx
} // namespace stla