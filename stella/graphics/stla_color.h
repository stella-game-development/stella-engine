#pragma once

#include <glm/glm.hpp>

#include <stdint.h>

namespace stla
{

namespace color
{

template<typename T> T red();
template<typename T> T green();
template<typename T> T blue();

glm::vec3 hex_to_rgb(uint32_t hex);

} // namespace color
} // namespace stla
