#pragma once

#include "stla_utils.h"

#include <glm/glm.hpp>

#include <stdint.h>

#include <string>
#include <vector>
#include <unordered_map>

namespace stla
{
namespace gfx
{

struct bone
{
    struct weight
    {
        int32_t vertex_id{ -1 };
        float value{ 0.0f };
    };

    int32_t id{ -1 };
    std::string name;
    bone *p_parent{ nullptr };
    glm::mat4 offset = glm::mat4(1.0f);
    glm::mat4 local_transformation = glm::mat4(1.0f);
    std::unordered_map<std::string, std::vector<weight>> mesh_vertex_weights;
    std::vector<bone> children;
};

struct skeleton
{
public:

    skeleton() = default;
    skeleton(const utils::assimp::scene_loader &loader);

    void load(const utils::assimp::scene_loader &loader);
    void clear();
    const bone *get_bone(const std::string &name) const;
    
    template<typename Func>
    void preorder(Func visitor) const;

private:

    template<typename Func>
    void preorder_traversal(const bone &b, uint32_t depth, Func visitor) const;
    void process_node(const aiNode *p_node, const glm::mat4 &parent_transform, const aiScene *p_scene);
    void process_mesh(const aiMesh *p_mesh, const aiScene *p_scene); 
    
    struct bone_node
    {
        bone b;
        std::vector<bone> children;
    };

    bone m_root;
    std::unordered_map<std::string, bone *> m_name_to_bone_map;
    std::unordered_map<std::string, int32_t> m_name_to_bone_id_map;
};

template<typename Func>
void skeleton::preorder(Func visitor) const
{
    preorder_traversal(m_root, 0, visitor);
}

template<typename Func>
void skeleton::preorder_traversal(const bone &b, uint32_t depth, Func visitor) const
{
    if constexpr (std::invocable<Func, const bone &>) {
        visitor(b);
    }
    else if constexpr (std::invocable<Func, const bone &, uint32_t>) {
        visitor(b, depth);
    }

    for (auto &child : b.children) {
        preorder_traversal(child, depth + 1, visitor);
    }
}
    
} // namespace gfx
} // namespace stla
