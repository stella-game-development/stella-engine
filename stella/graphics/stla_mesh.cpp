#include "stla_mesh.h"
#include "stla_log.h"

#define TINYOBJLOADER_IMPLEMENTATION // define this in only *one* .cc
// Optional. define TINYOBJLOADER_USE_MAPBOX_EARCUT gives robust trinagulation. Requires C++11
#define TINYOBJLOADER_USE_MAPBOX_EARCUT
#include "tiny_obj_loader.h"
#include "glm/glm.hpp"

#include <string.h>

#include <filesystem>
#include <vector>
#include <initializer_list>
#include <unordered_map>

namespace stla
{
namespace gfx
{

bool mesh::load(const std::filesystem::path &file_path)
{
    if (!std::filesystem::exists(file_path)) {
        return false;
    }
    
    // Check file extension
    if (file_path.extension() != ".obj") {
        STLA_LOG_ERROR("Invalid file type: %s", file_path.string().c_str());
        return false;
    }

    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string warn, err;

    std::string p = file_path.string();
    if (!tinyobj::LoadObj(&attrib, &shapes, nullptr, &warn, &err, p.c_str())) {
        STLA_LOG_ERROR("Unable to load obj file %s: warn: %s err: %s", 
            file_path.string().c_str(), warn.c_str(), err.c_str());
        return false;
    }

    clear();

    m_vertex_buffers.resize(1);

    auto &vertex_data = m_vertex_buffers.front().data;
    auto &vertex_layout = m_vertex_buffers.front().layout;

    uint32_t loc = 0;
    if (attrib.vertices.size() != 0) {
        vertex_layout.push_back(gfx::vertex_attribute_descriptor(loc, 3, gfx::vertex_attribute_type::f, false));
        loc++;
    }

    if (attrib.normals.size() != 0) {
        vertex_layout.push_back(gfx::vertex_attribute_descriptor(loc, 3, gfx::vertex_attribute_type::f, false));
        loc++;
    }

    if (attrib.texcoords.size() != 0) {
        vertex_layout.push_back(gfx::vertex_attribute_descriptor(loc, 2, gfx::vertex_attribute_type::f, false));
        loc++;
    }

    std::unordered_map<std::string, uint32_t> unique_vertices;

    vertex_data.reserve(
        (attrib.vertices.size() * sizeof(float)) +
        (attrib.normals.size() * sizeof(float)) + 
        (attrib.texcoords.size() * sizeof(float))
    );

    m_indices.reserve(attrib.vertices.size() / 3);

    for (const auto &shape : shapes) {
        for (const auto &index : shape.mesh.indices) {
            std::vector<float> vertex;
            vertex.reserve(8);

            // Position
            if (attrib.vertices.size() != 0) {
                vertex.push_back(attrib.vertices[3 * index.vertex_index + 0]);
                vertex.push_back(attrib.vertices[3 * index.vertex_index + 1]);
                vertex.push_back(attrib.vertices[3 * index.vertex_index + 2]);
            }

            // Normals
            if (attrib.normals.size() != 0) {
                vertex.push_back(attrib.normals[3 * index.normal_index + 0]);
                vertex.push_back(attrib.normals[3 * index.normal_index + 1]);
                vertex.push_back(attrib.normals[3 * index.normal_index + 2]);
            }

            // UVs
            if (attrib.texcoords.size() != 0) {
                vertex.push_back(attrib.texcoords[2 * index.texcoord_index + 0]);
                vertex.push_back(attrib.texcoords[2 * index.texcoord_index + 1]);
            }

            // TODO a more efficient way to do this
            std::string vertex_string((const char *)vertex.data(), sizeof(float) * vertex.size());

            if (unique_vertices.count(vertex_string) == 0) {
                unique_vertices[vertex_string] = (uint32_t)((vertex_data.size() / sizeof(float)) / vertex.size()); // store vertex position in vertices vector
                for (float value : vertex) {
                    uint8_t *p_value = reinterpret_cast<uint8_t *>(&value);
                    vertex_data.push_back(p_value[0]);
                    vertex_data.push_back(p_value[1]);
                    vertex_data.push_back(p_value[2]);
                    vertex_data.push_back(p_value[3]);
                }
            }

            m_indices.push_back(unique_vertices[vertex_string]);    
        }
    }

    m_file_path = file_path;
    m_name = file_path.filename().string();

    return true;
}

bool mesh::load(const aiMesh *p_mesh, const glm::mat4 &transform)
{
    if (p_mesh == nullptr) {
        return false;
    }

    clear();

    if (p_mesh->mVertices == nullptr) {
        return false;
    }

    auto compute_global_aabb = [](
        const glm::vec3 &min_aabb, 
        const glm::vec3 &max_aabb, 
        const glm::mat4 &transform, 
        glm::vec3 &global_min, 
        glm::vec3 &global_max) {
            // Define the 8 corners of the local AABB
            glm::vec3 corners[8] = {
                {min_aabb.x, min_aabb.y, min_aabb.z},
                {max_aabb.x, min_aabb.y, min_aabb.z},
                {min_aabb.x, max_aabb.y, min_aabb.z},
                {min_aabb.x, min_aabb.y, max_aabb.z},
                {max_aabb.x, max_aabb.y, min_aabb.z},
                {max_aabb.x, min_aabb.y, max_aabb.z},
                {min_aabb.x, max_aabb.y, max_aabb.z},
                {max_aabb.x, max_aabb.y, max_aabb.z}
            };

            // Initialize global min and max with extreme values
            global_min = glm::vec3(std::numeric_limits<float>::max());
            global_max = glm::vec3(std::numeric_limits<float>::lowest());

            // Transform each corner and update global AABB
            for (int i = 0; i < 8; ++i) {
                // Transform the corner by the global transformation matrix
                glm::vec4 transformedCorner = transform * glm::vec4(corners[i], 1.0f);
                glm::vec3 globalCorner = glm::vec3(transformedCorner);

                // Update global min and max
                global_min.x = glm::min(global_min.x, globalCorner.x);
                global_min.y = glm::min(global_min.y, globalCorner.y);
                global_min.z = glm::min(global_min.z, globalCorner.z);

                global_max.x = glm::max(global_min.x, globalCorner.x);
                global_max.y = glm::max(global_max.y, globalCorner.y);
                global_max.z = glm::max(global_max.z, globalCorner.z);


                //global_min = glm::min(global_min, globalCorner);
                //global_max = glm::max(global_max, globalCorner);
            }
    };

    math::aabb3d aabb;
    aabb.max.x = p_mesh->mAABB.mMax.x;
    aabb.max.y = p_mesh->mAABB.mMax.y;
    aabb.max.z = p_mesh->mAABB.mMax.z;

    aabb.min.x = p_mesh->mAABB.mMin.x;
    aabb.min.y = p_mesh->mAABB.mMin.y;
    aabb.min.z = p_mesh->mAABB.mMin.z;

    compute_global_aabb(aabb.min, aabb.max, transform, m_global_aabb.min, m_global_aabb.max);

    m_transform = transform;
    m_vertex_buffers.resize(1);

    auto &vertex_data = m_vertex_buffers.front().data;
    auto &vertex_layout = m_vertex_buffers.front().layout;

    vertex_data.reserve(p_mesh->mNumVertices * 8 * sizeof(float));

    uint32_t loc = 0;
    vertex_layout.push_back(gfx::vertex_attribute_descriptor(loc, 3, gfx::vertex_attribute_type::f, false));
    loc++;

    if (p_mesh->HasNormals()) {
        vertex_layout.push_back(gfx::vertex_attribute_descriptor(loc, 3, gfx::vertex_attribute_type::f, false));
        loc++;
    }

    if (p_mesh->HasTextureCoords(0)) {
        vertex_layout.push_back(gfx::vertex_attribute_descriptor(loc, 2, gfx::vertex_attribute_type::f, false));
        loc++;
    }

    auto push_float = [](float value, std::vector<uint8_t> &vertex_data) {
        uint8_t *p_value = reinterpret_cast<uint8_t *>(&value);
        vertex_data.push_back(p_value[0]);
        vertex_data.push_back(p_value[1]);
        vertex_data.push_back(p_value[2]);
        vertex_data.push_back(p_value[3]);
    };

    // Vertices
    for (uint32_t i = 0; i < p_mesh->mNumVertices; i++) {
        push_float(p_mesh->mVertices[i].x, vertex_data);
        push_float(p_mesh->mVertices[i].y, vertex_data);
        push_float(p_mesh->mVertices[i].z, vertex_data);
    
        if (p_mesh->HasNormals()) {
            push_float(p_mesh->mNormals[i].x, vertex_data);
            push_float(p_mesh->mNormals[i].y, vertex_data);
            push_float(p_mesh->mNormals[i].z, vertex_data);
        }

        if (p_mesh->HasTextureCoords(0)) {
            push_float(p_mesh->mTextureCoords[0][i].x, vertex_data);
            push_float(p_mesh->mTextureCoords[0][i].y, vertex_data);
        }
    }

    // Indices
    m_indices.reserve(p_mesh->mNumFaces * 3);
    for (int i = 0; i < p_mesh->mNumFaces; i++){
        aiFace face = p_mesh->mFaces[i];
        for (int j = 0; j < face.mNumIndices; j++) {
            m_indices.push_back(face.mIndices[j]);
        }
    }  

    // TODO Materials

    m_name = p_mesh->mName.C_Str();

    return true;
}

void mesh::push_vertex_buffer(const vertex_buffer_layout &layout, const std::vector<uint8_t> &data)
{
    m_vertex_buffers.push_back(vertex_buffer());
    auto &vertex_buffer = m_vertex_buffers.back();
    vertex_buffer.layout = layout;
    vertex_buffer.data = data;
}

mesh::vertex_buffer &mesh::create_vertex_buffer()  
{
    m_vertex_buffers.push_back(vertex_buffer());
    return m_vertex_buffers.back();
}

void mesh::clear()
{
    m_file_path.clear();
    m_name.clear();
    m_vertex_buffers.clear();
    m_transform = glm::mat4(1.0f);
}

uint32_t mesh::get_vertex_count() const
{
    if (m_vertex_buffers.size() == 0) {
        return 0;
    }

    return m_vertex_buffers[0].data.size() / m_vertex_buffers[0].get_vertex_stride();
}

mesh mesh::cube()
{
    static const std::vector<float> cube_vertices = {
        // positions          // normals           // texture coords
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  0.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,

        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,

        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
        -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  1.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
        -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  0.0f,
        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  1.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
         0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,

        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f
    };

    static mesh cube_mesh;

    std::vector<vertex_attribute_descriptor> layout;
    std::vector<uint8_t> data;

    layout.push_back(vertex_attribute_descriptor(0, 3, vertex_attribute_type::f, false));
    layout.push_back(vertex_attribute_descriptor(1, 3, vertex_attribute_type::f, false));
    layout.push_back(vertex_attribute_descriptor(2, 2, vertex_attribute_type::f, false));

    data.resize(cube_vertices.size() * sizeof(float));
    std::memcpy(data.data(), cube_vertices.data(), data.size());

    cube_mesh.push_vertex_buffer(layout, data);

    return cube_mesh;
}

std::ostream &operator << (std::ostream &os, const mesh &mesh)
{
    os << "Tri Mesh: " << mesh.m_name << std::endl;
    os << "  File Path : " << mesh.m_file_path << std::endl;
    os << "  Index Buffer" << std::endl;
    os << "    size  : " << mesh.m_indices.size() << std::endl;
    os << "    bytes : " << mesh.m_indices.size() * sizeof(uint32_t) << std::endl;
    os << "  Vertex Buffers" << std::endl;

    for (const auto &vertex_buffer : mesh.m_vertex_buffers) {
        os << "    Data" << std::endl;
        os << "      bytes : " << vertex_buffer.data.size() << std::endl;
        
        os << "  Vertex Layout" << std::endl;
        for (const auto &attrib : vertex_buffer.layout) {
            os << "    location " << attrib.loc << std::endl;
            os << "      count     : " << attrib.count << std::endl;
            os << "      type      : ";
            switch (attrib.type)
            {
                case gfx::vertex_attribute_type::f: 
                {
                    os << "Float (" << static_cast<int>(attrib.type) << ")" << std::endl;
                    break;
                }
                case gfx::vertex_attribute_type::mat4: 
                {
                    os << "Mat4 (" << static_cast<int>(attrib.type) << ")" << std::endl;
                    break;
                }
                default:
                {
                    os << "Unknown (" << static_cast<int>(attrib.type) << ")" << std::endl;
                    break;
                }
            }
            os << "      instanced : " << (attrib.instanced ? "TRUE" : "FALSE") << std::endl;
        }
    }

    return os;
}

} // namespace gfx
} // namespace geri
