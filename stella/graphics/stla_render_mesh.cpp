#include "stla_render_mesh.h"
#include "stla_mesh.h"
#include "stla_gfx.h"

namespace stla
{
namespace gfx
{

render_mesh::render_mesh(const mesh &mesh)
{
    create(mesh);
}

bool render_mesh::create(const mesh &mesh)
{
    destroy();

    m_index_count = mesh.get_indices().size();
    //STLA_ASSERT_RELEASE(m_index_count != 0);

    bool success = true;
    if (m_index_count != 0) {
        success = m_index_buffer.create(buffer_type::index, mesh.get_indices());
    }

    if (success) {
        for (const auto &buffer : mesh.get_vertex_buffers()) {
            if (!success) {
                break;
            }

            if (buffer.data.size() == 0) {
                continue;
            }
            render_buffer rb;
            rb.layout = buffer.layout;
            success = rb.buffer.create(buffer_type::vertex, buffer.data);
            m_render_buffers.push_back(rb);
        }
    }

    m_vertex_count = mesh.get_vertex_count();

    if (!success) {
        destroy();
    }

    return success;
}

void render_mesh::destroy()
{
    m_index_count = 0;
    m_vertex_count = 0;
    m_index_buffer.destroy();
    for (auto &vertex_buffer : m_render_buffers) {
        vertex_buffer.buffer.destroy();
        vertex_buffer.layout.clear();
    }
}

void render_mesh::draw(command_buffer &command_buffer) const
{
    for (const auto &render_buffer : m_render_buffers) {
        command_buffer.bind(render_buffer.layout, render_buffer.buffer);
    }

    if (m_index_count != 0) {
        command_buffer.bind(m_index_buffer);
        command_buffer.draw_indexed(m_index_count);
    }
    else {
        command_buffer.draw(m_vertex_count);
    }
}

} // namespace gfx
} // namespace stla
