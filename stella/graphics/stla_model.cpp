#include "stla_model.h"

#include <glm/gtc/matrix_transform.hpp>

namespace stla
{
namespace gfx
{

void model::load(const utils::assimp::scene_loader &loader)
{
    clear();

    if (loader.get_root() == nullptr) {
        STLA_LOG_ERROR("Scene has no root node: %s", loader.get_file_path());
        return;
    }

    m_file_path = loader.get_file_path();

    loader.for_each([&](const aiMesh *p_mesh, const glm::mat4 &node_transform, const aiScene *p_scene) { process_mesh(p_mesh, node_transform, p_scene); });
}

void model::clear()
{
    m_file_path.clear();
    m_meshes.clear();
}

glm::mat4 model::get_normalization_transform(float target_min, float target_max) const
{
    if (m_meshes.size() == 0) {
        return glm::mat4(1.0f);
    }

    math::aabb3d aabb;
    aabb.min = m_meshes.front().get_global_aabb().min;
    aabb.max = m_meshes.front().get_global_aabb().max;

    for (int i = 1; i < m_meshes.size(); i++) {
        auto mesh_aabb = m_meshes[i].get_global_aabb();

        aabb.min.x = glm::min(aabb.min.x, mesh_aabb.min.x);
        aabb.min.y = glm::min(aabb.min.y, mesh_aabb.min.y);
        aabb.min.z = glm::min(aabb.min.z, mesh_aabb.min.z);
        
        aabb.max.x = glm::max(aabb.max.x, mesh_aabb.max.x);
        aabb.max.y = glm::max(aabb.max.y, mesh_aabb.max.y);
        aabb.max.z = glm::max(aabb.max.z, mesh_aabb.max.z);
    }

    const float width  = aabb.max.x - aabb.min.x;
    const float height = aabb.max.y - aabb.min.y;
    const float depth  = aabb.max.z - aabb.min.z;

    const float target_range = target_max - target_min;
    const float scale_factor = target_range / glm::max(width, glm::max(height, depth));
    const glm::vec3 center = (aabb.min + aabb.max) * 0.5f;
    const glm::vec3 target_center = glm::vec3((target_min + target_max) * 0.5f);
    const glm::vec3 translation_offset = target_center - center;

    glm::mat4 transform(1.0f);
    transform = glm::scale(transform, glm::vec3(scale_factor));
    transform = glm::translate(transform, translation_offset);

    return transform;
}

void model::process_mesh(const aiMesh *p_mesh, const glm::mat4 &node_transform, const aiScene *p_scene)
{
    if (p_mesh == nullptr || p_scene == nullptr) {
        STLA_LOG_WARN("Model mesh or scene is null");
        return;
    }
    
    m_meshes.push_back(mesh());
    mesh &mesh = m_meshes.back();
    
    if (!mesh.load(p_mesh, node_transform)) {
        STLA_LOG_ERROR("Failed to load aiMesh");
        return;
    }

    texture_info diffuse_texture_info;

    if (p_mesh->mMaterialIndex >= 0) {
        aiMaterial *p_material = p_scene->mMaterials[p_mesh->mMaterialIndex];
        const uint32_t diffuse_texture_count = p_material->GetTextureCount(aiTextureType_DIFFUSE);
        if (diffuse_texture_count > 1) {
            STLA_LOG_ERROR("Model has unhandled diffuse texture count (filename=%s) (count=%d)", 
                m_file_path.filename().string().c_str(), diffuse_texture_count);
            STLA_ASSERT_RELEASE(0); // TODO not sure how to deal with a mesh having multiple of one type of texture
        }

        if (diffuse_texture_count != 0) {
            aiString str;
            p_material->GetTexture(aiTextureType_DIFFUSE, 0, &str);

            diffuse_texture_info.type = texture_type::diffuse;
            diffuse_texture_info.file_path = m_file_path.parent_path().append(str.C_Str());
            STLA_ASSERT(std::filesystem::exists(diffuse_texture_info.file_path));
        }
    }
    
    m_diffuse_maps.push_back(diffuse_texture_info);
}
}
}

