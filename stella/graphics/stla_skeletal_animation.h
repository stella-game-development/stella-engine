#pragma once

#include "stla_animation_data.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>

#include <stdint.h>

#include <stack>

namespace stla
{
namespace gfx
{

class animation
{
public:
    
    struct key_position
    {
        float time_stamp{ 0.0f };
        glm::vec3 position;
        key_position() = default;
        key_position(float ts, const glm::vec3 &p) : time_stamp(ts), position(p) { }
    };

    struct key_rotation
    {
        float time_stamp{ 0.0f };
        glm::quat orientation;
        key_rotation() = default;
        key_rotation(float ts, const glm::quat &o) : time_stamp(ts), orientation(o) { }
    };

    struct key_scale
    {
        float time_stamp{ 0.0f };
        glm::vec3 scale;
        key_scale() = default;
        key_scale(float ts, const glm::vec3 &s) : time_stamp(ts), scale(s) { }
    };

    struct channel
    {
        std::string name;
        std::vector<key_position> positions;
        std::vector<key_rotation> rotations;
        std::vector<key_scale> scales;

        int32_t get_position_index(float animation_time) const;
        int32_t get_rotation_index(float animation_time) const;
        int32_t get_scale_index(float animation_time) const;
        glm::mat4 get_local_transform(float animation_time) const; 
        
    private:
    
        float get_scale_factor(float last_time_stamp, float next_time_stamp, float animation_time) const;
        glm::mat4 interpolate_position(float animation_time) const;
        glm::mat4 interpolate_rotation(float animation_time) const;
        glm::mat4 interpolate_scaling(float animation_time) const;
    };

    animation() = default;
    animation(const aiAnimation &animation);

    const std::string &get_name() const { return m_name; }
    float get_duration() const { return m_duration; }
    float get_ticks_per_second() const { return m_ticks_per_second; }
    const channel *find_channel(const std::string &name) const;

private:
    std::string m_name;
    float m_duration{ 0.0f };
    float m_ticks_per_second{ 0.0f };
    std::vector<channel> m_channels;
};

class animator
{
public:

    void set_animation(const animation &anim, const skeleton &skel, uint32_t bone_count, const glm::mat4 &global_inverse_transform=glm::mat4(1.0f));
    void update(float dt);
    const std::vector<glm::mat4> &get_final_bone_matrices() const { return m_final_bone_matrices; }

private:
    void calculate_bone_transforms();

    float m_current_time{ 0.0f };
    float m_delta_time{ 0.0f };
    glm::mat4 m_global_inverse_transform = glm::mat4(1.0f);
    const animation *mp_animation{ nullptr };
    const skeleton *mp_skeleton{ nullptr };
    std::vector<glm::mat4> m_final_bone_matrices;
};

} // namespace gfx
} // namespace stla

