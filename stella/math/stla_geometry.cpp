#include "stla_geometry.h"
#include "stla_mesh.h"

namespace stla
{
namespace math
{

bool aabb3d::intersects(const glm::vec3 &point)
{
    return (
        point.x > min.x && point.x < max.x &&
        point.y > min.y && point.y < max.y &&
        point.z > min.z && point.z < max.z
    );
}

bool aabb2d::intersects(const glm::vec2 &point)
{
    return (
        point.x > min.x && point.x < max.x &&
        point.y > min.y && point.y < max.y
    );
}

glm::vec3 centroid(const gfx::mesh &mesh)
{
    glm::vec3 total_weighted_centroid(0.0f);
    float total_area = 0.0f;

    // TODO is this correct?
    auto calc_tri_area = [](const float *p_v0, const float *p_v1, const float *p_v2) {
        float x1 = p_v0[0];
        float y1 = p_v0[1];
        float z1 = p_v0[2];
        float x2 = p_v1[0];
        float y2 = p_v1[1];
        float z2 = p_v1[2];
        float x3 = p_v2[0];
        float y3 = p_v2[1];
        float z3 = p_v2[2];

        // Vector v1 = P2 - P1
        float v1x = x2 - x1;
        float v1y = y2 - y1;
        float v1z = z2 - z1;

        // Vector v2 = P3 - P1
        float v2x = x3 - x1;
        float v2y = y3 - y1;
        float v2z = z3 - z1;

        // Cross product v1 x v2
        float cross_x = v1y * v2z - v1z * v2y;
        float cross_y = v1z * v2x - v1x * v2z;
        float cross_z = v1x * v2y - v1y * v2x;

        // Magnitude of cross product
        float cross_mag = std::sqrt(cross_x * cross_x + cross_y * cross_y + cross_z * cross_z);

        // Area of the triangle
        return 0.5f * cross_mag;
    };

    mesh.for_each([&](const gfx::mesh::vertex_p3 &v0, const gfx::mesh::vertex_p3 &v1, const gfx::mesh::vertex_p3 &v2) {
        glm::vec3 triangle_centroid(0.0f);
        triangle_centroid.x = v0.position[0] + v1.position[0] + v2.position[0];
        triangle_centroid.y = v0.position[1] + v1.position[1] + v2.position[1];
        triangle_centroid.z = v0.position[2] + v1.position[2] + v2.position[2];
        triangle_centroid /= 3.0f;

        float triangle_area = calc_tri_area(&v0.position[0], &v1.position[0], &v2.position[0]);

        total_weighted_centroid += triangle_centroid * triangle_area;
        total_area += triangle_area;
    });

    return total_weighted_centroid / total_area; 
}

} // namespace geometry
} // namespace stla
