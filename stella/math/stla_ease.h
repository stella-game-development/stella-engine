#pragma once

namespace stla
{
namespace math
{

float ease_out_circ(float t);

} // namespace math
} // namespace stla
