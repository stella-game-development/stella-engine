#pragma once

#include <glm/glm.hpp>

namespace stla
{
namespace gfx
{
class mesh;
}

namespace math
{
    
struct aabb3d
{
    glm::vec3 min{ 0.0f };
    glm::vec3 max{ 0.0f };

    bool intersects(const glm::vec3 &point);
};

struct aabb2d
{
    glm::vec2 min{ 0.0f };
    glm::vec2 max{ 0.0f };

    bool intersects(const glm::vec2 &point);
};

struct triangle3d
{
    glm::vec3 p0{ 0.0f };
    glm::vec3 p1{ 0.0f };
    glm::vec3 p2{ 0.0f };
};

glm::vec3 centroid(const gfx::mesh &mesh);

} // namespace geometry
} // namespace stla
