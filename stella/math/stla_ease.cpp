#include "stla_ease.h"

#include <glm/glm.hpp>

namespace stla
{
namespace math
{

float ease_out_circ(float t)
{
    return glm::sqrt(1.0f - glm::pow(t - 1.0f, 2));
}

} // namespace math
} // namespace stla
