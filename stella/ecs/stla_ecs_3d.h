#pragma once

#include "stla_ecs_core.h"
#include "stla_mesh.h"
#include "stla_render_mesh.h"
#include "stla_gfx.h"

#include <filesystem>
#include <functional>
#include <set>

namespace stla
{
namespace ecs_3d
{

//-----------------------------------------------------------------------------
// EVENTS
//-----------------------------------------------------------------------------

namespace event
{

} // namespace event

//-----------------------------------------------------------------------------
// COMPONENTS
//-----------------------------------------------------------------------------

namespace component
{

struct shader
{
    gfx::shader s;

    shader(gfx::shader_type type, const void *p_code);
    shader(gfx::shader_type type, const std::filesystem::path &file_path);
};

struct material
{
    flecs::entity vertex_shader;
    flecs::entity fragment_shader;

    material(flecs::world& world, const std::string& vertex_name, const std::string& fragment_name);
};

struct uniform_buffer
{
    uint32_t loc{ 0 };
    gfx::buffer buffer;
    uniform_buffer(uint32_t binding_loc, size_t size, const void *p_data=nullptr);
    template<typename T>
    uniform_buffer(uint32_t binding_loc, const T &data) : uniform_buffer(binding_loc, sizeof(T), &data) { }
};

struct mesh
{
    gfx::mesh m;

    mesh(const gfx::mesh &mesh) : m(mesh) { }
};

struct renderable
{
    gfx::render_mesh render_mesh;

    renderable(flecs::world &world, const std::string &name);
};

struct mesh_renderer
{
    gfx::command_buffer command_buffer;
    gfx::region_2d render_region;
    gfx::color clear_color;

    mesh_renderer(
        const gfx::region_2d &render_region,
        const gfx::color &clear_color
    );
};

} // namespace component

//-----------------------------------------------------------------------------
// SYSTEMS/EVENT HANDLERS
//-----------------------------------------------------------------------------

namespace system
{

struct renderer
{
    renderer(flecs::world &world);
    static void shutdown(); 

private:
    static void begin_rendering(flecs::entity e, component::mesh_renderer &renderer);
    static void enqueue_commands(flecs::entity, component::mesh_renderer &renderer);
    static void end_rendering(flecs::entity, component::mesh_renderer &renderer);
    static void execute(flecs::entity, component::mesh_renderer &renderer);
    static inline flecs::query<const component::renderable> *sp_renderable_query = nullptr;
};

class event_handler : public ecs_core::ievent_handler
{
public:
};

} // namespace system

//-----------------------------------------------------------------------------
// MODULE
//-----------------------------------------------------------------------------

class module : public ecs_core::imodule
{
public:
    module(flecs::world &world);
    virtual void shutdown(flecs::world &world) override;
};

class utils
{
public:
    
    /** Register a uniform updater observer.
     * If an entity has both T and a component::uniform_buffer, the observer will
     * automatically copy T to the uniform buffer when T is set. 
     * 
     *  Example:
     * 
     * @code
     * e.set<T>({ data })
     * @endcode
     */
    template<typename T>
    static void register_uniform_updater(flecs::world &world)
    {
        const std::string type_name = typeid(T).name();
        if (s_registered_uniform_types.find(type_name) != s_registered_uniform_types.end()) {
            STLA_LOG_INFO("Uniform type already registerd: %s", type_name.c_str());
            return;
        }

        s_registered_uniform_types.insert(type_name);

        world.observer<const T>()
            .event(flecs::OnSet)
            .each([](flecs::entity e, const T &data) { 
                if (e.has<component::uniform_buffer>()) {
                    auto *p_uniform_buffer = e.get_mut<component::uniform_buffer>();
                    if (p_uniform_buffer == nullptr || !p_uniform_buffer->buffer.is_valid()) {
                        STLA_LOG_ERROR("Uniform buffer is invalid");
                        STLA_ASSERT(0);
                    }
                    else {
                        STLA_ASSERT(p_uniform_buffer->buffer.copy(&data, sizeof(T), 0));
                    }
                }
            });
    }
    
    /** Helper function to add both T and a component::uniform_buffer to an entity.
     * Also registers a uniform updater using the call: register_uniform_updater<T>(world);
     */
    template<typename T>
    static flecs::entity add_uniform_data(flecs::entity e, uint32_t loc, const std::string &name)
    {
        auto world = e.world();
        register_uniform_updater<T>(world);        
        world.set_scope(e);
        auto uniform_entity = world.entity(name.c_str())
            .child_of(e)
            .emplace<component::uniform_buffer>(0U, T())
            .add<T>();
        world.set_scope(0);
        return uniform_entity;
    }

    //
    // Helper function to 1) lookup a uniform entity within a parent's scope
    // 2) update T using provided 'update' function and 3) set T to trigger 
    // the uniform updater observer if one has been registered for T.
    // 
    // The function parameter must match the following signature:
    // 
    // void(*)(T&)
    // 
    template<typename T, typename Func>
    static void update_uniform_data(flecs::entity parent, const std::string &uniform_name, Func update)
    {
        auto world = parent.world();
        world.set_scope(parent);
        {
            auto uniform_entity = world.lookup(uniform_name.c_str());
            if (!uniform_entity || parent == uniform_entity) {
                STLA_LOG_ERROR("Uniform '%s' not found with in parent '%s' scope",
                    uniform_name.c_str(), parent.name().c_str());
                STLA_ASSERT(0);
            }
            else {
                T &data = uniform_entity.ensure<T>();
                update(data);
                uniform_entity.set<T>({ data });
            }
        }
        world.set_scope(0);   
    }

private:
    static inline std::set<std::string> s_registered_uniform_types;
};

} // namespace renderer
} // namespace stla