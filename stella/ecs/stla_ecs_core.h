#pragma once

#include "stla_event.h"
#include "stla_utils.h"

#include <flecs.h>
#include <glm/glm.hpp>
#include <SDL3/SDL.h>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>

#include <stdint.h>

namespace stla
{
namespace ecs_core
{

using frame_stage = flecs::entity_t;

static const frame_stage FRAME_STAGE_ON_STARTUP  = flecs::OnStart;
static const frame_stage FRAME_STAGE_PRE_FRAME   = flecs::PreFrame;
static const frame_stage FRAME_STAGE_PRE_UPDATE  = flecs::PreUpdate;
static const frame_stage FRAME_STAGE_ON_UPDATE   = flecs::OnUpdate;
static const frame_stage FRAME_STAGE_POST_UPDATE = flecs::PostUpdate;
static const frame_stage FRAME_STAGE_PRE_RENDER  = flecs::PreStore;
static const frame_stage FRAME_STAGE_ON_RENDER   = flecs::OnStore;
static const frame_stage FRAME_STAGE_POST_FRAME  = flecs::PostFrame;
static const frame_stage FRAME_STAGE_IMGUI       = flecs::PostFrame;

//-----------------------------------------------------------------------------
// EVENTS
//-----------------------------------------------------------------------------

struct event_tag { };

struct event_emitter
{
    event_emitter(event_emitter &&) noexcept = default;
    event_emitter(flecs::world &world);

    event_emitter &operator=(event_emitter &&) noexcept = default;

    template<typename E, typename T>
    void emit();

    template<typename E, typename T>
    void emit(const T &event_data);

    template<typename E, typename T>
    void enqueue();
    
    template<typename E, typename T>
    void enqueue(const T &event_data);

    template<typename E, typename T>
    static void emit(flecs::world &world); 

    template<typename E, typename T>
    static void enqueue(flecs::world &world); 

    flecs::entity emitter;
private:
    flecs::world *p_world{ nullptr };
};

template<typename E, typename T>
void event_emitter::emit()
{
    p_world->event<E>()
        .id<T>()
        .entity(emitter)
        .emit();
}

template<typename E, typename T>
void event_emitter::emit(const T &event_data) 
{
    STLA_ASSERT_RELEASE(emitter.has<T>());

    emitter.set<T>({ event_data });
    p_world->event<E>()
        .id<T>()
        .entity(emitter)
        .emit();
}

template<typename E, typename T>
void event_emitter::enqueue() 
{
    p_world->defer_begin();

    p_world->event<E>()
        .id<T>()
        .entity(emitter)
        .enqueue();

    p_world->defer_end();
}    

template<typename E, typename T>
void event_emitter::enqueue(const T &event_data) 
{
    p_world->defer_begin();

    emitter.set<T>({ event_data });
    p_world->event<E>()
        .id<T>()
        .entity(emitter)
        .enqueue();

    p_world->defer_end();
}    

template<typename E, typename T>
void event_emitter::emit(flecs::world &world)
{
    STLA_CHECK_RETV(world.has<event_emitter>());
    world.get_mut<event_emitter>()->emit<E, T>();
}

template<typename E, typename T>
void event_emitter::enqueue(flecs::world &world)
{
    STLA_CHECK_RETV(world.has<event_emitter>());
    world.get_mut<event_emitter>()->enqueue<E, T>();
}

//-----------------------------------------------------------------------------
// COMPONENTS
//-----------------------------------------------------------------------------

struct position
{
    glm::vec3 p{ 0.0f };
};

struct rotation
{
    // Note: quaternion from euler angles (pitch, yaw, roll), in radians.
    glm::quat q{ glm::vec3(0.0f) };
};

struct scale
{
    glm::vec3 s{ 0.0f };
};

struct velocity
{
    glm::vec3 v{ 0.0f };
};    

struct dimensions2d
{
    glm::ivec2 d{ 0 };
};

struct active_window
{
    uint32_t id{ 0 };
};

struct input
{
    input_action get_key_action(stla::key_code key) const;
    void set_key_action(stla::key_code key, input_action action);
    input_action get_button_action(stla::mouse_button button) const;
    void set_button_action(stla::mouse_button button, input_action action);
    glm::vec2 get_mouse_position() const;
    void set_mouse_position(const glm::vec2 &pos);
    mouse_wheel_action get_mouse_wheel_action() const;
    void set_mouse_wheel_action(mouse_wheel_action action);
    glm::vec2 get_mouse_motion() const;
    void set_mouse_motion(const glm::vec2 &motion);
    void reset();

private:
    input_action m_keys[static_cast<int>(stla::key_code::max)]{ input_action::undefined };
    input_action m_buttons[static_cast<int>(stla::mouse_button::max)]{ input_action::undefined };
    glm::vec2 m_mouse_position{ 0.0f };
    mouse_wheel_action m_mouse_wheel{ mouse_wheel_action::undefined };
    glm::vec2 m_mouse_motion{ 0.0f };
};

//-----------------------------------------------------------------------------
// SYSTEMS/EVENT HANDLERS
//-----------------------------------------------------------------------------

template<typename T>
T register_system(flecs::world &world)
{
    return T(world);
}

struct sdl3_input_system
{
    sdl3_input_system(flecs::world &world);
private:
    static void sys_process_input(flecs::world world, input &input, event_emitter &emitter);
    static stla::key_code convert(SDL_Keycode key);
};

class ievent_handler
{
public:
    ~ievent_handler()
    {
        if (m_observer.world().c_ptr() != nullptr) {
            m_observer.destruct();
        }
    }
    void set_observer(flecs::observer observer) { m_observer = observer; }
private:
    flecs::observer m_observer;
};

class event_handler : public ievent_handler
{
public:
    virtual void on_event(flecs::world &world, const event::quit &) { }
    virtual void on_event(flecs::world &world, const event::window_focus_gained &window_focused) { }
    virtual void on_event(flecs::world &world, const event::window_focus_lost &window_unfocused) { }
    virtual void on_event(flecs::world &world, const event::key &key) { }
    virtual void on_event(flecs::world &world, const event::button &button) { }
    virtual void on_event(flecs::world &world, const event::window_resize &resize) { }
    virtual void on_event(flecs::world &world, const event::mouse_wheel_motion &wheel) { }
    virtual void on_event(flecs::world &world, const event::mouse_motion &mouse) { }
    virtual void on_event(flecs::world &world, const SDL_Event &event) { }
};

//-----------------------------------------------------------------------------
// MODULE
//-----------------------------------------------------------------------------

class imodule
{
public:
    virtual ~imodule() = default;
    virtual void shutdown(flecs::world &world) = 0; 
};

class module : public imodule
{
public:
    module(flecs::world &world);
    virtual void shutdown(flecs::world &world) override;

    template<typename T>
    static void register_observers(flecs::world &world);
    static void register_observers(event_handler &handler, flecs::world &world);
};

namespace detail
{

template<typename T, typename E>
concept HasEvent = requires(flecs::world &f) {
    { T::on_event(f, E{}) } -> std::same_as<void>;
};

template<typename T, typename ET, typename E>
static void register_event(flecs::world &world)
{
    STLA_ASSERT(world.get<event_emitter>() != nullptr);

    if constexpr(HasEvent<T, E>) {
        STLA_LOG_INFO("Event registered: %s %s", typeid(T).name(), typeid(E).name());
    
        world.observer<const E>()
            .event<ET>()
            .each([&world](flecs::iter &it, size_t, const E &e) {
                T::on_event(world, e);
            });
    }
}

template<typename T, typename ET, typename E>
static void register_event(T &handler, flecs::world &world)
{
    STLA_ASSERT(world.get<event_emitter>() != nullptr);

    if constexpr (std::derived_from<T, ievent_handler>) {
        STLA_LOG_INFO("Event registered: %s %s", typeid(T).name(), typeid(E).name());

        world.observer<const E>()
            .event<ET>()
            .each([&handler, &world](flecs::iter &it, size_t, const E &e) {
                handler.on_event(world, e);
            });
    }
}

} // namespace detail

template<typename T>
void module::register_observers(flecs::world &world)
{        
    world.get_mut<event_emitter>()->emitter
        .add<event::quit>()
        .add<event::window_focus_gained>()
        .add<event::window_focus_lost>()
        .add<event::key>()
        .add<event::button>()
        .add<event::window_resize>()
        .add<event::mouse_wheel_motion>()
        .add<event::mouse_motion>()
        .add<SDL_Event>();

    detail::register_event<T, event_tag, event::quit>(world);
    detail::register_event<T, event_tag, event::window_focus_gained>(world);
    detail::register_event<T, event_tag, event::window_focus_lost>(world);
    detail::register_event<T, event_tag, event::key>(world);
    detail::register_event<T, event_tag, event::button>(world);
    detail::register_event<T, event_tag, event::window_resize>(world);
    detail::register_event<T, event_tag, event::mouse_wheel_motion>(world);
    detail::register_event<T, event_tag, event::mouse_motion>(world);
    detail::register_event<T, event_tag, SDL_Event>(world);
}

class utils
{
public:
    static glm::mat4 build_transform(flecs::entity e);
};

} // namespace core
} // namespace stla