#include "stla_ecs_3d.h"
#include "stla_mesh.h"

namespace stla
{
namespace ecs_3d
{

//-----------------------------------------------------------------------------
// EVENTS
//-----------------------------------------------------------------------------

namespace event
{
    
} // namespace event

//-----------------------------------------------------------------------------
// COMPONENTS
//-----------------------------------------------------------------------------

namespace component
{

shader::shader(gfx::shader_type type, const void *p_code)
{
    STLA_ASSERT_RELEASE(type != gfx::shader_type::undefined);
    STLA_ASSERT_RELEASE(p_code != nullptr);
    STLA_ASSERT(s.create(type, p_code));
}

shader::shader(gfx::shader_type type, const std::filesystem::path &file_path)
{
    STLA_ASSERT_RELEASE(type != gfx::shader_type::undefined);
    STLA_ASSERT(std::filesystem::exists(file_path));
    STLA_ASSERT(s.create(type, file_path));
}

material::material(flecs::world &world, const std::string &vertex_name, const std::string &fragment_name)
{
    const auto vertex_entity = world.lookup(vertex_name.c_str());
    const auto fragment_entity = world.lookup(fragment_name.c_str());
    if (!vertex_entity || !fragment_entity) {
        STLA_LOG_ERROR("Vertex/Fragment entity doesn't exist (vertex=%s) (fragment=%s)", 
            vertex_name.c_str(), fragment_name.c_str());
        STLA_ASSERT(0);
        return;
    }

    const auto *p_vertex = vertex_entity.get<component::shader>();
    const auto *p_fragment = fragment_entity.get<component::shader>();
    if (!p_vertex || !p_fragment) {
        STLA_LOG_ERROR("Vertex/Fragment entity doesn't have shader component (vertex=%p) (fragment=%p)",
            p_vertex, p_fragment);
        STLA_ASSERT(0);
        return;
    }

    if (p_vertex->s.get_type() != gfx::shader_type::vertex) {
        STLA_LOG_ERROR("Vertex shader type invalid (type=%d)", static_cast<int>(p_vertex->s.get_type()));
        STLA_ASSERT(0);
        return;
    }
    
    if (p_fragment->s.get_type() != gfx::shader_type::fragment) {
        STLA_LOG_ERROR("Fragment shader type invalid (type=%d)", static_cast<int>(p_fragment->s.get_type()));
        STLA_ASSERT(0);
        return;
    }

    vertex_shader = vertex_entity;
    fragment_shader = fragment_entity;   
}

uniform_buffer::uniform_buffer(uint32_t binding_loc, size_t size, const void *p_data) : 
    loc(binding_loc)
{
    STLA_ASSERT(buffer.create(stla::gfx::buffer_type::uniform, size, p_data));
}

renderable::renderable(flecs::world &world, const std::string &name)
{
    const auto mesh_entity = world.lookup(name.c_str());
    if (!mesh_entity) {
        STLA_LOG_ERROR("Mesh entity '%s' doesn't exist", name.c_str());
        STLA_ASSERT(0);
        return;
    }

    const auto *p_mesh = mesh_entity.get<component::mesh>();
    if (p_mesh) {
        STLA_ASSERT(render_mesh.create(p_mesh->m));
    }
    else {
        STLA_LOG_ERROR("Mesh entity '%s' doesn't have a mesh component", name.c_str());
        STLA_ASSERT(0);
    }
}

mesh_renderer::mesh_renderer(
    const gfx::region_2d &render_region,
    const gfx::color &clear_color
) : render_region(render_region), clear_color(clear_color) { }

} // namespace component

//-----------------------------------------------------------------------------
// SYSTEMS/EVENT HANDLERS
//-----------------------------------------------------------------------------

namespace system
{

//TODO can this be a template lambda?
template<typename T, typename Func>
static void register_destroy_observer(flecs::world &world, Func destroy)
{
    world.observer<T>()
        .event(flecs::OnDelete).event(flecs::OnRemove)
        .each([&](flecs::entity e, T &c) { destroy(c); }); 
}

renderer::renderer(flecs::world &world)
{
    sp_renderable_query = new flecs::query(world.query<const component::renderable>()); 

    register_destroy_observer<component::shader>(world, [](component::shader &shader) { shader.s.destroy(); });
    register_destroy_observer<component::renderable>(world, [](component::renderable &renderable) { renderable.render_mesh.destroy(); });
    register_destroy_observer<component::uniform_buffer>(world, [](component::uniform_buffer &ub) { ub.buffer.destroy(); });

    world.system<component::mesh_renderer>("Mesh Renderer: Begin Rendering")
        .kind(ecs_core::FRAME_STAGE_PRE_RENDER).each([&](flecs::entity e, component::mesh_renderer &renderer) { begin_rendering(e, renderer); });
    
    world.system<component::mesh_renderer>("Mesh Renderer: Enqueue Commands")
        .kind(ecs_core::FRAME_STAGE_ON_RENDER).each([&](flecs::entity e, component::mesh_renderer &renderer) { enqueue_commands(e, renderer); });

    world.system<component::mesh_renderer>("Mesh Renderer: End Rendering")
        .kind(ecs_core::FRAME_STAGE_ON_RENDER).each([&](flecs::entity e, component::mesh_renderer &renderer) { end_rendering(e, renderer); });

    world.system<component::mesh_renderer>("Mesh Renderer: Execute")
        .kind(ecs_core::FRAME_STAGE_ON_RENDER).each([&](flecs::entity e, component::mesh_renderer &renderer) { execute(e, renderer); });

    // world.system("On Render Task")
    //     .kind(ecs_core::FRAME_STAGE_ON_RENDER)
    //     .run([](flecs::iter &it) {
    //         // printf("Time: %f\n", it.delta_time());
    //     });
}

void renderer::shutdown()
{
    if (sp_renderable_query != nullptr) {
        delete sp_renderable_query;
        sp_renderable_query = nullptr;
    }
}

void renderer::begin_rendering(flecs::entity e, component::mesh_renderer &renderer)
{
    // if (e.has<component::render_target>()) {

    // } 
    renderer.command_buffer.begin_rendering(renderer.render_region);
    renderer.command_buffer.clear(renderer.clear_color, renderer.render_region);
    renderer.command_buffer.set_state(gfx::draw_state(true, false, true)); // TODO 
}

void renderer::enqueue_commands(flecs::entity, component::mesh_renderer &renderer)
{
    sp_renderable_query->each([&](flecs::entity e, const component::renderable &renderable) {
        // Bind material
        if (e.has<component::material>()) {
            const auto *p_material = e.get<component::material>();
            STLA_ASSERT_RELEASE(p_material != nullptr);
            const auto *p_vertex_shader = p_material->vertex_shader.get<component::shader>();
            const auto *p_fragment_shader = p_material->fragment_shader.get<component::shader>();
            if (!p_vertex_shader || !p_vertex_shader->s.is_valid() ||
                !p_fragment_shader || !p_fragment_shader->s.is_valid()) {
                STLA_LOG_ERROR("Invalid Vertex/Fragment shader");
                STLA_ASSERT(0);
            }
            else {
                renderer.command_buffer.bind(p_vertex_shader->s, p_fragment_shader->s);
            }
        }

        // Bind uniforms
        e.children([&](flecs::entity child) {
            if (child.has<component::uniform_buffer>()) {
                const auto *p_uniform_buffer = child.get<component::uniform_buffer>();
                if (!p_uniform_buffer || !p_uniform_buffer->buffer.is_valid()) {
                    STLA_LOG_ERROR("Invalid uniform buffer (entity=%s)", child.name().c_str());
                    STLA_ASSERT(0);
                }
                else {
                    renderer.command_buffer.bind_uniform(p_uniform_buffer->loc, p_uniform_buffer->buffer);
                }
            }
        });
        
        // Draw mesh
        renderable.render_mesh.draw(renderer.command_buffer);
    });
}

void renderer::end_rendering(flecs::entity, component::mesh_renderer &renderer)
{
    renderer.command_buffer.end_rendering();
}

void renderer::execute(flecs::entity, component::mesh_renderer &renderer)
{
    renderer.command_buffer.execute();
}


} // namespace system

//-----------------------------------------------------------------------------
// MODULE
//-----------------------------------------------------------------------------

module::module(flecs::world &world)
{
    ecs_core::register_system<system::renderer>(world);
}

void module::shutdown(flecs::world &world) 
{
    system::renderer::shutdown();
}

} // namespace renderer
} // namespace stla