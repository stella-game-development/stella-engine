#include "stla_ecs_core.h"
#include "stla_utils.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace stla
{
namespace ecs_core
{

//-----------------------------------------------------------------------------
// EVENTS
//-----------------------------------------------------------------------------

event_emitter::event_emitter(flecs::world &world) : p_world(&world)
{
    emitter = world.entity();
}

//-----------------------------------------------------------------------------
// COMPONENTS
//-----------------------------------------------------------------------------

input_action input::get_key_action(key_code key) const
{
    STLA_CHECK_RET(static_cast<int>(key) < static_cast<int>(key_code::max), input_action::undefined);

    return m_keys[static_cast<int>(key)];
}

void input::set_key_action(key_code key, input_action action)
{
    STLA_CHECK_RETV(static_cast<int>(key) < static_cast<int>(key_code::max));
    
    m_keys[static_cast<int>(key)] = action;
}

input_action input::get_button_action(mouse_button button) const
{
    STLA_CHECK_RET(static_cast<int>(button) < static_cast<int>(mouse_button::max), input_action::undefined);
    
    return m_buttons[static_cast<int>(button)];
}

void input::set_button_action(mouse_button button, input_action action)
{
    STLA_CHECK_RETV(static_cast<int>(button) < static_cast<int>(mouse_button::max));

    m_buttons[static_cast<int>(button)] = action;
}

glm::vec2 input::get_mouse_position() const
{
    return m_mouse_position;
}

void input::set_mouse_position(const glm::vec2 &pos)
{
    m_mouse_position = pos;
}

mouse_wheel_action input::get_mouse_wheel_action() const
{
    return m_mouse_wheel;
}

void input::set_mouse_wheel_action(mouse_wheel_action action)
{
    m_mouse_wheel = action;
}

glm::vec2 input::get_mouse_motion() const
{
    return m_mouse_motion;
}

void input::set_mouse_motion(const glm::vec2 &motion)
{
    m_mouse_motion = motion;
}

void input::reset()
{
    for (int i = 0; i < static_cast<int>(key_code::max); i++) {
        if (m_keys[i] != input_action::pressed) {
            m_keys[i] = input_action::released;
        }
    }

    m_mouse_wheel = mouse_wheel_action::undefined;

    m_mouse_motion = glm::vec2(0.0f);
}

//-----------------------------------------------------------------------------
// SYSTEMS
//-----------------------------------------------------------------------------

sdl3_input_system::sdl3_input_system(flecs::world &world)
{
    world.system<input, event_emitter>()
        .term_at(0).singleton()
        .term_at(1).singleton()
        .kind(FRAME_STAGE_PRE_FRAME)
        .each([](flecs::iter &it, size_t, input &input, event_emitter &emitter) { sys_process_input(it.world(), input, emitter); });
}

void sdl3_input_system::sys_process_input(flecs::world world, input &input, event_emitter &emitter)
{
    SDL_Event e;
    
    input.reset();

    while (SDL_PollEvent(&e) != 0) {
        emitter.emit<event_tag>(e);

        switch (e.type)
        {
            case SDL_EVENT_QUIT:
            case SDL_EVENT_WINDOW_CLOSE_REQUESTED:
            {
                emitter.emit<event_tag, event::quit>();
                break;
            }
            case SDL_EVENT_KEY_DOWN:
            {
                key_code key = convert(e.key.key);
                emitter.emit<event_tag>(event::key({ e.key.windowID, input_action::pressed, key }));
                input.set_key_action(key, input_action::pressed);
                break;
            }
            case SDL_EVENT_KEY_UP:
            {
                key_code key = convert(e.key.key);
                emitter.emit<event_tag>(event::key({ e.key.windowID, input_action::released, key }));
                input.set_key_action(key, input_action::released);
                break;
            }
            case SDL_EVENT_WINDOW_FOCUS_GAINED:
            {
                emitter.emit<event_tag>(event::window_focus_gained({ e.window.windowID }));
                if (!world.has<active_window>()) {
                    STLA_LOG_ERROR("No 'active_window' available");
                }
                else {
                    active_window &win = *world.get_mut<active_window>();
                    win.id = e.window.windowID;
                    STLA_LOG_INFO("New window focused (id=%d)", win.id);
                }
                break;
            }
            case SDL_EVENT_WINDOW_FOCUS_LOST:
            {
                emitter.emit<event_tag>(event::window_focus_lost({ e.window.windowID }));
                break;
            };
            case SDL_EVENT_MOUSE_BUTTON_DOWN:
            case SDL_EVENT_MOUSE_BUTTON_UP:
            {
                mouse_button button{ mouse_button::undefined };

                if (e.button.button == 1) {
                    button = mouse_button::left;
                }
                else if (e.button.button == 3) {
                    button = mouse_button::right;
                }

                if (e.type == SDL_EVENT_MOUSE_BUTTON_DOWN) {
                    emitter.emit<event_tag>(event::button({ e.button.windowID, input_action::pressed, button }));
                    
                    input.set_button_action(button, input_action::pressed);
                }
                else if (e.type == SDL_EVENT_MOUSE_BUTTON_UP) {
                    emitter.emit<event_tag>(event::button({ e.button.windowID, input_action::released, button }));

                    input.set_button_action(button, input_action::released);
                }
                break;
            }
            case SDL_EVENT_WINDOW_RESIZED:
            {
                emitter.enqueue<event_tag>(event::window_resize({ e.window.windowID, static_cast<uint32_t>(e.window.data1), static_cast<uint32_t>(e.window.data2) }));
                break;
            }
            case SDL_EVENT_MOUSE_MOTION:
            {
                emitter.emit<event_tag>(event::mouse_motion({ e.motion.windowID, e.motion.x, e.motion.y, e.motion.xrel, e.motion.yrel }));
                
                input.set_mouse_position(glm::vec2(e.motion.x, e.motion.y));
                input.set_mouse_motion(glm::vec2(e.motion.xrel, e.motion.yrel));

                break;
            }
            case SDL_EVENT_MOUSE_WHEEL:
            {
                mouse_wheel_action wheel_action{ mouse_wheel_action::undefined };

                if (e.wheel.y > 0) {
                    wheel_action = mouse_wheel_action::up;
                }
                else if (e.wheel.y < 0) {
                    wheel_action = mouse_wheel_action::down;
                }
                
                emitter.emit<event_tag>(event::mouse_wheel_motion({ e.wheel.windowID, wheel_action }));
                
                input.set_mouse_wheel_action(wheel_action);
                
                break;
            }
        }
    }
}

key_code sdl3_input_system::convert(SDL_Keycode key)
{
    switch (key)
    {
        case SDLK_A:      return key_code::a;
        case SDLK_B:      return key_code::b;
        case SDLK_C:      return key_code::c;
        case SDLK_D:      return key_code::d;
        case SDLK_E:      return key_code::e;
        case SDLK_F:      return key_code::f;
        case SDLK_G:      return key_code::g;
        case SDLK_H:      return key_code::h;
        case SDLK_I:      return key_code::i;
        case SDLK_J:      return key_code::j;
        case SDLK_K:      return key_code::k;
        case SDLK_L:      return key_code::l;
        case SDLK_M:      return key_code::m;
        case SDLK_N:      return key_code::n;
        case SDLK_O:      return key_code::o;
        case SDLK_P:      return key_code::p;
        case SDLK_Q:      return key_code::q;
        case SDLK_R:      return key_code::r;
        case SDLK_S:      return key_code::s;
        case SDLK_T:      return key_code::t;
        case SDLK_U:      return key_code::u;
        case SDLK_V:      return key_code::v;
        case SDLK_W:      return key_code::w;
        case SDLK_X:      return key_code::x;
        case SDLK_Y:      return key_code::y;
        case SDLK_Z:      return key_code::z;
        case SDLK_LCTRL:  return key_code::lctrl;
        case SDLK_LSHIFT: return key_code::lshift;
        case SDLK_RCTRL:  return key_code::rctrl;
        case SDLK_RSHIFT: return key_code::rshift;
        case SDLK_UP:     return key_code::up;
        case SDLK_DOWN:   return key_code::down;
        case SDLK_ESCAPE: return key_code::escape;
        case SDLK_TAB:    return key_code::tab;
        default:          return key_code::undefined;
    }
}

//-----------------------------------------------------------------------------
// MODULE
//-----------------------------------------------------------------------------

module::module(flecs::world &world)
{
    register_system<sdl3_input_system>(world);

    world.add<input>();
    world.add<active_window>();
    world.emplace<event_emitter>(world);
}

void module::shutdown(flecs::world &world) 
{ 
    world.remove<input>();
    world.remove<active_window>();
    world.remove<event_emitter>();
}

void module::register_observers(event_handler &handler, flecs::world &world)
{       
    world.get_mut<event_emitter>()->emitter
        .add<event::quit>()
        .add<event::window_focus_gained>()
        .add<event::window_focus_lost>()
        .add<event::key>()
        .add<event::button>()
        .add<event::window_resize>()
        .add<event::mouse_wheel_motion>()
        .add<event::mouse_motion>()
        .add<SDL_Event>();

    detail::register_event<event_handler, event_tag, event::quit>(handler, world);
    detail::register_event<event_handler, event_tag, event::window_focus_gained>(handler, world);
    detail::register_event<event_handler, event_tag, event::window_focus_lost>(handler, world);
    detail::register_event<event_handler, event_tag, event::key>(handler, world);
    detail::register_event<event_handler, event_tag, event::button>(handler, world);
    detail::register_event<event_handler, event_tag, event::window_resize>(handler, world);
    detail::register_event<event_handler, event_tag, event::mouse_wheel_motion>(handler, world);
    detail::register_event<event_handler, event_tag, event::mouse_motion>(handler, world);
    detail::register_event<event_handler, event_tag, SDL_Event>(handler, world);
}

glm::mat4 utils::build_transform(flecs::entity e)
{
    glm::mat4 t(1.0f);
    glm::mat4 rot(1.0f);
    glm::mat4 s(1.0f);

    if (e.has<position>()) {
        const position *p_position = e.get<position>();
        if (p_position != nullptr) {
            t = glm::translate(t, p_position->p);
        }
    }
    
    if (e.has<rotation>()) {
        const rotation *p_rotation = e.get<rotation>();
        if (p_rotation != nullptr) {
            rot = glm::toMat4(p_rotation->q);
        }
    }

    if (e.has<scale>()) { 
        const scale *p_scale = e.get<scale>();
        if (p_scale != nullptr) {
            s = glm::scale(s, p_scale->s);
        }
    }

    return t * rot * s;
}

} // namespace core
} // namespace stla