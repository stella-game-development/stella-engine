#include "stla_delta_time.h"

#include <SDL3/SDL.h>

namespace stla
{

delta_time::delta_time()
{

}

void delta_time::tick()
{
    float current_frame = static_cast<float>(SDL_GetTicks());
    m_ms = current_frame - m_last_frame;
    m_last_frame = current_frame;
}

} // namespace stla
