#ifndef __STLA_UTILS_H__
#define __STLA_UTILS_H__

#include "stla_utils.h"
#include "stla_core.h"
#include "stla_log.h"

#include <SDL3/SDL.h>
#include <assimp/quaternion.h>
#include <assimp/vector3.h>
#include <assimp/matrix4x4.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include <stdio.h>
#include <stdint.h>

#include <filesystem>
#include <string>
#include <concepts>

#ifdef __cplusplus
extern "C" {
#endif

#if defined(_MSC_VER)
#  define STLA_INLINE __forceinline
#else
#  define STLA_INLINE static inline __attribute((always_inline))
#endif


#ifdef _WIN32
#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)
#else
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#endif

#ifdef __GNUC__
#define STLA_PACK( __Declaration__ ) __Declaration__ __attribute__((__packed__))
#endif

#ifdef _MSC_VER
#define STLA_PACK( __Declaration__ ) __pragma( pack(push, 1) ) __Declaration__ __pragma( pack(pop))
#endif

#define STLA_ASSERT(condition)                \
    {                                         \
        if (!(condition))                     \
        {                                     \
            STLA_LOG_ERROR("[STLA ASSERT] %s\n", #condition); \
            SDL_assert(0);                    \
        }                                     \
    }

#define STLA_ASSERT_RELEASE(condition)                      \
    {                                                       \
        if (!(condition))                                   \
        {                                                   \
            STLA_LOG_ERROR("[STLA ASSERT] %s\n", #condition); \
            SDL_assert_release(0);                          \
        }                                                   \
    }

#define STLA_CHECK(condition) \
    { \
        if (!(condition)) { \
            STLA_LOG_ERROR("Condition '%s' failed!\n", #condition); \
            SDL_assert(0); \
        } \
    }

#define STLA_CHECK_RET(condition, ret_val) \
    { \
        if (!(condition)) { \
            STLA_LOG_ERROR("Condition '%s' failed! (ret_val=%d)\n", #condition, ret_val); \
            SDL_assert(0); \
            return ret_val; \
        } \
    }

#define STLA_CHECK_RETV(condition) \
    { \
        if (!(condition)) { \
            STLA_LOG_ERROR("Condition '%s' failed!\n", #condition); \
            SDL_assert(0); \
            return; \
        } \
    }

// Get the number of milliseconds since Stella library initialization 
uint64_t stla_get_time(void);

int32_t stla_get_cpu_count(void);

const char *stla_basename(const char *p_path); 

#ifdef __cplusplus
}
#endif

#ifdef __cplusplus

namespace stla
{
namespace utils
{

uint64_t get_time_ms();
int32_t get_cpu_count();
void delay(uint32_t ms);

static inline uint32_t flatten(uint32_t x, uint32_t y, uint32_t z, uint32_t w, uint32_t d) { return d * (y * w + x) + z; }
static inline uint32_t flatten(uint32_t x, uint32_t y, uint32_t w) { return (x * w) + y; }
    
float random_range(float from, float to);
int random_range(int from, int to);
float random_float();
int random_int();

template<typename Func>
uint64_t profile(Func fn)
{
    auto start = get_time_ms();
    fn();
    auto stop = get_time_ms();
    return stop - start;
}

namespace str
{

std::string remove_substring(std::string &input, const std::string &substr);

}

namespace assimp
{

class glm_helpers
{
public:

	static inline glm::mat4 convert(const aiMatrix4x4 &from)
	{
		glm::mat4 to;
		//the a,b,c,d in assimp is the row ; the 1,2,3,4 is the column
		to[0][0] = from.a1; to[1][0] = from.a2; to[2][0] = from.a3; to[3][0] = from.a4;
		to[0][1] = from.b1; to[1][1] = from.b2; to[2][1] = from.b3; to[3][1] = from.b4;
		to[0][2] = from.c1; to[1][2] = from.c2; to[2][2] = from.c3; to[3][2] = from.c4;
		to[0][3] = from.d1; to[1][3] = from.d2; to[2][3] = from.d3; to[3][3] = from.d4;
		return to;
	}

	static inline glm::vec3 convert(const aiVector3D &vec) 
	{ 
		return glm::vec3(vec.x, vec.y, vec.z); 
	}

	static inline glm::quat convert(const aiQuaternion &orientation)
	{
		return glm::quat(orientation.w, orientation.x, orientation.y, orientation.z);
	}
};

class scene_loader
{
public:

    ~scene_loader();
    bool load(const std::filesystem::path &file_path);

    template<typename Func>
    void for_each(Func visitor) const
    {
        STLA_ASSERT(mp_scene != nullptr);
        if (mp_scene == nullptr) {
            return;
        }

        if constexpr (std::invocable<Func, const aiAnimation *>) {
            for (int i = 0; i < mp_scene->mNumAnimations; i++) {
                visitor(mp_scene->mAnimations[i]);
            }   
        }
        else {
            // process_node(mp_scene->mRootNode, glm::mat4(1.0f), mp_scene, visitor);
            process_node(mp_scene->mRootNode, glm::mat4(1.0f), mp_scene, visitor);
        }
    }

    const aiNode *get_root() const
    {
        if (mp_scene == nullptr) {
            return nullptr;
        }
        return mp_scene->mRootNode;
    }

    const aiScene *get_scene() const 
    {
        return mp_scene;
    }

    std::filesystem::path get_file_path() const { return m_file_path; }

private:

    template<typename Func>
    void process_node(const aiNode *p_node, const glm::mat4 &parent_transform, const aiScene *p_scene, Func visitor) const
    {
        if (p_node == nullptr || p_scene == nullptr) {
            STLA_LOG_WARN("Model node or scene is null");
            return;
        }

        if constexpr (std::invocable<Func, const aiNode *, const aiScene *>) {
            visitor(p_node, p_scene);
        }
        else if constexpr (std::invocable<Func, const aiNode *, const glm::mat4 &, const aiScene *>) {
            visitor(p_node, parent_transform, p_scene);
        }

        const glm::mat4 node_transform = parent_transform * assimp::glm_helpers::convert(p_node->mTransformation);

        for (int i = 0; i < p_node->mNumMeshes; i++) {
            aiMesh *p_mesh = p_scene->mMeshes[p_node->mMeshes[i]];
            if constexpr (std::invocable<Func, const aiMesh *, const aiScene *>) {
                visitor(p_mesh, p_scene);
            }
            else if constexpr (std::invocable<Func, const aiMesh *, const glm::mat4 &, const aiScene *>) {
                visitor(p_mesh, node_transform, p_scene);
            }
        }

        for (int i = 0; i < p_node->mNumChildren; i++) { 
            process_node(p_node->mChildren[i], node_transform, p_scene, visitor);
        }
    }
    
    std::filesystem::path m_file_path;
    const aiScene *mp_scene{ nullptr };
    Assimp::Importer m_importer;
};

}

}
}

#endif

#endif