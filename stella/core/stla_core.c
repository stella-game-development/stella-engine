#include "stla_core.h"
#include "stla_utils.h"

#include <SDL3/SDL.h>

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

// stla_result_enum_t stla_shutdown(void)
// {
//     STLA_CHECK_RET(g_instance.initialized == true, STLA_ERROR_INTERNAL_OPERATION_FAILED)

//     uint32_t deallocations = stla_stats_get_deallocations();
//     uint32_t allocations   = stla_stats_get_allocations();
    
//     STLA_ASSERT(deallocations == allocations);

//     stla_log_shutdown();

//     SDL_Quit();

//     return STLA_OK;
// }

const char *stla_get_result_string(stla_result_enum_t result)
{
    switch (result)
    {
        case STLA_OK: return "STLA_OK";
        case STLA_ERROR_NULL_POINTER: return "STLA_ERROR_NULL_POINTER";
        case STLA_ERROR_TIMEOUT: return "STLA_ERROR_TIMEOUT";
        case STLA_ERROR_INVALID_OPERATION: return "STLA_ERROR_INVALID_OPERATION";
        case STLA_ERROR_OUT_OF_BOUNDS: return "STLA_ERROR_OUT_OF_BOUNDS";
        case STLA_ERROR_OUT_OF_HOST_MEMORY: return "STLA_ERROR_OUT_OF_HOST_MEMORY";
        case STLA_ERROR_OUT_OF_DEVICE_MEMORY: return "STLA_ERROR_OUT_OF_DEVICE_MEMORY";
        case STLA_ERROR_INTERNAL_OPERATION_FAILED: return "STLA_ERROR_INTERNAL_OPERATION_FAILED";
        case STLA_ERROR_UNDEFINED_ENUM: return "STLA_ERROR_UNDEFINED_ENUM";
        default: return "Unknown Result Enum";
    }
}

void *stla_allocate(
    const stla_allocation_callbacks_t *p_allocator, 
    size_t size, 
    size_t alignment)
{
    if (p_allocator != NULL && p_allocator->p_alloc != NULL) {
        return p_allocator->p_alloc(p_allocator->p_user_data, size, alignment);
    }

    return calloc(1, size);
}

void stla_free(
    const stla_allocation_callbacks_t *p_allocator, 
    void *p_memory)
{
    STLA_CHECK_RETV(p_memory != NULL);

    if (p_allocator != NULL && p_allocator->p_free != NULL) {
        p_allocator->p_free(p_allocator->p_user_data, p_memory);
        return;
    }

    free(p_memory);
}

#ifdef __cplusplus
}
#endif