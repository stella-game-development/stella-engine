#pragma once

namespace stla
{

class delta_time
{
public:

    delta_time();

    void tick();
    operator float() const { return m_ms; }

private:

    float m_ms{ 0.0f };
    float m_last_frame{ 0.0f };
};

}

//  {
//     float ms;
//     float last_frame;
// } stla_delta_time_t;

// void stla_delta_time_update(
//     stla_delta_time_t *p_dt);
