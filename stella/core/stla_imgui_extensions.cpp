#include "stla_imgui_extensions.h"

#include <limits>

namespace stla
{
namespace ImGui
{

struct InputTextCallback_UserData
{
    std::string*            Str;
    ImGuiInputTextCallback  ChainCallback;
    void*                   ChainCallbackUserData;
};

static int InputTextCallback(ImGuiInputTextCallbackData* data)
{
    InputTextCallback_UserData* user_data = (InputTextCallback_UserData*)data->UserData;
    if (data->EventFlag == ImGuiInputTextFlags_CallbackResize)
    {
        // Resize string callback
        // If for some reason we refuse the new length (BufTextLen) and/or capacity (BufSize) we need to set them back to what we want.
        std::string* str = user_data->Str;
        IM_ASSERT(data->Buf == str->c_str());
        str->resize(data->BufTextLen);
        data->Buf = (char*)str->c_str();
    }
    else if (user_data->ChainCallback)
    {
        // Forward to user callback, if any
        data->UserData = user_data->ChainCallbackUserData;
        return user_data->ChainCallback(data);
    }
    return 0;
}

void SetCursorCenterX(float item_width)
{
    if (::ImGui::GetContentRegionAvail().x < item_width) {
        return;
    }

    float cur_pos_x = ::ImGui::GetCursorPosX();
    float content_region_x = ::ImGui::GetContentRegionAvail().x;
    
    ::ImGui::SetCursorPosX(cur_pos_x + ((content_region_x - item_width) * 0.5f));
}

void SetCursorRightAligned(float item_width)
{
    if (::ImGui::GetContentRegionAvail().x < item_width) {
        return;
    }

    float cur_pos_x = ::ImGui::GetCursorPosX();
    float content_region_x = ::ImGui::GetContentRegionAvail().x;
    
    ::ImGui::SetCursorPosX(cur_pos_x + ((content_region_x - item_width)));
}

bool InputText(const char* label, std::string* str, ImGuiInputTextFlags flags, ImGuiInputTextCallback callback, void* user_data)
{
    IM_ASSERT((flags & ImGuiInputTextFlags_CallbackResize) == 0);
    flags |= ImGuiInputTextFlags_CallbackResize;

    InputTextCallback_UserData cb_user_data;
    cb_user_data.Str = str;
    cb_user_data.ChainCallback = callback;
    cb_user_data.ChainCallbackUserData = user_data;
    return ::ImGui::InputText(label, (char*)str->c_str(), str->capacity() + 1, flags, InputTextCallback, &cb_user_data);
}

bool InputTextMultiline(const char* label, std::string* str, const ImVec2& size, ImGuiInputTextFlags flags, ImGuiInputTextCallback callback, void* user_data)
{
    IM_ASSERT((flags & ImGuiInputTextFlags_CallbackResize) == 0);
    flags |= ImGuiInputTextFlags_CallbackResize;

    InputTextCallback_UserData cb_user_data;
    cb_user_data.Str = str;
    cb_user_data.ChainCallback = callback;
    cb_user_data.ChainCallbackUserData = user_data;
    return ::ImGui::InputTextMultiline(label, (char*)str->c_str(), str->capacity() + 1, size, flags, InputTextCallback, &cb_user_data);
}

bool InputTextWithHint(const char* label, const char* hint, std::string* str, ImGuiInputTextFlags flags, ImGuiInputTextCallback callback, void* user_data)
{
    IM_ASSERT((flags & ImGuiInputTextFlags_CallbackResize) == 0);
    flags |= ImGuiInputTextFlags_CallbackResize;

    InputTextCallback_UserData cb_user_data;
    cb_user_data.Str = str;
    cb_user_data.ChainCallback = callback;
    cb_user_data.ChainCallbackUserData = user_data;
    return ::ImGui::InputTextWithHint(label, hint, (char*)str->c_str(), str->capacity() + 1, flags, InputTextCallback, &cb_user_data);
}

bool InputUint8(const char *label, uint8_t *value) 
{
    // ImGui does not support uint8_t directly, so we use an int for the input.
    int temp_value = static_cast<int>(*value);

    // Use ImGui's InputInt to capture the input
    bool changed = ::ImGui::InputInt(label, &temp_value);

    // Ensure the value stays within the uint8_t range [0, 255]
    if (temp_value < 0) temp_value = 0;
    if (temp_value > std::numeric_limits<uint8_t>::max()) temp_value = std::numeric_limits<uint8_t>::max();

    // Assign the modified value back to the uint8_t variable
    *value = static_cast<uint8_t>(temp_value);

    return changed; // Return true if the value was changed
}            

bool InputUint16(const char *label, uint16_t *value) 
{
    // ImGui does not support uint8_t directly, so we use an int for the input.
    int temp_value = static_cast<int>(*value);

    // Use ImGui's InputInt to capture the input
    bool changed = ::ImGui::InputInt(label, &temp_value);

    if (temp_value < 0) temp_value = 0;
    if (temp_value > std::numeric_limits<uint16_t>::max()) temp_value = std::numeric_limits<uint16_t>::max();

    // Assign the modified value back to the uint8_t variable
    *value = static_cast<uint16_t>(temp_value);

    return changed; // Return true if the value was changed
}

bool InputUint32(const char *label, uint32_t *value) 
{
    // ImGui does not support uint8_t directly, so we use an int for the input.
    int temp_value = static_cast<int>(*value);

    // Use ImGui's InputInt to capture the input
    bool changed = ::ImGui::InputInt(label, &temp_value);

    if (temp_value < 0) temp_value = 0;
    if (temp_value > std::numeric_limits<uint32_t>::max()) temp_value = std::numeric_limits<uint32_t>::max();

    // Assign the modified value back to the uint8_t variable
    *value = static_cast<uint32_t>(temp_value);

    return changed; // Return true if the value was changed
}      

bool Combo(const char* label, int* current_item, const std::vector<std::string> &items, int items_count, int height_in_items)
{
    return ::ImGui::Combo(label, current_item, [](void* data, int idx, const char** out_text) { 
            if (data == nullptr) {
                return false;
            }

            const std::vector<std::string> &vec = *reinterpret_cast<const std::vector<std::string> *>(data);

            // *out_text = ((const std::vector<std::string>*)data)[idx].c_str(); return true; 
            *out_text = vec[idx].c_str(); 

            return true; 
        }, 
        (void*)&items, items_count, height_in_items);
}

logger::logger()
{
    m_auto_scroll = true;
    clear();
}

void logger::clear()
{
    m_buf.clear();
    m_line_offsets.clear();
    m_line_offsets.push_back(0);
}

void logger::add_log(const char* fmt, ...) 
{
    int old_size = m_buf.size();
    va_list args;
    va_start(args, fmt);
    m_buf.appendfv(fmt, args);
    va_end(args);
    for (int new_size = m_buf.size(); old_size < new_size; old_size++) {
        if (m_buf[old_size] == '\n') {
            m_line_offsets.push_back(old_size + 1);
        }
    }
}

void logger::add_log(const std::string &log)
{
    add_log(log.c_str());
}

void logger::draw()
{
    // Options menu
    if (::ImGui::BeginPopup("Options")) {
        ::ImGui::Checkbox("Auto-scroll", &m_auto_scroll);
        ::ImGui::EndPopup();
    }

    ::ImGui::Text("Filter");
    m_filter.Draw("###Filter", -100.0f);
    ::ImGui::NewLine();

    // Main window
    if (::ImGui::Button("Options"))
        ::ImGui::OpenPopup("Options");
    ::ImGui::SameLine();
    bool clear = ::ImGui::Button("Clear");
    ::ImGui::SameLine();
    bool copy = ::ImGui::Button("Copy");
    //::ImGui::SameLine();

    ::ImGui::Separator();

    if (::ImGui::BeginChild("scrolling", ImVec2(0, 0), ImGuiChildFlags_None, ImGuiWindowFlags_HorizontalScrollbar))
    {
        if (clear)
            this->clear();
        if (copy)
            ::ImGui::LogToClipboard();

        ::ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0, 0));
        const char* buf = m_buf.begin();
        const char* buf_end = m_buf.end();
        if (m_filter.IsActive())
        {
            // In this example we don't use the clipper when m_filter is enabled.
            // This is because we don't have random access to the result of our filter.
            // A real application processing logs with ten of thousands of entries may want to store the result of
            // search/filter.. especially if the filtering function is not trivial (e.g. reg-exp).
            for (int line_no = 0; line_no < m_line_offsets.Size; line_no++)
            {
                const char* line_start = buf + m_line_offsets[line_no];
                const char* line_end = (line_no + 1 < m_line_offsets.Size) ? (buf + m_line_offsets[line_no + 1] - 1) : buf_end;
                if (m_filter.PassFilter(line_start, line_end))
                    ::ImGui::TextUnformatted(line_start, line_end);
            }
        }
        else
        {
            // The simplest and easy way to display the entire buffer:
            //   ImGui::TextUnformatted(buf_begin, buf_end);
            // And it'll just work. TextUnformatted() has specialization for large blob of text and will fast-forward
            // to skip non-visible lines. Here we instead demonstrate using the clipper to only process lines that are
            // within the visible area.
            // If you have tens of thousands of items and their processing cost is non-negligible, coarse clipping them
            // on your side is recommended. Using ImGuiListClipper requires
            // - A) random access into your data
            // - B) items all being the  same height,
            // both of which we can handle since we have an array pointing to the beginning of each line of text.
            // When using the filter (in the block of code above) we don't have random access into the data to display
            // anymore, which is why we don't use the clipper. Storing or skimming through the search result would make
            // it possible (and would be recommended if you want to search through tens of thousands of entries).
            ImGuiListClipper clipper;
            clipper.Begin(m_line_offsets.Size);
            while (clipper.Step())
            {
                for (int line_no = clipper.DisplayStart; line_no < clipper.DisplayEnd; line_no++)
                {
                    const char* line_start = buf + m_line_offsets[line_no];
                    const char* line_end = (line_no + 1 < m_line_offsets.Size) ? (buf + m_line_offsets[line_no + 1] - 1) : buf_end;
                    ::ImGui::TextUnformatted(line_start, line_end);
                }
            }
            clipper.End();
        }
        ::ImGui::PopStyleVar();

        // Keep up at the bottom of the scroll region if we were already at the bottom at the beginning of the frame.
        // Using a scrollbar or mouse-wheel will take away from the bottom edge.
        if (m_auto_scroll && ::ImGui::GetScrollY() >= ::ImGui::GetScrollMaxY())
            ::ImGui::SetScrollHereY(1.0f);
    }
    ::ImGui::EndChild();
}


} // namespace imgui
} // namespace stla

