#ifndef __STLA_CORE_H__
#define __STLA_CORE_H__

#include <stdint.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    STLA_OK = 0,
    STLA_ERROR_NULL_POINTER, // todo get rid of this
    STLA_ERROR_TIMEOUT,
    STLA_ERROR_INVALID_OPERATION,
    STLA_ERROR_INVALID_PARAMETER,
    STLA_ERROR_OUT_OF_BOUNDS,
    STLA_ERROR_OUT_OF_HOST_MEMORY,
    STLA_ERROR_OUT_OF_DEVICE_MEMORY,
    STLA_ERROR_INTERNAL_OPERATION_FAILED,
    STLA_ERROR_UNDEFINED_ENUM
} stla_result_enum_t;

typedef enum {
    STLA_GFX_BACKEND_NONE = 0,
    STLA_GFX_BACKEND_SOFTWARE_RENDERER,
    STLA_GFX_BACKEND_OPENGL
} stla_gfx_backend_enum_t;

typedef void *(*stla_alloc_fn_t)(void *p_user_data, size_t size, size_t alignment);
typedef void (*stla_free_fn_t)(void *p_user_data, void *p_memory);

typedef struct {
    void* p_user_data;
    stla_alloc_fn_t p_alloc;
    stla_free_fn_t p_free;
} stla_allocation_callbacks_t;

const char *stla_get_result_string(
    stla_result_enum_t result);

void *stla_allocate(
    const stla_allocation_callbacks_t *p_allocator, 
    size_t size, 
    size_t alignment);

void stla_free(
    const stla_allocation_callbacks_t *p_allocator, 
    void *p_memory);

#ifdef __cplusplus
}
#endif

#endif