#pragma once

namespace stla
{

enum class key_code
{
    undefined = 0,
    a,
    b,
    c,
    d,
    e,
    f,
    g,
    h,
    i,
    j,
    k,
    l,
    m,
    n,
    o,
    p,
    q,
    r,
    s,
    t,
    u,
    v,
    w,
    x,
    y,
    z,
    lctrl,
    lshift,
    rctrl,
    rshift,
    up,
    down,
    escape,
    tab,
    max
};

enum class input_action
{
    undefined = 0,
    pressed,
    released
};

enum class mouse_button
{
    undefined = 0,
    left,
    right,
    max
};

enum class mouse_wheel_action
{
    undefined = 0,
    up,
    down
};

namespace input
{

bool is_key_pressed(key_code key);

} // namespace input
} // namespace stla
