#include "stla_application.h"

#include "stla_log.h"
#include "stla_window.h"
#include "stla_gfx.h"
#include "stla_delta_time.h"
#include "stla_input.h"

// Force dedicated GPU

extern "C" 
{
    __declspec(dllexport) unsigned long NvOptimusEnablement = 0x00000001;
}

extern "C"
{
    __declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;
}

namespace stla
{

application::application(const config &config)
{
    stla::log::startup();

    import_ecs_module<ecs_core::module>();

    ecs_core::module::register_observers(*this, m_world);

    mp_window = new stla::window(config.window.width, config.window.height, config.window.name.c_str(), config.window.vsync, config.window.resizeable);

    m_world.system("On Startup")
        .kind(ecs_core::FRAME_STAGE_ON_STARTUP)
        .run([this](flecs::iter &it) { flecs::world world = it.world(); this->on_startup(world); });

    m_world.system("On Pre Frame")
        .kind(ecs_core::FRAME_STAGE_PRE_FRAME)
        .run([this](flecs::iter &it) { flecs::world world = it.world(); this->on_pre_frame(world, it.delta_time()); });

    m_world.system("On Pre Update")
        .kind(ecs_core::FRAME_STAGE_PRE_UPDATE)
        .run([this](flecs::iter &it) { flecs::world world = it.world(); this->on_pre_update(world, it.delta_time()); });

    m_world.system("On Update")
        .kind(ecs_core::FRAME_STAGE_ON_UPDATE)
        .run([this](flecs::iter &it) { flecs::world world = it.world(); this->on_update(world, it.delta_time()); });

    m_world.system("On Post Update")
        .kind(ecs_core::FRAME_STAGE_POST_UPDATE)
        .run([this](flecs::iter &it) { flecs::world world = it.world(); this->on_post_update(world, it.delta_time()); });

    m_world.system("On Pre Render")
        .kind(ecs_core::FRAME_STAGE_PRE_RENDER)
        .run([this](flecs::iter &it) { flecs::world world = it.world(); this->on_pre_render(world, it.delta_time()); });

    m_world.system("On Render")
        .kind(ecs_core::FRAME_STAGE_ON_RENDER)
        .run([this](flecs::iter &it) { flecs::world world = it.world(); this->on_render(world, it.delta_time()); });
    
    m_world.system("On Post Frame")
        .kind(ecs_core::FRAME_STAGE_POST_FRAME)
        .run([this](flecs::iter &it) { flecs::world world = it.world(); this->on_post_frame(world, it.delta_time()); });
}

application::~application()
{
    while (m_ecs_modules.size()) {
        m_ecs_modules.top()->shutdown(m_world);
        m_ecs_modules.pop();
    }

    m_world.~world();
    
    delete mp_window;

    stla::log::shutdown();
}

void application::quit() { m_world.quit(); }
    
void application::run()
{
    delta_time dt;
    while (true) {
        dt.tick();

        on_pre_progress(m_world, static_cast<float>(dt));
        if (!m_world.progress(static_cast<float>(dt))) {
            break;
        }
        on_post_progress(m_world, static_cast<float>(dt));

        SDL_GL_SwapWindow(SDL_GL_GetCurrentWindow());
    }
}

stla::window &application::get_window() 
{
    STLA_ASSERT_RELEASE(mp_window != nullptr);
    return *mp_window;
}

} // namespace stla
