#pragma once

#include "stla_window.h"
#include "stla_event.h"
#include "stla_ecs_core.h"

#include <flecs.h>

#include <stdint.h>

#include <string>
#include <stack>

namespace stla
{

class application : public ecs_core::event_handler
{
public:

    struct config
    {
        struct {
            uint32_t width;
            uint32_t height;
            std::string name;
            bool vsync;
            bool resizeable;
        } window;
    };

    application(const config &config);
    virtual ~application();

    void quit();
    void run();
    stla::window &get_window();
    flecs::world &get_world() { return m_world; }

    virtual void on_startup(flecs::world &world) { }
    virtual void on_pre_progress(flecs::world &world, float dt) { }
    virtual void on_pre_frame(flecs::world &world, float dt) { }
    virtual void on_pre_update(flecs::world &world, float dt) { }
    virtual void on_update(flecs::world &world, float dt) { }
    virtual void on_post_update(flecs::world &world, float dt) { }
    virtual void on_pre_render(flecs::world &world, float dt) { }
    virtual void on_render(flecs::world &world, float dt) { }
    virtual void on_post_frame(flecs::world &world, float dt) { }
    virtual void on_post_progress(flecs::world &world, float dt) { }

    virtual void on_event(flecs::world &world, const event::quit &) override { quit(); }
    virtual void on_event(flecs::world &world, const event::window_focus_gained &window_focused) override { }
    virtual void on_event(flecs::world &world, const event::window_focus_lost &window_unfocused) override { }
    virtual void on_event(flecs::world &world, const event::key &key) override { }
    virtual void on_event(flecs::world &world, const event::button &button) override { }
    virtual void on_event(flecs::world &world, const event::window_resize &resize) override { }
    virtual void on_event(flecs::world &world, const event::mouse_wheel_motion &wheel) override { }
    virtual void on_event(flecs::world &world, const event::mouse_motion &mouse) override { }
    virtual void on_event(flecs::world &world, const SDL_Event &event) override { }

    template<typename T>
    void import_ecs_module()
    {
        m_ecs_modules.push(m_world.import<T>().get_mut<T>());
    }

private:
    stla::window *mp_window{ nullptr };
    flecs::world m_world;
    std::stack<ecs_core::imodule *> m_ecs_modules;
};

} // namespace stla
