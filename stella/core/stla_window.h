#pragma once

#include "stla_gfx.h"

#include <SDL3/SDL.h>

#include <stdint.h>
#include <stdbool.h>

namespace stla
{

class window
{
public:

    window(uint32_t w, uint32_t h, const char *p_title, bool vsync=true, bool resizeable=true);
    ~window();

    void set_title(const char *p_title);
    SDL_Window *raw_ptr() const;
    SDL_GLContext gl_cxt() const;
    uint32_t get_width() const;
    uint32_t get_height() const;
    void resize(uint32_t w, uint32_t h);
    uint32_t get_pixel_width() const;
    uint32_t get_pixel_height() const;
    float get_aspect_ratio() const;
    uint32_t get_window_id() const;
    void set_min_size(uint32_t w, uint32_t h);
    void present(const gfx::image &image);
    
private:

    struct present_shader_data
    {
        uint64_t sampler;
    };

    SDL_Window *mp_window{ nullptr };
    SDL_GLContext m_gl_cxt{ nullptr };

    struct {
        stla::gfx::command_buffer command_buffer;
        stla::gfx::shader vertex_shader;
        stla::gfx::shader fragment_shader;
        stla::gfx::buffer vertex_buffer;
        stla::gfx::buffer index_buffer;
        stla::gfx::buffer uniform_buffer;
    } m_present_state;
};

}
