#include "stla_utils.h"

#include <SDL3/SDL.h>

#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

uint64_t stla_get_time(void)
{
    return SDL_GetTicks();
}

int32_t stla_get_cpu_count(void)
{
    return SDL_GetCPUCount();
}

const char *stla_basename(
    const char *p_path) 
{
    STLA_CHECK_RET(p_path != NULL, NULL);

    const char *filename = p_path;
    const char *last_slash = strrchr(p_path, '/');
    const char *last_backslash = strrchr(p_path, '\\');

    if (last_slash || last_backslash) {
        // Either slash or backslash found, move to the character after it
        filename = last_slash > last_backslash ? last_slash + 1 : last_backslash + 1;
    }

    return filename;
}

#ifdef __cplusplus
}
#endif

#ifdef __cplusplus

#include <random>

namespace stla
{
namespace utils
{

uint64_t get_time_ms()
{
    return SDL_GetTicks();
}

int32_t get_cpu_count()
{
    return SDL_GetCPUCount();
}

void delay(uint32_t ms)
{
    SDL_Delay(ms);
}

float random_range(float from, float to) 
{
    std::random_device                    rand_dev;
    std::mt19937                          generator(rand_dev());
    std::uniform_real_distribution<float> distr(from, to);
    return distr(generator);
}

int random_range(int from, int to)
{
    std::random_device                 rand_dev;
    std::mt19937                       generator(rand_dev());
    std::uniform_int_distribution<int> distr(from, to);
    return distr(generator);   
}

float random_float()
{
    return (float)std::rand() / (float)RAND_MAX; 
}

int random_int()
{
    return std::rand();
}

namespace str
{
    
std::string remove_substring(std::string &input, const std::string &substr)
{
    std::string::size_type i{ 0 };
    if ((i = input.find(substr)) != std::string::npos) {
        size_t count = substr.length();
        if ((i + count) > input.length()) {
            count = std::string::npos;
        }
        input.erase(i, count);
    }
    return input; 
}

} // namespace str

namespace assimp
{

scene_loader::~scene_loader()
{
    m_importer.FreeScene();
    mp_scene = nullptr;
}

bool scene_loader::load(const std::filesystem::path &file_path)
{
    // TODO auto generate unique mesh names (will need const_cast)
    if (!std::filesystem::exists(file_path)) {
        STLA_LOG_ERROR("Model file doesn't exist: %s", file_path.string().c_str());
        return false;
    }

    m_importer.FreeScene();
    
    // m_importer.SetPropertyBool(AI_CONFIG_PP_PTV_KEEP_HIERARCHY, true);
    // m_importer.SetPropertyBool(AI_CONFIG_PP_PTV_NORMALIZE, true);
    // m_importer.SetPropertyInteger(AI_CONFIG_PP_LBW_MAX_WEIGHTS, 4);

    mp_scene = m_importer.ReadFile(file_path.string(), 
        aiProcess_GlobalScale | aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_GenBoundingBoxes | aiProcess_FlipUVs | aiProcess_GenSmoothNormals);
        // aiProcess_GlobalScale | aiProcess_Triangulate | aiProcess_GenBoundingBoxes | aiProcess_FlipUVs | aiProcess_GenSmoothNormals);
        // | aiProcess_GenSmoothNormals | aiProcess_CalcTangentSpace | aiProcess_GenBoundingBoxes);
        // aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_GenNormals | aiProcess_LimitBoneWeights | aiProcess_GenBoundingBoxes);

    if (mp_scene == nullptr || mp_scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || mp_scene->mRootNode == nullptr) {
        STLA_LOG_ERROR("Failed to load model file %s: %s", file_path.string().c_str(), m_importer.GetErrorString());
        return false;
    }

    m_file_path = file_path;

    return true;
}

}
}
}

#endif

