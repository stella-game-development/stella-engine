#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace stla
{

class icamera
{
public:
    virtual glm::mat4 get_view() const = 0;
    virtual glm::mat4 get_projection() const = 0;
    virtual glm::mat4 get_view_projection() const = 0;
    virtual glm::vec3 get_position() const = 0;
    virtual glm::vec3 get_direction() const = 0;
};

class perspective_camera : public icamera
{
public:

    perspective_camera();
    perspective_camera(float fov, float aspect, float near, float far, glm::vec3 position, glm::vec3 front);
    perspective_camera(const perspective_camera &) = default;

    virtual glm::mat4 get_view() const override;
    virtual glm::mat4 get_projection() const override;
    virtual glm::mat4 get_view_projection() const override;

    virtual glm::vec3 get_position() const override;
    void set_position(const glm::vec3 &position);

    virtual glm::vec3 get_direction() const override;
    void set_direction(const glm::vec3 &direction);

    glm::vec3 get_up() const;
    void set_up(const glm::vec3 &up);

    float get_fov() const;
    void set_fov(float fov);
    
    float aspect_ratio() const;
    void set_aspect_ratio(float ar);

    float get_near() const;
    void set_near(float near);

    float get_far() const;
    void set_far(float far);

    glm::vec3 get_front() const;
    void set_front(const glm::vec3 &front);

private:

    void update_projection();

    glm::mat4 m_projection;
    glm::vec3 m_position;
    glm::vec3 m_front;
    glm::vec3 m_up;

    float m_fov;
    float m_aspect;
    float m_near;
    float m_far;
};

} // namespace stla
