#pragma once

#include "stla_input.h"

#include <stdint.h>

namespace stla
{
namespace event
{

using window_id = uint32_t;

struct quit { };

struct key
{
    window_id window;
    input_action action;
    key_code key;
};

struct button
{
    window_id window;
    input_action action;
    mouse_button button;
};

struct window_resize
{
    window_id window;
    uint32_t x;
    uint32_t y;
};

struct window_focus_gained
{
    window_id window;
};

struct window_focus_lost
{
    window_id window;
};

struct mouse_wheel_motion
{
    window_id window;
    mouse_wheel_action action;
};

struct mouse_motion
{
    window_id window;
    float x;
    float y;
    float relx;
    float rely;
};

} // namespace event
} // namespace stla
