#include "stla_log.h"

#include <SDL3/SDL.h>

#include <string.h>
#include <stdbool.h>
#include <stdarg.h>
#include <stdio.h>

namespace stla
{

bool log::startup()
{
    if (sp_instance == nullptr) {
        sp_instance = new log();
    }

    return true;
}

void log::shutdown()
{
    if (sp_instance != nullptr) {
        delete sp_instance;
        sp_instance = nullptr;
    }
}

void log::vwrite(level level, const char *p_format, va_list args)
{
    if (sp_instance == nullptr) {
        return;
    }

    std::lock_guard<std::mutex> lock(sp_instance->m_mutex);
    vprintf(p_format, args);
}

}

#ifdef __cplusplus
extern "C" {
#endif

void stla_log_write(
    stla_log_level_t level,
    const char *format, ...)
{
    va_list list;
    va_start(list, format);
    stla::log::vwrite(static_cast<stla::log::level>(level), format, list);
    va_end(list);
}

#ifdef __cplusplus
}
#endif

