#include "stla_input.h"
#include "stla_log.h"

#include <SDL3/SDL.h>

namespace stla
{
namespace input
{

static SDL_Keycode convert(key_code key)
{
    switch (key)
    {
        case key_code::a: return SDLK_A;
        case key_code::b: return SDLK_B;
        case key_code::c: return SDLK_C;
        case key_code::d: return SDLK_D;
        case key_code::e: return SDLK_E;
        case key_code::f: return SDLK_F;
        case key_code::g: return SDLK_G;
        case key_code::h: return SDLK_H;
        case key_code::i: return SDLK_I;
        case key_code::j: return SDLK_J;
        case key_code::k: return SDLK_K;
        case key_code::l: return SDLK_L;
        case key_code::m: return SDLK_M;
        case key_code::n: return SDLK_N;
        case key_code::o: return SDLK_O;
        case key_code::p: return SDLK_P;
        case key_code::q: return SDLK_Q;
        case key_code::r: return SDLK_R;
        case key_code::s: return SDLK_S;
        case key_code::t: return SDLK_T;
        case key_code::u: return SDLK_U;
        case key_code::v: return SDLK_V;
        case key_code::w: return SDLK_W;
        case key_code::x: return SDLK_X;
        case key_code::y: return SDLK_Y;
        case key_code::z: return SDLK_Z;
        case key_code::lctrl: return SDLK_LCTRL;
        case key_code::lshift: return SDLK_LSHIFT;
        case key_code::rctrl: return SDLK_RCTRL;
        case key_code::rshift: return SDLK_RSHIFT;
        case key_code::up: return SDLK_UP;
        case key_code::down: return SDLK_DOWN;
        case key_code::escape: return SDLK_ESCAPE;
        case key_code::tab: return SDLK_TAB;
        default: return SDLK_UNKNOWN;
    }
}

bool is_key_pressed(key_code key)
{
    SDL_Keycode sdl_key = convert(key);
    if (sdl_key == SDLK_UNKNOWN) {
        STLA_LOG_ERROR("Unknown key (key_code=%d)", static_cast<int>(key));
        return false;
    }

    SDL_Scancode sdl_scan = SDL_GetScancodeFromKey(sdl_key, nullptr);
    const uint8_t *p_keyboard_state = SDL_GetKeyboardState(nullptr);
    return p_keyboard_state[sdl_scan];
}

} // namespace input
} // namespace stla
