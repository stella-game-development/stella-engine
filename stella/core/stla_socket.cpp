#include "stla_socket.h"
#include "stla_log.h"

namespace stla
{

socket::socket(protocol proto) :
    m_protocol(proto)
{ }

socket::~socket()
{
    close();
}

bool socket::is_connected()
{
    if (m_socket == INVALID_SOCKET) {
        return false;
    }

    if (m_to_close.load()) {
        close();
    }

    return m_connected.load();
}

bool socket::is_receiving()
{
    if (m_to_close.load()) {
        close();
    }

    return m_is_receiving.load();
}

void socket::close()
{
    if (m_socket == INVALID_SOCKET) {
        return;
    }

    STLA_LOG_ERROR("Socket closed (socket=%p) (addr=%s) (port=%d)", 
        m_socket, m_addr.c_str(), m_port);

    closesocket(m_socket);
    stop_receiving();
    m_connected.store(false);
    m_recv_queue.clear();
    m_to_close.store(false);
    m_socket = INVALID_SOCKET;
    m_addr = "";
    m_port = 0;
}

bool socket::connect(const std::string &addr, uint16_t port) 
{
    disconnect();
    
    if (!create()) {
        return false;
    }

#ifdef _WIN32
    sockaddr_in service = { 0 };
    service.sin_family = AF_INET;
    service.sin_addr.s_addr = inet_addr(addr.c_str());
    service.sin_port = htons(static_cast<u_short>(port));
    int result = ::connect(m_socket, reinterpret_cast<SOCKADDR*>(&service), sizeof(service));
    
    if (result == SOCKET_ERROR) {
        STLA_LOG_ERROR("Socket connect error: %d", WSAGetLastError());
        return false;
    }

    m_connected.store(true);

    m_addr = addr;
    m_port = port;

    return true;
#else
    return false;
#endif
}   

void socket::disconnect()
{
    if (!is_connected()) {
        return;
    }

    close();
}

uint32_t socket::send(const uint8_t *p_data, size_t size)
{
    if (p_data == nullptr || size == 0) {
        return 0;
    }

    if (!is_connected()) {
        return 0;
    }

#ifdef _WIN32
    // // Put into blocking mode
    // unsigned long ul = 0;
    // if (ioctlsocket(m_socket, FIONBIO, (unsigned long *) &ul) == SOCKET_ERROR) {
    //     return 0;
    // }

    int byte_count = ::send(m_socket, reinterpret_cast<const char *>(p_data), size, 0);
    if (byte_count == SOCKET_ERROR) {
        STLA_LOG_ERROR("Socket send error: %d", WSAGetLastError());
        return 0;
    }

    return static_cast<uint32_t>(byte_count);
#else
    return 0;
#endif
}

uint32_t socket::send(const std::string &msg)
{
    return send(reinterpret_cast<const uint8_t *>(msg.c_str()), msg.size());
}

uint32_t socket::send(const std::vector<uint8_t> &data)
{
    return send(data.data(), data.size());
}

bool socket::start_receiving()
{
    if (!is_connected()) {
        return false;
    }

    if (m_is_receiving.load()) {
        return true;
    }

    m_is_receiving.store(true);

    m_recv_thread = std::thread(&socket::receive_thread, this);

    return true;
}

bool socket::stop_receiving()
{
    m_is_receiving.store(false);

    if (m_recv_thread.joinable()) {
        m_recv_thread.join();
    }

    return true;
}

bool socket::receive(std::vector<uint8_t> &data)
{
    if (m_recv_queue.size() == 0) {
        data.clear();
        return false;
    }

    m_recv_queue.pop_all(data);    

    return true;
}

void socket::receive_thread()
{
    static constexpr int MAX_QUEUED_BYTES = 128;

    char data[128] = { 0 };

    while (m_connected.load() && m_is_receiving.load()) {
        int result = recv(m_socket, data, sizeof(data), 0);

        if (result <= 0) {
            m_to_close.store(true);
            STLA_LOG_ERROR("'recv' failed with error: %d", WSAGetLastError());
            break;
        }

        if (m_recv_queue.size() >= MAX_QUEUED_BYTES) {
            STLA_LOG_ERROR("Socket receive queue has reached max size of %d bytes, packet droped (size=%d).", MAX_QUEUED_BYTES, result);
        }
        else {
            for (int i = 0; i < result; i++) {
                m_recv_queue.push(data[i]);
            }
        }
    }

    m_is_receiving.store(false);
}

bool socket::create()
{
#ifdef _WIN32
    if (!s_winsock_initialized.load()) {
        {
            const std::lock_guard<std::mutex> lg(s_winsock_init_mutex);
            WSADATA wsa_data;
            WORD w_version_requested = MAKEWORD(2, 2);
            int wserr = WSAStartup(w_version_requested, &wsa_data);
            if (wserr != 0) {
                STLA_LOG_ERROR("Failed to initialized Winsock");
                return false;
            }
            else {
                STLA_LOG_INFO("Winsock initialized successfully (status=%d)", wsa_data.szSystemStatus);
                s_winsock_initialized.store(true);        
            }
        }
    }

    m_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

#endif

    return true;
}

} // namespace stla
