#pragma once

#include <stdint.h>

#include <atomic>
#include <mutex>
#include <string>
#include <vector>
#include <queue>
#include <thread>

#ifdef __linux__ 
    //linux code goes here
#elif _WIN32
    #include <winsock2.h>
#else
#endif

namespace stla
{

template<typename T>
class queue
{
public:

    queue() = default;

    void push(const T &data)
    {
        std::lock_guard<std::mutex> lg(m_mutex);
        m_queue.push(data);
    }

    T pop() 
    {
        std::lock_guard<std::mutex> lg(m_mutex);
        T data = m_queue.front();
        m_queue.pop();
        return data;
    }

    size_t size() 
    {
        std::lock_guard<std::mutex> lg(m_mutex);
        return m_queue.size();
    }
    
    void pop_all(std::vector<T> &data)
    {
        std::lock_guard<std::mutex> lg(m_mutex);
        data.reserve(m_queue.size());
        data.clear();
        while (m_queue.size() != 0) {
            data.push_back(m_queue.front());
            m_queue.pop();
        }
    }

    void clear()
    {
        std::lock_guard<std::mutex> lg(m_mutex);
        while (m_queue.size() != 0) {
            m_queue.pop();
        }
    }

private:
    std::mutex m_mutex;
    std::queue<T> m_queue;
};

class socket
{
public:

    enum class protocol
    {
        tcp
    };

    socket(protocol proto = protocol::tcp);
    ~socket();

    bool is_connected();
    bool is_receiving();
    bool connect(const std::string &addr, uint16_t port);
    void disconnect();
    uint32_t send(const uint8_t *p_data, size_t size);
    uint32_t send(const std::string &msg);
    uint32_t send(const std::vector<uint8_t> &data);
    bool start_receiving(); 
    bool stop_receiving(); 
    bool receive(std::vector<uint8_t> &data);

private:

    void receive_thread();
    bool create();
    void close();

    protocol m_protocol;
    std::atomic_bool m_to_close{ false };
    std::atomic_bool m_connected{ false };
    std::atomic_bool m_is_receiving{ false };
    std::thread m_recv_thread; 
    std::string m_addr{ "" };
    uint16_t m_port{ 0 };
    queue<uint8_t> m_recv_queue;

#ifdef __linux__ 
#error "Stella Socket not implemented Linux"
#elif _WIN32
    SOCKET m_socket{ INVALID_SOCKET };
#else
#error "Stella Socket not implemented for unknown platform"
#endif

#ifdef _WIN32
    static inline std::atomic_bool s_winsock_initialized = false;
    static inline std::mutex s_winsock_init_mutex;
#endif
};
    
} // namespace stla
