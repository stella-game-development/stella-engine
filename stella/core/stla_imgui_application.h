#pragma once

#include "stla_application.h"
#include "stla_ecs_core.h"

#include <functional>

namespace stla
{

class imgui_application : public application
{
public:
    
    imgui_application(
        const application::config &config, 
        bool main_window_dockable=false, 
        std::function<void()> initialize_imgui_systems = [](){ });

    virtual ~imgui_application();

    virtual void on_render_imgui(flecs::world &world, float dt) = 0;

    void init_font(float font_size);
    
private:
    void init_theme();
    void start_frame();
    void end_frame();

    bool m_main_window_dockable{ false };
};

} // namespace stla
