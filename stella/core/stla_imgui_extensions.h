#pragma once

#include <imgui.h>

#include <string>
#include <vector>

namespace stla
{
namespace ImGui
{

void SetCursorCenterX(float item_width);
void SetCursorRightAligned(float item_width);

bool InputText(const char* label, std::string* str, ImGuiInputTextFlags flags = 0, ImGuiInputTextCallback callback = nullptr, void* user_data = nullptr);
bool InputTextMultiline(const char* label, std::string* str, const ImVec2& size = ImVec2(0, 0), ImGuiInputTextFlags flags = 0, ImGuiInputTextCallback callback = nullptr, void* user_data = nullptr);
bool InputTextWithHint(const char* label, const char* hint, std::string* str, ImGuiInputTextFlags flags = 0, ImGuiInputTextCallback callback = nullptr, void* user_data = nullptr);

bool InputUint8(const char *label, uint8_t *value);
bool InputUint16(const char *label, uint16_t *value);
bool InputUint32(const char *label, uint32_t *value);

bool Combo(const char* label, int* current_item, const std::vector<std::string>& items, int items_count, int height_in_items = -1);

// TODO max logs
class logger
{
public:

    logger();

    void clear();
    void add_log(const char* fmt, ...); 
    void add_log(const std::string &log);
    void draw();

private:
    ImGuiTextBuffer     m_buf;
    ImGuiTextFilter     m_filter;
    ImVector<int>       m_line_offsets; // Index to lines offset. We maintain this with AddLog() calls.
    bool                m_auto_scroll;  // Keep scrolling if already at the bottom.
};

} // namespace imgui
} // namespace stla
