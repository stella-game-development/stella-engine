#include "stla_window.h"
#include "stla_utils.h"
#include "stla_log.h"

#include <glad/glad.h>

#include <vector>

namespace stla
{

static inline const char *g_vertex_shader_src = R"(
    #version 460 core
    #extension GL_ARB_bindless_texture : require
    
    layout (location = 0) in vec3 a_position;
    layout (location = 1) in vec2 a_uv;
    
    layout (std140, binding = 0) uniform shader_data
    {
        sampler2D u_sampler;
    };

    out vec2 v_uv;
    
    void main()
    {
        v_uv = a_uv; 
        gl_Position = vec4(a_position, 1.0);
    }
)";

static inline const char *g_fragment_shader_src = R"(
    #version 460 core
    #extension GL_ARB_bindless_texture : require

    layout (std140, binding = 0) uniform shader_data
    {
        sampler2D u_sampler;
    };
    
    in vec2 v_uv;
    out vec4 f_color;

    void main()
    {
        f_color = vec4(texture(u_sampler, vec2(v_uv.x, 1.0 - v_uv.y)).xyz, 1.0);
    }
)";

static inline const std::vector<float> g_quad_vertices = {
    // positions          // texture coords
     1.0f,  1.0f, 0.0f,   1.0f, 1.0f, // top right
     1.0f, -1.0f, 0.0f,   1.0f, 0.0f, // bottom right
    -1.0f, -1.0f, 0.0f,   0.0f, 0.0f, // bottom left
    -1.0f,  1.0f, 0.0f,   0.0f, 1.0f  // top left 
};

static inline const std::vector<unsigned int> g_quad_indices = {  
    0, 1, 3, // first triangle
    1, 2, 3  // second triangle
};

window::window(uint32_t w, uint32_t h, const char *p_title, bool vsync, bool resizeable)
{
    STLA_ASSERT_RELEASE(SDL_Init(SDL_INIT_VIDEO) == SDL_TRUE);

    int context_flags = SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG;

#ifdef _DEBUG
    // context_flags |= SDL_GL_CONTEXT_DEBUG_FLAG;
#endif
 
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, context_flags);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 6);
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 16);

    SDL_WindowFlags window_flags = SDL_WINDOW_OPENGL;
    if (resizeable) {
        window_flags |= SDL_WINDOW_RESIZABLE;
    }

    mp_window = SDL_CreateWindow(p_title, w, h, window_flags);

    STLA_ASSERT(mp_window != NULL);
    
    m_gl_cxt = SDL_GL_CreateContext(mp_window);
    STLA_ASSERT(m_gl_cxt != NULL);

    SDL_GL_MakeCurrent(mp_window, m_gl_cxt);

    if (vsync) {
        SDL_GL_SetSwapInterval(1);
    }

    gfx::api::startup();

    m_present_state.vertex_shader.create(stla::gfx::shader_type::vertex, g_vertex_shader_src);
    m_present_state.fragment_shader.create(stla::gfx::shader_type::fragment, g_fragment_shader_src);
    m_present_state.vertex_buffer.create(stla::gfx::buffer_type::vertex, g_quad_vertices);
    m_present_state.index_buffer.create(stla::gfx::buffer_type::index, g_quad_indices);
    m_present_state.uniform_buffer.create(stla::gfx::buffer_type::uniform, sizeof(present_shader_data));
}

window::~window()
{
    m_present_state.vertex_shader.destroy();
    m_present_state.fragment_shader.destroy();
    m_present_state.vertex_buffer.destroy();
    m_present_state.index_buffer.destroy();
    m_present_state.uniform_buffer.destroy();

	SDL_DestroyWindow(mp_window);
	
	SDL_Quit();

    stla::gfx::api::shutdown();
}

void window::set_title(const char *p_title)
{
	STLA_ASSERT(SDL_SetWindowTitle(mp_window, p_title) == 0);
}

SDL_Window *window::raw_ptr() const
{
	return mp_window;
}

SDL_GLContext window::gl_cxt() const
{
	return m_gl_cxt;
}

uint32_t window::get_width() const
{
	int w = 0;
    SDL_GetWindowSize(mp_window, &w, nullptr);
	return static_cast<uint32_t>(w);
}

uint32_t window::get_height() const
{	
	int h = 0;
    SDL_GetWindowSize(mp_window, nullptr, &h);
	return static_cast<uint32_t>(h);
}

void window::resize(uint32_t w, uint32_t h)
{
    SDL_SetWindowSize(mp_window, static_cast<int>(w), static_cast<int>(h));
}

uint32_t window::get_pixel_width() const
{
	int w = 0;
	SDL_GetWindowSizeInPixels(mp_window, &w, nullptr);
	return static_cast<uint32_t>(w);
}

uint32_t window::get_pixel_height() const
{
	int h = 0;
	SDL_GetWindowSizeInPixels(mp_window, nullptr, &h);
	return static_cast<uint32_t>(h);
}

float window::get_aspect_ratio() const
{
    return static_cast<float>(get_pixel_width()) / static_cast<float>(get_pixel_height());
}

uint32_t window::get_window_id() const
{
    return static_cast<uint32_t>(SDL_GetWindowID(mp_window));
}

void window::set_min_size(uint32_t w, uint32_t h)
{
    SDL_SetWindowMinimumSize(mp_window, static_cast<int>(w), static_cast<int>(h));
}

void window::present(const gfx::image &image)
{
    gfx::region_2d render_region(0, 0, get_width(), get_height());

    static const std::vector<stla::gfx::vertex_attribute_descriptor> vertex_layout = {
        { 0, 3, stla::gfx::vertex_attribute_type::f, false },
        { 1, 2, stla::gfx::vertex_attribute_type::f, false },
    };

    present_shader_data data;
    data.sampler = image.get_handle();
    m_present_state.uniform_buffer.copy(data);

    m_present_state.command_buffer.begin_rendering(render_region);
    m_present_state.command_buffer.clear(stla::gfx::color(0.25f, 0.15f, 0.15f, 1.0f), render_region);
    m_present_state.command_buffer.set_state(stla::gfx::draw_state(false, false, false));
    m_present_state.command_buffer.bind(m_present_state.vertex_shader, m_present_state.fragment_shader);
    m_present_state.command_buffer.bind(vertex_layout, m_present_state.vertex_buffer);
    m_present_state.command_buffer.bind(m_present_state.index_buffer);
    m_present_state.command_buffer.bind_uniform(0, m_present_state.uniform_buffer);
    m_present_state.command_buffer.draw_indexed(6);
    m_present_state.command_buffer.end_rendering();
    m_present_state.command_buffer.execute();

}

}

