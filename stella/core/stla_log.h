#ifndef __STLA_LOG_H__
#define __STLA_LOG_H__

#include "stla_core.h"

#include <stdint.h>

#ifdef __cplusplus

#include <mutex>

namespace stla
{

class log
{
public:

    enum class level
    {
        none,
        error,
        warn,
        info,
        debug,
        verbose
    };

    static bool startup();
    static void shutdown();
    static void vwrite(level level, const char *p_format, va_list args);

private:

    static inline log *sp_instance = nullptr;

    std::mutex m_mutex;
};

}

#endif

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    STLA_LOG_LEVEL_NONE,       /*!< No log output */
    STLA_LOG_LEVEL_ERROR,      /*!< Critical errors, software module can not recover on its own */
    STLA_LOG_LEVEL_WARN,       /*!< Error conditions from which recovery measures have been taken */
    STLA_LOG_LEVEL_INFO,       /*!< Information messages which describe normal flow of events */
    STLA_LOG_LEVEL_DEBUG,      /*!< Extra information which is not necessary for normal use (values, pointers, sizes, etc). */
    STLA_LOG_LEVEL_VERBOSE     /*!< Bigger chunks of debugging information, or frequent messages which can potentially flood the output. */
} stla_log_level_t;

#define CONFIG_LOG_COLORS (1)

#if CONFIG_LOG_COLORS
#define LOG_COLOR_BLACK   "30"
#define LOG_COLOR_RED     "31"
#define LOG_COLOR_GREEN   "32"
#define LOG_COLOR_BROWN   "33"
#define LOG_COLOR_BLUE    "34"
#define LOG_COLOR_PURPLE  "35"
#define LOG_COLOR_CYAN    "36"
#define LOG_COLOR(COLOR)  "\033[0;" COLOR "m"
#define LOG_BOLD(COLOR)   "\033[1;" COLOR "m"
#define LOG_RESET_COLOR   "\033[0m"
#define LOG_COLOR_E       LOG_COLOR(LOG_COLOR_RED)
#define LOG_COLOR_W       LOG_COLOR(LOG_COLOR_BROWN)
#define LOG_COLOR_I       LOG_COLOR(LOG_COLOR_GREEN)
#define LOG_COLOR_D       
#define LOG_COLOR_V
#else //CONFIG_LOG_COLORS
#define LOG_COLOR_E
#define LOG_COLOR_W
#define LOG_COLOR_I
#define LOG_COLOR_D
#define LOG_COLOR_V
#define LOG_RESET_COLOR
#endif //CONFIG_LOG_COLORS

# define PRIu8		"u"
# define PRIu16		"u"
# define PRIu32		"u"
# define PRIu64		__PRI64_PREFIX "u"

#define stla_log_fromat(letter, format)  LOG_COLOR_ ## letter "STLA_" #letter ": " format LOG_RESET_COLOR "\n"

// stla_result_enum_t stla_log_startup(void);

// void stla_log_shutdown(void);

void stla_log_write(
    stla_log_level_t level, 
    const char* format, ...);

#define STLA_LOG_ERROR(   format, ... ) stla_log_write(STLA_LOG_LEVEL_ERROR,   stla_log_fromat(E, format), ##__VA_ARGS__)
#define STLA_LOG_WARN(    format, ... ) stla_log_write(STLA_LOG_LEVEL_WARN,    stla_log_fromat(W, format), ##__VA_ARGS__)
#define STLA_LOG_INFO(    format, ... ) stla_log_write(STLA_LOG_LEVEL_INFO,    stla_log_fromat(I, format), ##__VA_ARGS__)
#define STLA_LOG_DEBUG(   format, ... ) stla_log_write(STLA_LOG_LEVEL_DEBUG,   stla_log_fromat(D, format), ##__VA_ARGS__)
#define STLA_LOG_VERBOSE( format, ... ) stla_log_write(STLA_LOG_LEVEL_VERBOSE, stla_log_fromat(V, format), ##__VA_ARGS__)


#ifdef __cplusplus
}
#endif

#endif