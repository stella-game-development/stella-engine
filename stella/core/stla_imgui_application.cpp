#pragma once

#include "stla_imgui_application.h"
#include "stla_imgui_style.h"

#include <imgui.h>
#include <imgui_internal.h>
#include <imgui_impl_sdl3.h>
#include <imgui_impl_opengl3.h>
#include <SDL3/SDL.h>

namespace stla
{

imgui_application::imgui_application(
        const application::config &config, 
        bool main_window_dockable, 
        std::function<void()> initialize_imgui_systems) :
	stla::application(config), m_main_window_dockable(main_window_dockable)
{
    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;         // Enable Docking
    io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;       // Enable Multi-Viewport / Platform Windows

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsLight();

    // When viewports are enabled we tweak WindowRounding/WindowBg so platform windows can look identical to regular ones.
    ImGuiStyle& style = ImGui::GetStyle();
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        style.WindowRounding = 0.0f;
        style.Colors[ImGuiCol_WindowBg].w = 1.0f;
    }

    // Setup Platform/Renderer backends
    STLA_ASSERT(ImGui_ImplSDL3_InitForOpenGL(stla::application::get_window().raw_ptr(), stla::application::get_window().gl_cxt()));
    STLA_ASSERT(ImGui_ImplOpenGL3_Init("#version 460"));

    init_theme();

    init_font(12.0f);
    
    auto &world = get_world();

    world.system("ImGui Start Frame")
        .kind(ecs_core::FRAME_STAGE_POST_FRAME)
        .run([&](flecs::iter &it) { start_frame(); });
    
    world.system("ImGui Render")
        .kind(ecs_core::FRAME_STAGE_POST_FRAME)
        .run([&](flecs::iter &it) { flecs::world world = it.world(); this->on_render_imgui(world, it.delta_time()); });

    initialize_imgui_systems();

    world.system("ImGui End Frame")
        .kind(ecs_core::FRAME_STAGE_POST_FRAME)
        .run([&](flecs::iter &it) { 
            end_frame(); });

    world.observer<const SDL_Event>()
        .event<ecs_core::event_tag>()
        .each([&](flecs::iter &it, size_t, const SDL_Event &e) {
            ImGui_ImplSDL3_ProcessEvent(&e);
        });
}

imgui_application::~imgui_application()
{
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplSDL3_Shutdown();
    ImGui::DestroyContext();
}

void imgui_application::init_theme()
{
    ImGui::SetupImGuiStyle(true, 1.0f);
    
    // The Cherno
    auto &colors = ImGui::GetStyle().Colors;

    colors[ImGuiCol_WindowBg]           = ImVec4{ 0.1f, 0.105f, 0.11f, 1.0f };
    colors[ImGuiCol_DockingEmptyBg]     = ImVec4{ 0.0f, 0.0f, 0.0f, 0.0f };
    colors[ImGuiCol_ModalWindowDimBg]   = ImVec4{ 0.0f, 0.0f, 0.0f, 0.0f };

    // // Headers
    // colors[ImGuiCol_Header]             = ImVec4{ 0.2f,  0.205f,  0.21f,  1.0f };
    // colors[ImGuiCol_HeaderHovered]      = ImVec4{ 0.3f,  0.305f,  0.31f,  1.0f };
    // colors[ImGuiCol_HeaderActive]       = ImVec4{ 0.15f, 0.1505f, 0.151f, 1.0f };

    // // Buttons
    // colors[ImGuiCol_Button]             = ImVec4{ 0.2f,  0.205f,  0.21f,  1.0f };
    // colors[ImGuiCol_ButtonHovered]      = ImVec4{ 0.3f,  0.305f,  0.31f,  1.0f };
    // colors[ImGuiCol_ButtonActive]       = ImVec4{ 0.15f, 0.1505f, 0.151f, 1.0f };

    // // Frame BG
    colors[ImGuiCol_FrameBg]            = ImVec4{ 0.2f,  0.205f,  0.21f,  1.0f };
    colors[ImGuiCol_FrameBgHovered]     = ImVec4{ 0.3f,  0.305f,  0.31f,  1.0f };
    colors[ImGuiCol_FrameBgActive]      = ImVec4{ 0.15f, 0.1505f, 0.151f, 1.0f };

    // // Tabs
    // colors[ImGuiCol_Tab]                = ImVec4{ 0.15f, 0.1505f, 0.151f, 1.0f };
    // colors[ImGuiCol_TabHovered]         = ImVec4{ 0.38f, 0.3805f, 0.381f, 1.0f };
    // colors[ImGuiCol_TabActive]          = ImVec4{ 0.28f, 0.2805f, 0.281f, 1.0f };
    // colors[ImGuiCol_TabUnfocused]       = ImVec4{ 0.15f, 0.1505f, 0.151f, 1.0f };
    // colors[ImGuiCol_TabUnfocusedActive] = ImVec4{ 0.2f,  0.205f,  0.21f,  1.0f };

    // // Title
    // colors[ImGuiCol_TitleBg]            = ImVec4{ 0.15f, 0.1505f, 0.151f, 1.0f };
    // colors[ImGuiCol_TitleBgActive]      = ImVec4{ 0.15f, 0.1505f, 0.151f, 1.0f };
    // colors[ImGuiCol_TitleBgCollapsed]   = ImVec4{ 0.15f, 0.1505f, 0.151f, 1.0f };
}

void imgui_application::init_font(float font_size)
{
    ImGuiIO& io = ImGui::GetIO();
    io.Fonts->Clear();
    ImFontConfig font_config;
    font_config.SizePixels = font_size;
#ifdef STELLA_FONT_PATH
    // io.Fonts->AddFontFromFileTTF(STELLA_FONT_PATH, font_size);
    io.Fonts->AddFontFromFileTTF(STELLA_FONT_PATH, 12);
	io.Fonts->AddFontFromFileTTF(STELLA_FONT_PATH, 10);
	io.Fonts->AddFontFromFileTTF(STELLA_FONT_PATH, 14);
	io.Fonts->AddFontFromFileTTF(STELLA_FONT_PATH, 18);
    io.FontDefault = io.Fonts->AddFontFromFileTTF(STELLA_FONT_PATH, font_size);
#else
    io.Fonts->AddFontDefault(&font_config);    
#endif
    io.Fonts->Build();
}

void imgui_application::start_frame()
{
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplSDL3_NewFrame();
    ImGui::NewFrame();
    if (m_main_window_dockable) {
        ImGui::DockSpaceOverViewport(0, ImGui::GetMainViewport(), ImGuiDockNodeFlags_PassthruCentralNode);
    }
}

void imgui_application::end_frame()
{
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    ImGuiIO& io = ImGui::GetIO(); 
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        SDL_Window *backup_current_window = SDL_GL_GetCurrentWindow();
        SDL_GLContext backup_current_context = SDL_GL_GetCurrentContext();
        ImGui::UpdatePlatformWindows();
        ImGui::RenderPlatformWindowsDefault();
        SDL_GL_MakeCurrent(backup_current_window, backup_current_context);
    }
}

} // namespace stla
