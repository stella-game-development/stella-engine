#pragma once

#include <SDL3/SDL.h>

#ifdef __cplusplus

#include "graphics/stla_gfx.h"
#include "graphics/stla_color.h"
#include "graphics/stla_mesh.h"
#include "graphics/stla_model.h"
#include "graphics/stla_render_mesh.h"
#include "graphics/stla_animation_data.h"
#include "graphics/stla_animated_model.h"
#include "graphics/stla_skeletal_animation.h"

#include <imgui.h>
#include <imgui_internal.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp> 
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtx/compatibility.hpp>
#endif

#include "core/stla_window.h"
#include "core/stla_input.h"
#include "core/stla_utils.h"
#include "core/stla_log.h"
#include "core/stla_core.h"
#include "core/stla_delta_time.h"
#include "core/stla_camera.h"
#include "core/stla_window.h"
#include "core/stla_application.h"
#include "core/stla_imgui_application.h"
#include "core/stla_event.h"
#include "core/stla_socket.h"
#include "core/stla_imgui_extensions.h"

#include "math/stla_geometry.h"
#include "math/stla_ease.h"
#include "math/stla_math_utils.h"

#include "ecs/stla_ecs_core.h"
#include "ecs/stla_ecs_3d.h"