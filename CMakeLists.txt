cmake_minimum_required(VERSION 3.8)
set(CMAKE_C_STANDARD 99)
set(CMAKE_CXX_STANDARD 20)
project(stella)
set_property(GLOBAL PROPERTY USE_FOLDERS ON)

# Sandbox Options
option(STELLA_VENDORED "Use vendored libraries" ON)
option(STELLA_EXAMPLES "Build examples" OFF)

if (STELLA_VENDORED)
    # This assumes you have added SDL as a submodule in vendored/SDL
    add_subdirectory(deps/SDL EXCLUDE_FROM_ALL)
else()
    # 1. Look for a SDL3 package,
    # 2. look for the SDL3-shared component, and
    # 3. fail if the shared component cannot be found.
    find_package(SDL3 REQUIRED CONFIG REQUIRED COMPONENTS SDL3-shared)
endif()

file(
	GLOB IMGUI_SOURCES
	deps/imgui/imgui_demo.cpp
	deps/imgui/imgui_draw.cpp
	deps/imgui/imgui_tables.cpp
	deps/imgui/imgui_widgets.cpp
	deps/imgui/imgui.cpp
	deps/imgui/backends/imgui_impl_sdl3.cpp
	deps/imgui/backends/imgui_impl_opengl3.cpp
)

file(GLOB_RECURSE STELLA_CPP_SOURCES stella/*.cpp)

add_library(
    ${PROJECT_NAME} 
    STATIC
    ${STELLA_CPP_SOURCES}
    ${IMGUI_SOURCES}
    deps/glad/src/glad.c
)

target_include_directories(
    ${PROJECT_NAME}
    PUBLIC
    stella
    stella/core
    stella/graphics
    stella/math
    stella/ecs
    deps/glm
    deps/glad/include
    deps/imgui
    deps/imgui/backends
    deps/flecs/include
    deps/dmon
    deps/stb
    deps/json/single_include
    deps/PerlinNoise
    deps/SDL/include
    deps/tinyobjloader
    deps/assimp/include
)


add_subdirectory(deps/flecs)

set(BUILD_SHARED_LIBS OFF)
set(ASSIMP_BUILD_TESTS OFF)
set(ASSIMP_INSTALL OFF)
set(ASSIMP_INJECT_DEBUG_POSTFIX OFF)
add_subdirectory(deps/assimp)

if ("${CMAKE_BUILD_TYPE}" STREQUAL "") 
    message("-- [STELLA] CMAKE_BUILD_TYPE variable is not set, setting to Debug")
    set(CMAKE_BUILD_TYPE "Debug")
endif()

set(STELLA_EXAMPLES_DIR ${CMAKE_CURRENT_SOURCE_DIR}/examples)

set(SDL_DLL   SDL3.dll)
set(FLECS_DLL flecs.dll)
message("-- [STELLA] Libs: ${SDL_DLL}, ${FLECS_DLL}")

set(STELLA_LIB_PATH     ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_BUILD_TYPE}/stella.lib)
set(SDL3_LIB_PATH       ${CMAKE_CURRENT_BINARY_DIR}/deps/SDL/${CMAKE_BUILD_TYPE}/SDL3.dll)
set(FLECS_LIB_PATH      ${CMAKE_CURRENT_BINARY_DIR}/deps/flecs/${CMAKE_BUILD_TYPE}/flecs.dll)
# set(STELLA_FONT_PATH    ${CMAKE_CURRENT_SOURCE_DIR}/fonts/Montserrat/static/Montserrat-Regular.ttf)

get_directory_property(hasParent PARENT_DIRECTORY)
if (hasParent)

    set(STELLA_EXAMPLES_DIR ${CMAKE_CURRENT_SOURCE_DIR}/examples PARENT_SCOPE)
    set(STELLA_LIB_PATH     ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_BUILD_TYPE}/stella.lib PARENT_SCOPE)
    set(SDL3_LIB_PATH       ${CMAKE_CURRENT_BINARY_DIR}/deps/SDL/${CMAKE_BUILD_TYPE}/SDL3.dll PARENT_SCOPE)
    set(FLECS_LIB_PATH      ${CMAKE_CURRENT_BINARY_DIR}/deps/flecs/${CMAKE_BUILD_TYPE}/flecs.dll PARENT_SCOPE)

    if (STELLA_FONT_PATH) 
        set(STELLA_FONT_PATH ${STLA_FONT_PATH} PARENT_SCOPE)
    endif()

endif()

message("-- [STELLA] Building in ${CMAKE_BUILD_TYPE} mode")

target_link_libraries(
    ${PROJECT_NAME} 
    PUBLIC 
    SDL3::SDL3
    flecs
    assimp
)

if (STELLA_FONT_PATH) 
    message("-- [STELLA] Setting ImGui font path: ${STELLA_FONT_PATH}")
    target_compile_definitions(${PROJECT_NAME} PUBLIC -DSTELLA_FONT_PATH="${STELLA_FONT_PATH}")
endif()

if (STELLA_EXAMPLES) 
    message("-- [STELLA] Building examples")
    
    message("-- [STELLA]   Basics")
    message("-- [STELLA]     + Hello World")
    add_subdirectory(examples/basics/hello-world)
    message("-- [STELLA]     + Hello World ImGui")
    add_subdirectory(examples/basics/hello-world-imgui)

    message("-- [STELLA]   Graphics")
    message("-- [STELLA]     + Shaders and Vertices")
    add_subdirectory(examples/graphics/shaders_and_vertices)
    message("-- [STELLA]     + Uniform Buffer")
    add_subdirectory(examples/graphics/uniform_buffers)
    message("-- [STELLA]     + Images")
    add_subdirectory(examples/graphics/images)
    message("-- [STELLA]     + Textures")
    add_subdirectory(examples/graphics/textures)
    message("-- [STELLA]     + Mesh")
    add_subdirectory(examples/graphics/mesh)
    message("-- [STELLA]     + Instancing")
    add_subdirectory(examples/graphics/instancing)
    message("-- [STELLA]     + Framebuffers")
    add_subdirectory(examples/graphics/framebuffers)
    message("-- [STELLA]     + Model")
    add_subdirectory(examples/graphics/model)

    message("-- [STELLA]   ECS")
    message("-- [STELLA]     + Renderer 3D")
    add_subdirectory(examples/ecs/renderer_3d)
    message("-- [STELLA]     + Animation")
    add_subdirectory(examples/ecs/animation)
endif()