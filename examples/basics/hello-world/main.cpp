#include <stella.h>

#include <stdio.h>

class ex_hello_world : public stla::application
{
public:

    ex_hello_world() : stla::application({ 500, 500, "Hello World", true, false })
    {
    }

    ~ex_hello_world()
    {
    }

    virtual void on_update(flecs::world &world, float dt) override 
    {
    }

    virtual void on_render(flecs::world &world, float dt) override
    {
        stla::window &window = stla::application::get_window();

        stla::gfx::region_2d render_region(0, 0, window.get_width(), window.get_height());

        m_command_buffer.begin_rendering(render_region);
        m_command_buffer.set_state(stla::gfx::draw_state(true, true, true));
        m_command_buffer.clear(stla::gfx::color(0.0f), render_region);
        m_command_buffer.end_rendering();

        m_command_buffer.execute();
    }

    virtual void on_event(flecs::world &world, const stla::event::quit &) override { stla::application::quit(); }

private:
    stla::gfx::command_buffer m_command_buffer;
};

int main() 
{
    stla::application *p_app = new ex_hello_world();
    p_app->run();
    delete p_app;
}