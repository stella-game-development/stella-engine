#include <stella.h>

#include <stdio.h>

STLA_PACK(
struct uniform_data
{
    uint64_t solid_texture;
});

class ex_framebuffers : public stla::application
{
    static inline const char *s_vertex_source = "#version 460 core\n"
    "#extension GL_ARB_bindless_texture : require\n"
    "layout (location = 0) in vec3 a_position;\n"
    "layout (location = 1) in vec2 a_uv;\n"
    "layout (std140, binding = 0) uniform uniform_data\n"
    "{\n"
    "   sampler2D u_solid_texture;\n"
    "};\n"
    "out vec2 v_uv;\n"
    "void main()\n"
    "{\n"
    "   v_uv = a_uv;"
    "   gl_Position = vec4(a_position, 1.0);\n"
    "}\0";

    static inline const char *s_fragment_source = "#version 460 core\n"
    "#extension GL_ARB_bindless_texture : require\n"
    "out vec4 f_color;\n"
    "in vec2 v_uv;\n"
    "layout (std140, binding = 0) uniform uniform_data\n"
    "{\n"
    "   sampler2D u_solid_texture;\n"
    "};\n"
    "void main()\n"
    "{\n"
    "   f_color = texture(u_solid_texture, v_uv);\n"
    "}\n\0";

    static inline const std::vector<float> s_quad_vertices = {
        // positions          // texture coords
         0.5f,  0.5f, 0.0f,   1.0f, 1.0f, // top right
         0.5f, -0.5f, 0.0f,   1.0f, 0.0f, // bottom right
        -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, // bottom left
        -0.5f,  0.5f, 0.0f,   0.0f, 1.0f  // top left 
    };

    static inline const std::vector<uint32_t> s_quad_indices = {  
        0, 1, 3, // first triangle
        1, 2, 3  // second triangle
    };

    static inline const std::vector<stla::gfx::vertex_attribute_descriptor> s_quad_layout = {
        { 0, 3, stla::gfx::vertex_attribute_type::f, false },
        { 1, 2, stla::gfx::vertex_attribute_type::f, false },
    };

public:

    ex_framebuffers() : stla::application({ 500, 500, "Framebuffers", true, false })
    {
        m_vertex_shader.create(stla::gfx::shader_type::vertex, s_vertex_source);
        m_fragment_shader.create(stla::gfx::shader_type::fragment, s_fragment_source);
        m_vertex_buffer.create(stla::gfx::buffer_type::vertex, s_quad_vertices);
        m_index_buffer.create(stla::gfx::buffer_type::index, s_quad_indices);
        m_uniform_buffer.create(stla::gfx::buffer_type::uniform, sizeof(m_uniform_data), &m_uniform_data);
        m_solid_texture.create(
            stla::gfx::image_info(
                stla::gfx::image_type::texture, 
                stla::gfx::pixel_format::r8g8b8a8_float, 
                stla::gfx::sampler_wrap_type::repeat,
                stla::gfx::sampler_filter_type::nearest,
                stla::gfx::ivec2(1, 1)
            ), 
            stla::color::red<glm::vec4>()
        );
        m_uniform_data.solid_texture = m_solid_texture.get_handle();
        m_uniform_buffer.copy(&m_uniform_data, sizeof(m_uniform_data));

        m_attachments.color.create(
            stla::gfx::image_info(
                stla::gfx::image_type::color_attachment, 
                stla::gfx::pixel_format::r8g8b8a8, 
                stla::gfx::sampler_wrap_type::repeat,
                stla::gfx::sampler_filter_type::nearest,
                stla::gfx::ivec2(500, 500)
            )
        );
        
        m_attachments.depth.create(
            stla::gfx::image_info(
                stla::gfx::image_type::depth_attachment, 
                stla::gfx::pixel_format::depth24_stencil8, 
                stla::gfx::sampler_wrap_type::repeat,
                stla::gfx::sampler_filter_type::nearest,
                stla::gfx::ivec2(500, 500)
            )
        );;
    }

    ~ex_framebuffers()
    {
        m_vertex_shader.destroy();
        m_fragment_shader.destroy();
        m_index_buffer.destroy();
        m_vertex_buffer.destroy();
        m_uniform_buffer.destroy();
        m_solid_texture.destory();
        m_attachments.color.destory();
        m_attachments.depth.destory();
    }

    virtual void on_update(flecs::world &world, float dt) override 
    {
    }

    virtual void on_render(flecs::world &world, float dt) override
    {
        stla::window &window = stla::application::get_window();

        stla::gfx::region_2d render_region(0, 0, window.get_width(), window.get_height());

        m_command_buffer.begin_rendering(m_attachments.color, m_attachments.depth, render_region);
        m_command_buffer.set_state(stla::gfx::draw_state(true, false, true));
        m_command_buffer.clear(stla::gfx::color(0.0f), render_region);
        m_command_buffer.bind(m_vertex_shader, m_fragment_shader);
        m_command_buffer.bind(s_quad_layout, m_vertex_buffer);
        m_command_buffer.bind(m_index_buffer);
        m_command_buffer.bind_uniform(0, m_uniform_buffer);
        m_command_buffer.draw_indexed(6);
        m_command_buffer.end_rendering();
        m_command_buffer.execute();
    }
    
    virtual void on_post_frame(flecs::world &world, float dt) 
    {
        stla::application::get_window().present(m_attachments.color);
    }

    virtual void on_event(flecs::world &world, const stla::event::quit&) override { stla::application::quit(); }

private:
    stla::gfx::command_buffer m_command_buffer;
    stla::gfx::shader m_vertex_shader;
    stla::gfx::shader m_fragment_shader;
    stla::gfx::buffer m_vertex_buffer;
    stla::gfx::buffer m_index_buffer;
    stla::gfx::buffer m_uniform_buffer;
    stla::gfx::image m_solid_texture;
    struct {
        stla::gfx::image color;
        stla::gfx::image depth;
    } m_attachments;
    uniform_data m_uniform_data;
};

int main() 
{
    stla::application *p_app = new ex_framebuffers();
    p_app->run();
    delete p_app;
}