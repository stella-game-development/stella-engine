#include <stella.h>

#include <stdio.h>

STLA_PACK(
struct uniform_data
{
    glm::mat4 view_projection;
    glm::mat4 transform;
});

class ex_uniform_buffers : public stla::application
{
    static inline const char *s_vertex_source = "#version 460 core\n"
    "layout (location = 0) in vec3 a_position;\n"
    "layout (location = 1) in vec2 a_uv;\n"
    "layout (std140, binding = 0) uniform uniform_data\n"
    "{\n"
    "   mat4 u_view_projection;\n"
    "   mat4 u_transform;\n"
    "};\n"
    "out vec4 v_color;"
    "void main()\n"
    "{\n"
    "   v_color = vec4(a_uv, 0.0, 1.0);\n"
    "   gl_Position = u_view_projection * u_transform * vec4(a_position, 1.0);\n"
    "}\0";

    static inline const char *s_fragment_source = "#version 460 core\n"
    "out vec4 f_color;\n"
    "in vec4 v_color;"
    "void main()\n"
    "{\n"
    "   f_color = v_color;\n"
    "}\n\0";

    static inline const std::vector<float> s_quad_vertices = {
        // positions          // texture coords
         0.5f,  0.5f, 0.0f,   1.0f, 1.0f, // top right
         0.5f, -0.5f, 0.0f,   1.0f, 0.0f, // bottom right
        -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, // bottom left
        -0.5f,  0.5f, 0.0f,   0.0f, 1.0f  // top left 
    };

    static inline const std::vector<uint32_t> s_quad_indices = {  
        0, 1, 3, // first triangle
        1, 2, 3  // second triangle
    };

    static inline const std::vector<stla::gfx::vertex_attribute_descriptor> s_quad_layout = {
        { 0, 3, stla::gfx::vertex_attribute_type::f, false },
        { 1, 2, stla::gfx::vertex_attribute_type::f, false },
    };

public:

    ex_uniform_buffers() : stla::application({ 500, 500, "Uniform Buffers", true, false })
    {
        m_vertex_shader.create(stla::gfx::shader_type::vertex, s_vertex_source);
        m_fragment_shader.create(stla::gfx::shader_type::fragment, s_fragment_source);
        m_vertex_buffer.create(stla::gfx::buffer_type::vertex, s_quad_vertices);
        m_index_buffer.create(stla::gfx::buffer_type::index, s_quad_indices);
        m_uniform_buffer.create(stla::gfx::buffer_type::uniform, sizeof(m_uniform_data), &m_uniform_data);
    }

    ~ex_uniform_buffers()
    {
        m_vertex_shader.destroy();
        m_fragment_shader.destroy();
        m_index_buffer.destroy();
        m_vertex_buffer.destroy();
        m_uniform_buffer.destroy();
    }

    virtual void on_update(flecs::world &world, float dt) override 
    {
    }

    virtual void on_render(flecs::world &world, float dt) override
    {
        stla::window &window = stla::application::get_window();

        stla::gfx::region_2d render_region(0, 0, window.get_width(), window.get_height());

        m_uniform_data.view_projection = glm::mat4(1.0f);
        m_uniform_data.transform = glm::mat4(1.0f);

        m_uniform_data.transform = glm::rotate(
            m_uniform_data.transform, 
            glm::radians(static_cast<float>(stla::utils::get_time_ms()) * 0.05f), 
            glm::vec3(0.0, 0.0, -1.0)
        );

        m_command_buffer.begin_rendering(render_region);
        m_command_buffer.set_state(stla::gfx::draw_state(true, false, true));
        m_command_buffer.clear(stla::gfx::color(0.0f), render_region);
        m_command_buffer.bind(m_vertex_shader, m_fragment_shader);
        m_command_buffer.bind(s_quad_layout, m_vertex_buffer);
        m_command_buffer.bind(m_index_buffer);
        m_command_buffer.bind_uniform(0, m_uniform_buffer);
        m_command_buffer.copy(m_uniform_buffer, &m_uniform_data, sizeof(m_uniform_data));
        m_command_buffer.draw_indexed(6);
        m_command_buffer.end_rendering();

        m_command_buffer.execute();
    }

    virtual void on_event(flecs::world &world, const stla::event::quit&) override { stla::application::quit(); }

private:
    stla::gfx::command_buffer m_command_buffer;
    stla::gfx::shader m_vertex_shader;
    stla::gfx::shader m_fragment_shader;
    stla::gfx::buffer m_vertex_buffer;
    stla::gfx::buffer m_index_buffer;
    stla::gfx::buffer m_uniform_buffer;

    uniform_data m_uniform_data;
};

int main() 
{
    stla::application *p_app = new ex_uniform_buffers();
    p_app->run();
    delete p_app;
}