#include <stella.h>

#include <stdio.h>

#include <filesystem>
#include <unordered_map>

#ifndef MODEL_FILE_PATH
    #error "Model file path macro not defined!"
#endif

STLA_PACK(
struct uniform_data
{
    glm::mat4 view_projection;
    glm::mat4 transform;
    uint64_t diffuse;
});

class ex_model : public stla::application
{
    static inline const char *s_vertex_source = "#version 460 core\n"
    "#extension GL_ARB_bindless_texture : require\n"
    "layout (location = 0) in vec3 a_position;\n"
    "layout (location = 1) in vec3 a_normal;\n"
    "layout (location = 2) in vec2 a_uv;\n"
    "layout (std140, binding = 0) uniform uniform_data\n"
    "{\n"
    "   mat4 u_view_projection;\n"
    "   mat4 u_transform;\n"
    "   sampler2D u_diffuse;\n"
    "};\n"
    "out vec2 v_uv;"
    "void main()\n"
    "{\n"
    "   v_uv = a_uv;\n"
    "   gl_Position = u_view_projection * u_transform * vec4(a_position, 1.0);\n"
    "}\0";

    static inline const char *s_fragment_source = "#version 460 core\n"
    "#extension GL_ARB_bindless_texture : require\n"
    "layout (std140, binding = 0) uniform uniform_data\n"
    "{\n"
    "   mat4 u_view_projection;\n"
    "   mat4 u_transform;\n"
    "   sampler2D u_diffuse;\n"
    "};\n"
    "out vec4 f_color;\n"
    "in vec2 v_uv;"
    "void main()\n"
    "{\n"
    "   f_color = texture(u_diffuse, v_uv);\n"
    "}\n\0";

public:

    ex_model() : stla::application({ 500, 500, "Model", true, false })
    {
        if (!std::filesystem::exists(MODEL_FILE_PATH)) {
            STLA_LOG_ERROR("Model file path does not exist: %s", MODEL_FILE_PATH);
            STLA_ASSERT_RELEASE(0);
        }
        
        m_scene_loader.load(MODEL_FILE_PATH);
        m_model.load(m_scene_loader);

        m_vertex_shader.create(stla::gfx::shader_type::vertex, s_vertex_source);
        m_fragment_shader.create(stla::gfx::shader_type::fragment, s_fragment_source);
    
        for (const auto &mesh : m_model.get_meshes()) {
            m_render_meshes.emplace_back(mesh);

            stla::gfx::buffer uniform_buffer;
            uniform_buffer.create(stla::gfx::buffer_type::uniform, sizeof(uniform_data));
            m_uniform_buffers.push_back(uniform_buffer);
        }

        // Note: the example model only has one diffuse texture
        m_diffuse_texture.create(m_model.get_diffuse_maps().front().file_path, 
            stla::gfx::sampler_wrap_type::clamp_to_edge, stla::gfx::sampler_filter_type::nearest);
    }

    ~ex_model()
    {
        m_vertex_shader.destroy();
        m_fragment_shader.destroy();
        m_diffuse_texture.destory();

        for (auto &uniform_buffer : m_uniform_buffers) {
            uniform_buffer.destroy();
        }
        m_uniform_buffers.clear();

        for (auto &render_mesh : m_render_meshes) {
            render_mesh.destroy();
        }

        m_render_meshes.clear();
    }

    virtual void on_update(flecs::world &world, float dt) override 
    {
    }

    virtual void on_render(flecs::world &world, float dt) override
    {
        orbit_camera();

        stla::window &window = stla::application::get_window();
        const stla::gfx::region_2d render_region(0, 0, window.get_width(), window.get_height());
        m_command_buffer.begin_rendering(render_region);
        m_command_buffer.set_state(stla::gfx::draw_state(true, true, true));
        m_command_buffer.clear(stla::gfx::color(0.0f, 0.10f, 0.25f, 1.0f), render_region);
        m_command_buffer.bind(m_vertex_shader, m_fragment_shader);
        
        for (int i = 0; i < m_render_meshes.size(); i++) {
            m_command_buffer.bind_uniform(0, m_uniform_buffers[i]);
            m_render_meshes[i].draw(m_command_buffer);
        }
        
        m_command_buffer.end_rendering();
        m_command_buffer.execute();
    }

private:

    void orbit_camera()
    {
        stla::window &window = stla::application::get_window();
        const float radius = 4.5f;
        const float speed = 0.0005f;
        uniform_data data;

        data.diffuse = m_diffuse_texture.get_handle();

        glm::mat4 projection = glm::perspective(glm::radians(45.0f), window.get_aspect_ratio(), 0.1f, 10000.0f);
        float cam_x = glm::cos(speed * stla::utils::get_time_ms()) * radius;
        float cam_z = glm::sin(speed * stla::utils::get_time_ms()) * radius;
        glm::vec3 camera_position = glm::vec3(cam_x, 2.0f, cam_z);
        glm::mat4 view = glm::lookAt(camera_position, glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        data.view_projection = projection * view;

        for (int i = 0; i < m_uniform_buffers.size(); i++) {
            data.transform = m_model.get_normalization_transform() * m_model.get_meshes()[i].get_transform();
            m_uniform_buffers[i].copy(data);
        }
    }

    stla::gfx::command_buffer m_command_buffer;
    stla::gfx::shader m_vertex_shader;
    stla::gfx::shader m_fragment_shader;
    stla::gfx::model m_model;
    stla::utils::assimp::scene_loader m_scene_loader;
    std::vector<stla::gfx::buffer> m_uniform_buffers;
    std::vector<stla::gfx::render_mesh> m_render_meshes;
    stla::gfx::image m_diffuse_texture;
};

int main() 
{
    stla::application *p_app = new ex_model();
    p_app->run();
    delete p_app;
}