#include <stella.h>

#include <stdio.h>

#include <filesystem>

#ifndef STANFORD_BUNNY_OBJ_PATH
    #error "Stanford bunny obj path macro not defined!"
#endif

STLA_PACK(
struct uniform_data
{
    glm::mat4 view_projection;
    glm::mat4 transform;
});

class ex_mesh : public stla::application
{
    static inline const char *s_vertex_source = "#version 460 core\n"
    "layout (location = 0) in vec3 a_position;\n"
    "layout (location = 1) in vec3 a_normal;\n"
    "layout (std140, binding = 0) uniform uniform_data\n"
    "{\n"
    "   mat4 u_view_projection;\n"
    "   mat4 u_transform;\n"
    "};\n"
    "out vec4 v_color;"
    "void main()\n"
    "{\n"
    "   v_color = vec4(a_normal, 1.0);\n"
    "   gl_Position = u_view_projection * u_transform * vec4(a_position, 1.0);\n"
    "}\0";

    static inline const char *s_fragment_source = "#version 460 core\n"
    "out vec4 f_color;\n"
    "in vec4 v_color;"
    "void main()\n"
    "{\n"
    "   f_color = v_color;\n"
    "}\n\0";

public:

    ex_mesh() : stla::application({ 500, 500, "Mesh", true, false })
    {
        if (!std::filesystem::exists(STANFORD_BUNNY_OBJ_PATH)) {
            STLA_LOG_ERROR("Stanford bunny obj path does not exist: %s", STANFORD_BUNNY_OBJ_PATH);
            STLA_ASSERT_RELEASE(0);
        }
        
        m_mesh.load(STANFORD_BUNNY_OBJ_PATH);
        std::cout << m_mesh;
        
        m_mesh_centroid = stla::math::centroid(m_mesh);
        m_render_mesh.create(m_mesh);

        m_vertex_shader.create(stla::gfx::shader_type::vertex, s_vertex_source);
        m_fragment_shader.create(stla::gfx::shader_type::fragment, s_fragment_source);
        m_uniform_buffer.create(stla::gfx::buffer_type::uniform, sizeof(uniform_data));
    }

    ~ex_mesh()
    {
        m_vertex_shader.destroy();
        m_fragment_shader.destroy();
        m_uniform_buffer.destroy();
    }

    virtual void on_update(flecs::world &world, float dt) override 
    {
    }

    virtual void on_render(flecs::world &world, float dt) override
    {
        stla::window &window = stla::application::get_window();

        orbit_camera();

        const stla::gfx::region_2d render_region(0, 0, window.get_width(), window.get_height());
        m_command_buffer.begin_rendering(render_region);
        m_command_buffer.set_state(stla::gfx::draw_state(true, true, true));
        m_command_buffer.clear(stla::gfx::color(0.0f), render_region);
        m_command_buffer.bind(m_vertex_shader, m_fragment_shader);
        m_render_mesh.draw(m_command_buffer);
        m_command_buffer.bind_uniform(0, m_uniform_buffer);
        m_command_buffer.draw_indexed(m_mesh.get_indices().size());
        m_command_buffer.end_rendering();

        m_command_buffer.execute();
    }

    virtual void on_event(flecs::world &world, const stla::event::quit&) override { stla::application::quit(); }

private:

    void orbit_camera()
    {
        stla::window &window = stla::application::get_window();
        const float radius = 10.0f;
        const float speed = 0.0005f;
        uniform_data data;

        data.transform = glm::mat4(1.0f);
        glm::mat4 projection = glm::perspective(glm::radians(45.0f), window.get_aspect_ratio(), 0.1f, 10000.0f);
        float cam_x = glm::cos(speed * stla::utils::get_time_ms()) * radius;
        float cam_z = glm::sin(speed * stla::utils::get_time_ms()) * radius;
        glm::vec3 camera_position = glm::vec3(cam_x, 5.0f, cam_z);
        glm::mat4 view = glm::lookAt(camera_position, m_mesh_centroid, glm::vec3(0.0f, 1.0f, 0.0f));
        data.view_projection = projection * view;
        m_uniform_buffer.copy(data);
    }

    stla::gfx::command_buffer m_command_buffer;
    stla::gfx::shader m_vertex_shader;
    stla::gfx::shader m_fragment_shader;
    stla::gfx::buffer m_uniform_buffer;
    stla::gfx::mesh m_mesh;
    stla::gfx::render_mesh m_render_mesh;
    stla::utils::assimp::scene_loader m_scene_loader;
    glm::vec3 m_mesh_centroid{ 0.0f };
};

int main() 
{
    stla::application *p_app = new ex_mesh();
    p_app->run();
    delete p_app;
}