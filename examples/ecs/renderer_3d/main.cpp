#include <stella.h>

#include <stdio.h>

#include <filesystem>
#include <unordered_map>
#include <concepts>
#include <map>
#include <iostream>

//STLA_PACK(
struct model_data 
{
    glm::mat4 view_projection;
    glm::mat4 transform;
};

class ex_renderer_3d : public stla::application
{
    //TODO bone transforms at a different binding

    static inline const char *s_vertex_source = R"(
        #version 460 core
        #extension GL_ARB_bindless_texture : require
        layout (location = 0) in vec3 a_position;
        layout (location = 1) in vec3 a_normal;
        layout (location = 2) in vec2 a_uv;
        layout (std140, binding = 0) uniform model_data
        {
            mat4 u_view_projection;
            mat4 u_transform;
        };
        out vec4 v_color;
        void main()
        {
            v_color = vec4(a_normal, 1.0);
            //gl_Position = vec4(a_position, 1.0);
            gl_Position = u_view_projection * u_transform * vec4(a_position, 1.0);
        }
    )";

    static inline const char *s_fragment_source = R"(
        #version 460 core
        #extension GL_ARB_bindless_texture : require
        layout (std140, binding = 0) uniform model_data
        {
            mat4 u_view_projection;
            mat4 u_transform;
        };
        out vec4 f_color;
        in vec4 v_color;
        void main()
        {
            f_color = v_color;
        }
    )";

public:

    ex_renderer_3d() : stla::application({ 500, 500, "ECS: Renderer 3D", true, false })
    {        
        stla::application::import_ecs_module<stla::ecs_3d::module>();

        auto &window = stla::application::get_window();
        auto &world = stla::application::get_world();
        
        world.entity("Mesh Renderer").emplace<stla::ecs_3d::component::mesh_renderer>(
            stla::gfx::region_2d(0, 0, window.get_width(), window.get_height()),
            stla::gfx::color(0.0f, 0.10f, 0.25f, 1.0f)
        );

        world.entity("cube_mesh").emplace<stla::ecs_3d::component::mesh>(stla::gfx::mesh::cube());
        world.entity("vertex_shader").emplace<stla::ecs_3d::component::shader>(stla::gfx::shader_type::vertex, s_vertex_source);
        world.entity("fragment_shader").emplace<stla::ecs_3d::component::shader>(stla::gfx::shader_type::fragment, s_fragment_source);

        m_cube_entity = world.entity("cube_object")
            .emplace<stla::ecs_3d::component::material>(world, "vertex_shader", "fragment_shader")
            .emplace<stla::ecs_3d::component::renderable>(world, "cube_mesh");

        m_cube_model_data = stla::ecs_3d::utils::add_uniform_data<model_data>(m_cube_entity, 0, "model_data_uniform"); 
    }

    ~ex_renderer_3d()
    { 
    }

    virtual void on_startup(flecs::world &world) override
    {

    } 

    virtual void on_update(flecs::world &world, float dt) override
    {
        stla::ecs_3d::utils::update_uniform_data<model_data>(m_cube_entity, "model_data_uniform", [&](model_data& data) {
            data.transform = glm::mat4(1.0f);
            data.view_projection = orbit_camera();
        });
    }

private:

    glm::mat4 orbit_camera()
    {
        stla::window &window = stla::application::get_window();
        const float radius = 4.0f;
        const float speed = 0.0002f;

        glm::mat4 projection = glm::perspective(glm::radians(45.0f), window.get_aspect_ratio(), 0.1f, 10000.0f);
        float cam_x = glm::cos(speed * stla::utils::get_time_ms()) * radius;
        float cam_z = glm::sin(speed * stla::utils::get_time_ms()) * radius;
        glm::vec3 camera_position = glm::vec3(cam_x, 2.0f, cam_z);
        glm::mat4 view = glm::lookAt(camera_position, glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        return projection * view;
    }

    flecs::entity m_cube_entity;
    flecs::entity m_cube_model_data;
};

int main() 
{
    stla::application *p_app = new ex_renderer_3d();
    p_app->run();
    delete p_app;
}