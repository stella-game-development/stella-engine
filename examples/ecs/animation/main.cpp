#include <stella.h>

#include <stdio.h>

#include <filesystem>
#include <unordered_map>
#include <concepts>
#include <map>

#ifndef MODEL_FILE_PATH
    #error "Model file path macro not defined!"
#endif

STLA_PACK(
struct uniform_data
{
    static inline const int32_t MAX_BONES = 100;
    glm::mat4 view_projection;
    glm::mat4 transform;
    glm::mat4 bone_transforms[MAX_BONES];
});

class ex_animation : public stla::application
{
    //TODO bone transforms at a different binding

    static inline const char *s_vertex_source = R"(
        #version 460 core
        #extension GL_ARB_bindless_texture : require
        layout (location = 0) in vec3 a_position;
        layout (location = 1) in vec3 a_normal;
        layout (location = 2) in vec2 a_uv;
        layout (location = 3) in ivec4 a_bone_ids;
        layout (location = 4) in vec4 a_bone_weights;
        const int MAX_BONES = 100;
        const int MAX_BONE_INFLUENCE = 4;
        layout (std140, binding = 0) uniform uniform_data
        {
            mat4 u_view_projection;
            mat4 u_transform;
            mat4 u_bone_transforms[MAX_BONES];
        };
        out vec4 v_color;
        void main()
        {
            v_color = vec4(a_normal, 1.0);
            mat4 bone_transform = u_bone_transforms[a_bone_ids[0]] * a_bone_weights[0];
            bone_transform += u_bone_transforms[a_bone_ids[1]] * a_bone_weights[1];
            bone_transform += u_bone_transforms[a_bone_ids[2]] * a_bone_weights[2];
            bone_transform += u_bone_transforms[a_bone_ids[3]] * a_bone_weights[3];

            vec4 PosL = bone_transform * vec4(a_position, 1.0);
            gl_Position = u_view_projection * u_transform * PosL;
        }
    )";

    static inline const char *s_fragment_source = R"(
        #version 460 core
        #extension GL_ARB_bindless_texture : require
        const int MAX_BONES = 100;
        layout (std140, binding = 0) uniform uniform_data
        {
            mat4 u_view_projection;
            mat4 u_transform;
            mat4 u_bone_transforms[MAX_BONES];
        };
        out vec4 f_color;
        in vec4 v_color;
        void main()
        {
            f_color = v_color;
        }
    )";

public:

    ex_animation() : stla::application({ 500, 500, "ECS: Animation", true, false })
    {
        if (!std::filesystem::exists(MODEL_FILE_PATH)) {
            STLA_LOG_ERROR("Model file path does not exist: %s", MODEL_FILE_PATH);
            STLA_ASSERT_RELEASE(0);
        }
 
        m_scene_loader.load(MODEL_FILE_PATH);
        m_skeleton = stla::gfx::skeleton(m_scene_loader); 
        m_animated_model = stla::gfx::animated_model(m_scene_loader, m_skeleton);
        m_scene_loader.for_each([&](const aiAnimation *p_animation) {
            if (p_animation == nullptr) {
                return;
            }

            // printf("Anim: %s\n", p_animation->mName.C_Str());
            m_animations.push_back(stla::gfx::animation(*p_animation));
        });

        //glm::mat4 model_transform = m_animated_model.get_normalization_transform() * m_animated_model.get_global_inverse_transform();
        m_animator.set_animation(m_animations[0], m_skeleton, 100);
       
        m_vertex_shader.create(stla::gfx::shader_type::vertex, s_vertex_source);
        m_fragment_shader.create(stla::gfx::shader_type::fragment, s_fragment_source);
        m_uniform_buffer.create(stla::gfx::buffer_type::uniform, sizeof(uniform_data));
    
        for (const auto &tri_mesh : m_animated_model.get_meshes()) {
            m_render_meshes.emplace_back(tri_mesh);
        }
    }

    ~ex_animation()
    {
        m_vertex_shader.destroy();
        m_fragment_shader.destroy();
        m_uniform_buffer.destroy();
        
        for (auto &render_mesh : m_render_meshes) {
            render_mesh.destroy();
        }
        
        m_render_meshes.clear();
    }

    virtual void on_update(flecs::world &world, float dt) override 
    {
        
    }

    virtual void on_render(flecs::world &world, float dt) override
    {
        // m_animator.UpdateAnimation(dt * 0.0009f);
        m_animator.update(dt);

        orbit_camera();

        auto bone_transforms = m_animator.get_final_bone_matrices();
        STLA_ASSERT(bone_transforms.size() <= uniform_data::MAX_BONES);
        for (int i = 0; i < bone_transforms.size(); i++) {
            m_uniform_data.bone_transforms[i] = bone_transforms[i];
        }

        m_uniform_buffer.copy(m_uniform_data);
        
        stla::window &window = stla::application::get_window();
        const stla::gfx::region_2d render_region(0, 0, window.get_width(), window.get_height());
        m_command_buffer.begin_rendering(render_region);
        m_command_buffer.set_state(stla::gfx::draw_state(true, true, true));
        m_command_buffer.clear(stla::gfx::color(0.0f, 0.10f, 0.25f, 1.0f), render_region);
        m_command_buffer.bind(m_vertex_shader, m_fragment_shader);
        m_command_buffer.bind_uniform(0, m_uniform_buffer);
        
        for (auto &render_mesh : m_render_meshes) {
            render_mesh.draw(m_command_buffer);
        }
        
        m_command_buffer.end_rendering();
        m_command_buffer.execute();
    }

    virtual void on_event(flecs::world &world, const stla::event::quit&) override { stla::application::quit(); }

private:

    void orbit_camera()
    {
        stla::window &window = stla::application::get_window();
        const float radius = 4.0f;
        const float speed = 0.0002f;

        m_uniform_data.transform = m_animated_model.get_normalization_transform();

        glm::mat4 projection = glm::perspective(glm::radians(45.0f), window.get_aspect_ratio(), 0.1f, 10000.0f);
        float cam_x = glm::cos(speed * stla::utils::get_time_ms()) * radius;
        float cam_z = glm::sin(speed * stla::utils::get_time_ms()) * radius;
        glm::vec3 camera_position = glm::vec3(cam_x, 2.0f, cam_z);
        glm::mat4 view = glm::lookAt(camera_position, glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        m_uniform_data.view_projection = projection * view;
    }

    stla::gfx::command_buffer m_command_buffer;
    stla::gfx::shader m_vertex_shader;
    stla::gfx::shader m_fragment_shader;
    stla::gfx::buffer m_uniform_buffer;
    stla::gfx::animated_model m_animated_model;
    stla::gfx::skeleton m_skeleton;
    std::vector<stla::gfx::animation> m_animations;
    stla::gfx::animator m_animator;
    std::vector<stla::gfx::render_mesh> m_render_meshes;
    stla::utils::assimp::scene_loader m_scene_loader;
    uniform_data m_uniform_data;
};

int main() 
{
    stla::application *p_app = new ex_animation();
    p_app->run();
    delete p_app;
}